<?php
session_start();

require_once "db.php";
require_once "functions.php";


if($_SERVER['REQUEST_METHOD'] == 'POST') {
    if(empty($_POST['urlCover']) || empty($_POST['title']) || empty($_POST['subTitle']) || empty($_POST['aboutYou']) || empty($_POST['number']) || empty($_POST['location']) || empty($_POST['provide_id']) || empty($_POST['image1Url']) || empty($_POST['descProduct1']) || empty($_POST['image2Url']) || empty($_POST['descProduct2']) || empty($_POST['image3Url']) || empty($_POST['descProduct3']) || empty($_POST['descProduct4']) || empty($_POST['linkedIn']) || empty($_POST['facebook']) || empty($_POST['twitter']) || empty($_POST['google'])) {
       $_SESSION['error'] = "All fields are required";
       header('Location: create-page.php');
    } else {
$sqlInsert = "INSERT INTO company
(urlCover, title, subTitle, aboutYou, number, location, provide_id, image1Url, descProduct1, image2Url, descProduct2, image3Url, descProduct3, descProduct4, linkedIn, facebook, twitter, google)
VALUES (:urlCover, :title, :subTitle, :aboutYou, :number, :location, :provide_id, :image1Url, :descProduct1, :image2Url, :descProduct2, :image3Url, :descProduct3, :descProduct4, :linkedIn, :facebook, :twitter, :google)";

$stmtInsert = $pdo->prepare($sqlInsert);
if($stmtInsert->execute($_POST)) {
    $id=encrypt($pdo->lastInsertId());
    $id = urlencode($id);
        header("Location: company.php?id={$id}");
        die();
    } else {
        $_SESSION['error'] = "error page";
        header('Location: create-page.php');
        die();
    }
    header('Location: create-page.php');
    die();
}

} else {
    header('Location: create-page.php');
    die();
}