<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" 
    integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <style>
        .bg-image {
            background-image: url('https://picsum.photos/id/2/2000/2000');
            background-repeat: no-repeat;
            background-size: cover;
            height: 100vh;
            opacity: 0.8;
        }
        .btn {
            color: white!important;
        }
        .btn:hover {
            color: gray!important;
        }
    </style>
</head>
<body>
    <div class="container-fluid bg-image d-flex align-items-center justify-content-center text-center text-light">
        <div class="row">
            <div class="col">
                   <div class="h1">Create your very own web page</div>
                   <div class="h3">All you need is to fill out this form!</div>
                   <div><a href="create-page.php" class="btn btn-outline-light">Start</a></div>
            </div>
        </div>
    </div>
</body>
</html>