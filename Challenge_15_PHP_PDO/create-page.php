<?php
session_start();
require_once 'db.php';
require_once 'functions.php';
$sqlProvide = "SELECT * FROM provide";
$stmtProvide = $pdo->query($sqlProvide);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" 
    integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <style>
        .bg-image {
            background-image: url('https://picsum.photos/id/668/2000/2000');
            background-repeat: no-repeat;
            background-size: cover;
            height: 100%;
            opacity: 0.9;
        }
    </style>
</head>
<body>
    <div class="bg-image">
    <div class="container ">
        <div class="row flex-column">
            <div class="col">
                <h1 class="text-center text-dark m-5">You are one step away from your web page</h1>
                <?php
        if(isset($_SESSION['error'])) {
          echo "<div class='alert alert-danger text-center' role='alert'>
          {$_SESSION['error']}";
          unset($_SESSION['error']);
     echo   "</div>";
        } if(isset($_SESSION['success'])) {
          echo "<div class='alert alert-success text-center' role='alert'>
          {$_SESSION['success']}";
          unset($_SESSION['success']);
     echo "</div>";
        }
        ?>
                <form action="insert-page.php" method="POST">
                    <div class="row">
                    <div class="col-md-4 col-12 mb-2 mb-md-0">
                        <div class="row mb-3 mx-1">
                            <div class="col-12 bg-light ">
                                <div class="form-group">
                                    <label for="urlCover">Cover image url</label>
                                    <input type="text" class="form-control" id="urlCover" name="urlCover"  value="<?= isset($_POST['urlCover']) ? $_POST['urlCover'] : '' ?>">
                                </div>
                                <div class="form-group">
                                    <label for="coverTitle">Main cover title</label>
                                    <input type="text" class="form-control" id="coverTitle" name="title">
                                </div>
                                <div class="form-group">
                                    <label for="coverSubTitle">Subtitle of Page</label>
                                    <input type="text" class="form-control" id="coverSubTitle" name="subTitle">
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Something about you</label>
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="aboutYou"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="number">Telephone number</label>
                                    <input type="text" class="form-control" id="coverSubTitle" name="number">
                                </div>
                                <div class="form-group">
                                    <label for="location">Location</label>
                                    <input type="text" class="form-control" id="coverSubTitle" name="location">
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3 mx-1">
                            <div class="col-12 bg-light">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Do you provide services or products?</label>
                                    <select class="form-control" id="exampleFormControlSelect1" name="provide_id">
                                    <?php while($row=$stmtProvide->fetch()) {
                                            echo  "<option value='{$row['id']}'>{$row['name']}</option>";   
                                         }    
                                      ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-12 mb-3 mb-md-0">
                        <div class="col-12 bg-light p-3">
                            <div class="text-">Provide an url for an image and description of your service/product</div>
                            <div class="form-group">
                                <label for="image1">Image url</label>
                                <input type="text" class="form-control" id="image1" name="image1Url">
                            </div>
                            <div class="form-group">
                                    <label for="exampleFormControlTextarea2">Description of service/product</label>
                                    <textarea class="form-control" id="exampleFormControlTextarea2" rows="3" name="descProduct1"></textarea>
                                </div>
                            <div class="form-group">
                                <label for="image2">Image Url</label>
                                <input type="text" class="form-control" id="image2" name="image2Url">
                            </div>
                            <div class="form-group">
                                    <label for="exampleFormControlTextarea3">Description of service/product</label>
                                    <textarea class="form-control" id="exampleFormControlTextarea3" rows="2" name="descProduct2"></textarea>
                                </div>
                            <div class="form-group">
                                <label for="image3">Image Url</label>
                                <input type="text" class="form-control" id="image3" name="image3Url">
                            </div>
                            <div class="form-group">
                                    <label for="exampleFormControlTextarea4">Description of service/product</label>
                                    <textarea class="form-control" id="exampleFormControlTextarea4" rows="2" name="descProduct3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-12 mb-3 mb-md-0">
                    <div class="col-12 bg-light p-3">
                            <div class="text-left">Provide a description of your company, something people should be aware of before they contact you:</div>
                            <div class="form-group">
                                    <label for="exampleFormControlTextarea5">Description of service/product</label>
                                    <textarea class="form-control" id="exampleFormControlTextarea5" rows="3" name="descProduct4"></textarea>
                                </div>
                            <hr>
                            <div class="form-group">
                                <label for="linkedIn">Linked In</label>
                                <input type="text" class="form-control" id="linkedIn" name="linkedIn">
                            </div>
                            <div class="form-group">
                                <label for="facebook">Facebook</label>
                                <input type="text" class="form-control" id="facebook" name="facebook">
                            </div>
                            <div class="form-group">
                                <label for="twitter">Twitter</label>
                                <input type="text" class="form-control" id="twitter" name="twitter">
                            </div>
                            <div class="form-group">
                                <label for="google">Google+</label>
                                <input type="text" class="form-control" id="google" name="google">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-secondary btn-block m-5">Submit</form>
                </form>
            </div>

        </div>

    </div>
    </div>
    </div>
</body>

</html>