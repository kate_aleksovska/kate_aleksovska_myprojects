<?php
require_once 'db.php';
require_once 'functions.php';


$id = decrypt($_GET['id']);

$sqlPage = "SELECT company.*, provide.name as provide
FROM `company` LEFT JOIN provide on company.provide_id = provide.id
WHERE company.id =:id LIMIT 1";

$stmtPage = $pdo->prepare($sqlPage);
$stmtPage->execute(['id' => $id]);


if($stmtPage->rowCount() == 0) {
    header("Location: create-page.php");
    die();
} 
$page = $stmtPage->fetch();


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <style>
        .bg-image {
            background-image: url('<?= $page['urlCover'] ?>');
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
            height: 100vh;
            opacity: 0.9;
        }
        @media(max-width:576px) {
    body {
        padding-top: 0;
    }
    .fixed-top {
        position:static!important;
    }
}
.title {
    position: absolute;
    top: 100px;
}
    </style>
</head>

<body>
    <nav class="navbar navbar-expand-sm navbar-light bg-light fixed-top position-md-static text-capitalize">
    <a class="navbar-brand" href="#webster">Webster</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarColor01">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item mr-3">
          <a class="nav-link active home d-inline-block" href="#home">Home<span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item mr-3">
          <a class="nav-link aboutus d-inline-block" href="#aboutus" >about us</a>
        </li>
        <li class="nav-item mr-3">
          <a class="nav-link provide d-inline-block" href="#provide"><?= $page['provide'] ?></a>
        </li>
        <li class="nav-item">
          <a class="nav-link contact d-inline-block" href="#contact">contact</a>
        </li>
      </ul>
    </div>
  </nav>
    <div class="container-fluid bg-image d-flex justify-content-center" id="home">
        <div class="row  flex-column justify-content-center text-center text-light">
            <div class="col ">
                <div class="h2 m-3 title"><?= $page['title'] ?></div>
            </div>
            <div class="col">
                <p class="h3"><?= $page['subTitle'] ?></p>
            </div>
        </div>
    </div>
    <div class="container-fluid justify-content-center" id="aboutus">
        <div class="row flex-column justify-content-center align-items-center">
            <div class="col-lg-3 text-center p-3">
                <div class="h2">About Us</div>
                <p><?= $page['aboutYou'] ?></p>
                <hr>

            </div>
            <div class="col-lg-3 text-center">
                <p class="mb-0">Tel: <?= $page['number'] ?></p>
                <p class="mb-0">Location: <?= $page['location'] ?></p>
            </div>
        </div>
    </div>
    <div class="container-fluid" id="provide">
        <div class="row">
            <div class="col">
                <div class="container" >
                    <div class="row justify-content-center align-items-center">
                        <div class="row align-items-center flex-column text-left">
                            <div class="col">
                                <h2><?= $page['provide'] ?></h2>
                            </div>
                            <div class="col text-white">
                                <div class="row justify-content-center">
                                    <div class="col-12 col-md-4">
                                        <div class="card  m-3 m-md-1">
                                            <img src="<?= $page['image1Url'] ?>" class="card-img-top" alt="...">
                                            <div class="card-body bg-dark">
                                                <p class="card-title">Service 1 description</p>
                                                <p class="card-text"><?= $page['descProduct1'] ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="card   m-3 m-md-1">
                                            <img src="<?= $page['image2Url'] ?>" class="card-img-top" alt="...">
                                            <div class="card-body bg-dark">
                                                <p class="card-title">Service 2 description</p>
                                                <p class="card-text"><?= $page['descProduct2'] ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <div class="card   m-3 m-md-1">
                                            <img src="<?= $page['image3Url'] ?>" class="card-img-top" alt="...">
                                            <div class="card-body bg-dark">
                                                <p class="card-title">Service 3 description</p>
                                                <p class="card-text"><?= $page['descProduct3'] ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="container p-3" id="contact">
        <div class="row">
            <div class="col-md-6">
                <div class="h3 text-left">Contact</div>
                <p><?= $page['descProduct4'] ?></p>
            </div>
            <div class="col-md-6">
                <form>
                    <div class="form-group">
                        <label for="name1">Name</label>
                        <input type="name1" class="form-control" id="name1" aria-describedby="name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea8">Message</label>
                        <textarea class="form-control" id="exampleFormControlTextarea8" rows="3"></textarea>
                    </div>
                    <a type="submit" class="btn btn-primary">Submit</a>
                </form>
            </div>
        </div>

    </div>
    <footer class="bg-dark text-center">
        <p class="text-white">Copyright by Kate Aleksovska</p>
        <p class="mb-0"><a href="<?= $page['linkedIn'] ?>">LinkedIn</a> <a href="<?= $page['facebook'] ?>">Facebook</a> <a href="<?= $page['twitter'] ?>">Twitter</a> <a href="<?= $page['google'] ?>">Google+</a></p>
    </footer>

    <script type="text/javascript">  
       let home = document.querySelector("#home");
       console.log(home)
       let aboutus = document.querySelector("#aboutus");
       let provide = document.querySelector("#provide");
       let contact = document.querySelector("#contact");

       window.addEventListener("scroll",()=>{
         var windo = window.pageYOffset;
         if(home.offsetTop <= windo && aboutus.offsetTop > windo){
           console.log("Home")
           document.querySelector(".home").classList.add("active");
           document.querySelector(".aboutus").classList.remove("active");
           document.querySelector(".provide").classList.remove("active");
           document.querySelector(".contact").classList.remove("active");
         }
         else if(aboutus.offsetTop <= windo && provide.offsetTop > windo){
           console.log("AboutUs");
           document.querySelector(".aboutus").classList.add("active");
           document.querySelector(".provide").classList.remove("active");
           document.querySelector(".contact").classList.remove("active");
           document.querySelector(".home").classList.remove("active");
         }
         else if(provide.offsetTop <= windo && contact.offsetTop > windo){
           console.log("Provide");
           document.querySelector(".provide").classList.add("active");
           document.querySelector(".contact").classList.remove("active");
           document.querySelector(".home").classList.remove("active");
           document.querySelector(".aboutus").classList.remove("active");
         }
         else if(contact.offsetTop <= windo){
           console.log("Contact");
           document.querySelector(".contact").classList.add("active");
           document.querySelector(".home").classList.remove("active");
           document.querySelector(".aboutus").classList.remove("active");
           document.querySelector(".provide").classList.remove("active");
         }
         else {
           console.log("Home");
         }
       });
    </script>
<script
      src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
      integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
      integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js"
      integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF"
      crossorigin="anonymous"
    ></script>
</body>

</html>