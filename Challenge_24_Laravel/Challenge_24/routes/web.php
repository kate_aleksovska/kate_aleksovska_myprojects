<?php

use App\Http\Controllers\FoodballMatchController;
use App\Http\Controllers\PlayerController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TeamController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::middleware(['auth'])->group(function() {
    Route::prefix('teams')->group(function() {
        Route::middleware(['admin'])->group(function() {
        Route::get('',            [TeamController::class, 'index'])->name('teams.index');
        Route::get('create',      [TeamController::class, 'create'])->name('teams.create');
        Route::post('',           [TeamController::class, 'store'])->name('teams.store');
        Route::get('{team}',      [TeamController::class, 'show'])->name('teams.show');
        Route::get('{team}',      [TeamController::class, 'edit'])->name('teams.edit');
        Route::put('{team}',      [TeamController::class, 'update'])->name('teams.update');
        Route::delete('{team}',   [TeamController::class, 'destroy'])->name('teams.destroy');


       });
    });

    Route::prefix('players')->group(function() {
        Route::middleware(['admin'])->group(function() {
        Route::get('',              [PlayerController::class, 'index'])->name('players.index');
        Route::get('create',        [PlayerController::class, 'create'])->name('players.create');
        Route::post('',             [PlayerController::class, 'store'])->name('players.store');
        Route::get('{player}',      [PlayerController::class, 'show'])->name('players.show');
        Route::get('{player}',      [PlayerController::class, 'edit'])->name('players.edit');
        Route::put('{player}',      [PlayerController::class, 'update'])->name('players.update');
        Route::delete('{player}',   [PlayerController::class, 'destroy'])->name('players.destroy');
       });
    });

    Route::prefix('matches')->group(function() {
        Route::get('',             [FoodballMatchController::class, 'index'])->name('matches.index');

        Route::middleware(['admin'])->group(function() {
        Route::get('create',       [FoodballMatchController::class, 'create'])->name('matches.create');
        Route::post('',            [FoodballMatchController::class, 'store'])->name('matches.store');
        Route::get('{match}',      [FoodballMatchController::class, 'show'])->name('matches.show');
        Route::get('{match}',      [FoodballMatchController::class, 'edit'])->name('matches.edit');
        Route::put('{match}',      [FoodballMatchController::class, 'update'])->name('matches.update');
        Route::delete('{match}',   [FoodballMatchController::class, 'destroy'])->name('matches.destroy');
       });
    });

});
require __DIR__.'/auth.php';
