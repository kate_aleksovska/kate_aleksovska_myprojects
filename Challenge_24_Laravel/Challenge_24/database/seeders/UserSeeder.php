<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->first_name = 'Kate';
        $user->last_name = 'Aleksovska';
        $user->age = 30;
        $user->email = 'kate@gmail.com';
        $user->role_id = Role::where('name', 'admin')->first()->id;
        $user->password = Hash::make('password');
        $user->save();
    }
}
