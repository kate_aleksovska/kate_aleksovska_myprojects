<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoodballmatchPlayerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foodballmatch_player', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('foodballmatch_id')->unsigned();
            $table->bigInteger('player_id')->unsigned();

            $table->foreign('foodballmatch_id')->references('id')->on('foodball_matches')->onDelete('cascade');
            $table->foreign('player_id')->references('id')->on('players')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foodballmatch_player');
    }
}
