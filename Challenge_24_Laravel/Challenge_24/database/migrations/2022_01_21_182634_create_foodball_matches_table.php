<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoodballMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foodball_matches', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('host_id')->unsigned();
            $table->bigInteger('guest_id')->unsigned();
            $table->string('result')->nullable();
            $table->date('match_date')->format('Y-m-d');
            $table->foreign('host_id')->references('id')->on('teams')->onDelete('cascade');
            $table->foreign('guest_id')->references('id')->on('teams')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foodball_matches');
    }
}
