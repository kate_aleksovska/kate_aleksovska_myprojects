<?php

namespace App\Http\Controllers;

use App\Models\Team;
use Illuminate\Http\Request;
use App\Models\FoodballMatch;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\MatchRequest;

class FoodballMatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $matches = FoodballMatch::all();
        return view('matches.index', compact('matches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teams = Team::all();
        return view('matches.create', compact('teams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MatchRequest $request)
    {   
        $teamGuest = Team::where('id', $request->guest)->first();
        $teamHost = Team::where('id', $request->host)->first();
        $playersGuest = [];
        $playersHost = [];
        foreach($teamGuest->players->toArray() as $key=>$value){
            array_push($playersGuest, $value['id']);
        }
        foreach($teamHost->players->toArray() as $key=>$value){
            array_push($playersHost, $value['id']);
        }
        $match = new FoodballMatch();
        $match->guest_id = $request->guest;
        $match->host_id = $request->host;
        $match->match_date = date('Y-m-d', strtotime($request->match_date));
       
        if(!$match->save()){
            return redirect()->route('matches.index')->with('error', 'Something bad happended!!');
        }  
          
        foreach($playersGuest as $key=>$value){
            DB::table('foodballmatch_player')->insert(['player_id' => $value,
                                                        'foodballmatch_id' => $match->id]);
        }
        foreach($playersHost as $key=>$value){
            DB::table('foodballmatch_player')->insert(['player_id' => $value,
                                                        'foodballmatch_id' => $match->id]);
        }
        return redirect()->route('matches.index')->with('success', "Match with {$match->belongsToHost->name} {$match->belongsToGuest->name} has been played!");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FoodballMatch
     * @return \Illuminate\Http\Response
     */
    public function show(FoodballMatch $match)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FoodballMatch
     * @return \Illuminate\Http\Response
     */
    public function edit(FoodballMatch $match)
    {
        $teams = Team::all();
        return view('matches.edit', compact('match', 'teams'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FoodballMatch
     * @return \Illuminate\Http\Response
     */
    public function update(MatchRequest $request, FoodballMatch $match)
    {
        $teamGuest = Team::where('id', $request->guest)->first();
        $teamHost = Team::where('id', $request->host)->first();
        $playersGuest = [];
        $playersHost = [];
        
        $players1 = [];
        $player = DB::table('foodballmatch_player')->where('foodballmatch_id', $match->id)->get();
         foreach($player as $key=>$value){
            //  array_push($players1, $value->player_id);
            $players1[$key] =$value->player_id;
         }

        
        foreach($teamGuest->players->toArray() as $key=>$value){
            array_push($playersGuest, $value['id']);
        }
        foreach($teamHost->players->toArray() as $key=>$value){
            array_push($playersHost, $value['id']);
        }
        $match->guest_id = $request->guest;
        $match->host_id = $request->host;
        $match->match_date = date('Y-m-d', strtotime($request->match_date));
       
        if(!$match->save()){
            return redirect()->route('matches.index')->with('error', 'Something bad happended!!');
        }  
            DB::table('foodballmatch_player')
              ->where('foodballmatch_id', $match->id)
              ->delete();
      
        foreach($playersGuest as $key=>$value){
            DB::table('foodballmatch_player')->insert(['player_id' => $value,
                                                        'foodballmatch_id' => $match->id]);
        }
        foreach($playersHost as $key=>$value){
            DB::table('foodballmatch_player')->insert(['player_id' => $value,
                                                        'foodballmatch_id' => $match->id]);
        }
        return redirect()->route('matches.index')->with('success', "Match with {$match->belongsToHost->name} {$match->belongsToGuest->name} has been updated!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FoodballMatch  
     * @return \Illuminate\Http\Response
     */
    public function destroy(FoodballMatch $match)
    {   
        DB::table('foodballmatch_player')
         ->where('foodballmatch_id', $match->id)->delete();
        if($match->delete())
        return redirect()->route('matches.index')->with('success', "Match has been deleted!!");

        return redirect()->route('matches.index')->with('error', 'Something bad happened!!');
    }
}
