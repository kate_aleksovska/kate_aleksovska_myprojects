<?php

namespace App\Http\Controllers;

use App\Models\Team;
use Illuminate\Http\Request;
use App\Http\Requests\TeamRequest;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = Team::all();
        return view('teams.index', compact('teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('teams.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeamRequest $request)
    {
        $team = new Team();
        $team->name = $request->name;
        $team->year_of_foundation = date('Y', strtotime($request->year));
        if(!$team->save()){
            return redirect()->route('teams.index')->with('error', 'Something bad happended!!');
        }
        return redirect()->route('teams.index')->with('success', "{$team->name} has been created!");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function show(Team $team)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function edit(Team $team)
    {

        return view('teams.edit', compact('team'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function update(TeamRequest $request, Team $team)
    {
        $team->name = $request->name;
        $team->year_of_foundation = date('Y', strtotime($request->year));
        if(!$team->save()){
            return redirect()->route('teams.index')->with('error', 'Something bad happended!!');
        }
        return redirect()->route('teams.index')->with('success', "{$team->name} has been updated!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function destroy(Team $team)
    {
        if($team->delete())
        return redirect()->route('teams.index')->with('success', "{$team->name} has been deleted!!");

        return redirect()->route('teams.index')->with('error', 'Something bad happened!!');
    }
}
