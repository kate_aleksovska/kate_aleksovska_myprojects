<?php

namespace App\Http\Controllers;

use App\Models\Team;
use App\Models\Player;
use Illuminate\Http\Request;
use App\Http\Requests\PlayerRequest;

class PlayerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $players = Player::all();
         return view('players.index', compact('players'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teams = Team::all();
        return view('players.create', compact('teams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PlayerRequest $request)
    {
        $player = new Player();
        $player->name = $request->name;
        $player->lastname = $request->lastname;
        $player->birth_date = date('Y-m-d', strtotime($request->birth_date));
        $player->team_id = $request->team;
        if(!$player->save()){
            return redirect()->route('players.index')->with('error', 'Something bad happended!!');
        }
        return redirect()->route('players.index')->with('success', "{$player->name} {$player->lastname} has been added as player!");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Player
     * @return \Illuminate\Http\Response
     */
    public function show(Player $team)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Player
     * @return \Illuminate\Http\Response
     */
    public function edit(Player $player)
    {
        $teams = Team::all();
        return view('players.edit', compact('player', 'teams'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Player
     * @return \Illuminate\Http\Response
     */
    public function update(PlayerRequest $request, Player $player)
    {
        $player->name = $request->name;
        $player->lastname = $request->lastname;
        $player->birth_date = date('Y-m-d', strtotime($request->birth_date));
        $player->team_id = $request->team;
        if(!$player->save()){
            return redirect()->route('players.index')->with('error', 'Something bad happended!!');
        }
        return redirect()->route('players.index')->with('success', "Player with name {$player->name} {$player->lastname} has been updated!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Player
     * @return \Illuminate\Http\Response
     */
    public function destroy(Player $player)
    {
        if($player->delete())
        return redirect()->route('players.index')->with('success', "{$player->name} has been deleted!!");

        return redirect()->route('players.index')->with('error', 'Something bad happened!!');
    }
}
