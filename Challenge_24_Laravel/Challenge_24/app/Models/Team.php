<?php

namespace App\Models;

use App\Models\Player;
use App\Models\FoodballMatch;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Team extends Model
{
    use HasFactory;

    public function players()
    {
        return $this->hasMany(Player::class);
    }
    
    public function host()
    {
        return $this->hasMany(FoodballMatch::class, 'host_id');
    }
    public function guest()
    {
        return $this->hasMany(FoodballMatch::class, 'guest_id');
    }
}
