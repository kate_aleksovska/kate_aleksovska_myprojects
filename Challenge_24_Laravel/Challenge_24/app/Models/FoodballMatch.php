<?php

namespace App\Models;

use App\Models\Team;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class FoodballMatch extends Model
{
    use HasFactory;
    public function belongsToHost()
    {
        return $this->belongsTo(Team::class, 'host_id', 'id');
    }

    public function belongsToGuest()
    {
        return $this->belongsTo(Team::class, 'guest_id', 'id');
    }

    public function players()
    {
        return $this->belongsToMany(Player::class);
    }
    public function match()
    {
        return $this->belongsTo(FoodballMatch::class);
    }
}
