<?php

namespace App\Console\Commands;

use App\Models\FoodballMatch;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CreateResultsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:results';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adding random results in matches';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {   
        $host = rand(0,10);
        $guest = rand(0,10);
        DB::table('foodball_matches')
        ->where('match_date','<=', date('Y-m-d H:i:s', strtotime('-1 day')))
        ->where('result', NULL)
        ->update(['result'=>"{$host}:$guest"]);
        // return 0;
    }
}
