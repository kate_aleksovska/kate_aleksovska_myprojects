@extends('layouts.master')

@section('content')
<div class="card m-5">
    <div class="card-header">
        <p class="inline-block">Create new match</p>
    </div>
    <div class="card-body">
        <form action="{{route('matches.store')}}" method="post">
            @csrf
            <div class="form-group">
                <label for="host">Home Team</label>
                <select class="form-control" id="host" name="host" name="host" value={{old('host')}}>
                    @foreach ($teams as $team)
                        <option value="{{ $team->id }}">{{ $team->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="guest">Guest Team</label>
                <select class="form-control" id="guest" name="guest" name="guest" value={{old('guest')}}>
                    @foreach ($teams as $team)
                        <option value="{{ $team->id }}">{{ $team->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="match_date">Match date</label>
                <input type="date" class="form-control @error('match_date') is-invalid @enderror" id="match_date" name="match_date" value={{old('match_date')}}>
                @error('match_date')
                  <div class="invalid-feedback">
                      {{$message}}
                  </div>
              @enderror
              </div>
         <button class="btn btn-success my-2">Save</button>
        </form>
    </div>
</div>
@endsection