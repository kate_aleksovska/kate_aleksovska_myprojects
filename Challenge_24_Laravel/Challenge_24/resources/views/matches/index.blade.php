@extends('layouts.master')

@section('content')
<div class="card m-5">
    <div class="card-header d-flex justify-content-between">
        <p class="inline-block">Matches</p>
        @if (Auth::user()->is_admin())
                  <a href="{{route('matches.create')}}" class="btn btn-primary inline-block">Add new Match</a>
              @endif
    </div>
    <div class="card-body">
        @if (Session::has('success'))
            <div class="alert alert-success">
                {{Session::get('success')}}
            </div>
        @elseif (Session::has('error'))
            <div class="alert alert-danger">
                {{Session::get('danger')}}
            </div>
        @endif
    <table class="table">
        <thead>
            <tr>
                <th>Team 1</th>
                <th>Team 2</th>
                <th>Results</th>
                <th>Match Date</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
            @foreach ($matches as $match)
            <tr>
                <td>{{$match->belongsToHost->name}}</td>
                <td>{{$match->belongsToGuest->name}}</td>
                <td>
                @if ($match->result == NULL)
                    /  
                @else
                    {{$match->result}}
                @endif    
                </td>
                <td>{{$match->match_date}}</td>
                @if (Auth::user()->is_admin())
                <td><a href="{{route('matches.edit', $match->id)}}" class=" btn btn-link text-success">Edit</a></td>
                <td> 
                    <form action="{{route('matches.destroy', $match->id)}}" method="POST">
                        @csrf
                        @method('DELETE')
                    <button class="btn btn-link text-danger">Delete</button>
                </form>
                </td>
                @endif
            </tr>
            @endforeach
        </tbody>
    </table>
    </div>
</div>
@endsection