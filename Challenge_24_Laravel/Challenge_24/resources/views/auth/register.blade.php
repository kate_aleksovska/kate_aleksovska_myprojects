<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <form method="POST" action="{{ route('register') }}">
            @csrf

             <!--First Name -->
             <div>
                <x-label for="first_name" :value="__('First Name')" />

                <x-input id="first_name" class="block mt-1 w-full" type="text" name="first_name" :value="old('first_name')" autofocus />
                @error('first_name')
                <p class="mt-3 list-disc list-inside text-sm text-red-600">{{$message}}</p>
            @enderror
            </div>
            <!-- Last Name -->
            <div>
                <x-label for="last_name" :value="__('Last Name')" />

                <x-input id="last_name" class="block mt-1 w-full" type="text" name="last_name" :value="old('last_name')" autofocus />
                @error('last_name')
                <p class="mt-3 list-disc list-inside text-sm text-red-600">{{$message}}</p>
            @enderror
            </div>
            <!-- Email Address -->
            <div class="mt-4">
                <x-label for="email" :value="__('Email')" />

                <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" />
                @error('email')
                <p class="mt-3 list-disc list-inside text-sm text-red-600">{{$message}}</p>
            @enderror
            </div>
             <!-- Age -->
            <div class="mt-4">
                <x-label for="age" :value="__('age')" />

                <x-input id="age" class="block mt-1 w-full" type="number" name="age" :value="old('age')" />
                @error('age')
                <p class="mt-3 list-disc list-inside text-sm text-red-600">{{$message}}</p>
            @enderror
            </div>
            <!-- Password -->
            <div class="mt-4">
                <x-label for="password" :value="__('Password')" />

                <x-input id="password" class="block mt-1 w-full"
                                type="password"
                                name="password"
                                autocomplete="new-password" />
                                @error('password')
                        <p class="mt-3 list-disc list-inside text-sm text-red-600">{{$message}}</p>
            @enderror
                                
            </div>

            <!-- Confirm Password -->
            <div class="mt-4">
                <x-label for="password_confirmation" :value="__('Confirm Password')" />

                <x-input id="password_confirmation" class="block mt-1 w-full"
                                type="password"
                                name="password_confirmation"/>
            </div>

            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>

                <x-button class="ml-4">
                    {{ __('Register') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>
