@extends('layouts.master')

@section('content')
<div class="card m-5">
    <div class="card-header">
        <p class="inline-block">Create new team</p>
    </div>
    <div class="card-body">
        <form action="{{route('teams.update', $team->id)}}" method="post">
            @csrf
            @method('PUT')
            <div class="form-group">
              <label for="name">Name</label>
              <input type="name" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value={{old('name', $team->name)}}>
              @error('name')
                  <div class="invalid-feedback">
                      {{$message}}
                  </div>
              @enderror
            </div>
            <div class="form-group">
                <label for="year">Year</label>
                <input type="text" class="form-control @error('year') is-invalid @enderror" id="year" name="year" value={{old('year', $team->year_of_foundation)}}>
                @error('year')
                  <div class="invalid-feedback">
                      {{$message}}
                  </div>
              @enderror
              </div>
         <button class="btn btn-success my-2">Save</button>
        </form>
    </div>
</div>
@endsection