@extends('layouts.master')

@section('content')
<div class="card m-5">
    <div class="card-header d-flex justify-content-between">
        <p class="inline-block">Teams</p>
        <a href="{{route('teams.create')}}" class="btn btn-primary inline-block">Add new Team</a>
    </div>
    <div class="card-body">
        @if (Session::has('success'))
            <div class="alert alert-success">
                {{Session::get('success')}}
            </div>
        @elseif (Session::has('error'))
            <div class="alert alert-danger">
                {{Session::get('danger')}}
            </div>
        @endif
    <table class="table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Year founded</th>
                <th>Players in team</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($teams as $team)
            <tr>
                <td>{{$team->name}}</td>
                <td>{{$team->year_of_foundation}}</td>

                <td>
                    @foreach ($team->players as $player)
                        {{$player->name.' '.$player->lastname}}</br>
                    @endforeach
                </td>
                <td><a href="{{route('teams.edit', $team->id)}}" class=" btn btn-link text-success">Edit</a></td>
                <td> 
                    <form action="{{route('teams.destroy', $team->id)}}" method="POST">
                        @csrf
                        @method('DELETE')
                    <button class="btn btn-link text-danger">Delete</button>
                </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    </div>
</div>
@endsection