@extends('layouts.master')

@section('content')
<div class="card m-5">
    <div class="card-header">
        <p class="inline-block">Create new player</p>
    </div>
    <div class="card-body">
        <form action="{{route('players.store')}}" method="post">
            @csrf
            <div class="form-group">
              <label for="name">Name</label>
              <input type="name" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value={{old('name')}}>
              @error('name')
                  <div class="invalid-feedback">
                      {{$message}}
                  </div>
              @enderror
            </div>
            <div class="form-group">
                <label for="lastname">Surname</label>
                <input type="lastname" class="form-control @error('lastname') is-invalid @enderror" id="lastname" name="lastname" value={{old('lastname')}}>
                @error('lastname')
                    <div class="invalid-feedback">
                        {{$message}}
                    </div>
                @enderror
              </div>
            <div class="form-group">
                <label for="birth_date">Birth date</label>
                <input type="date" class="form-control @error('birth_date') is-invalid @enderror" id="birth_date" name="birth_date" value={{old('birth_date')}}>
                @error('birth_date')
                  <div class="invalid-feedback">
                      {{$message}}
                  </div>
              @enderror
              </div>
              <div class="form-group">
                <label for="team">Team</label>
                <select class="form-control" id="team" name="team" name="team" value={{old('team')}}>
                    @foreach ($teams as $team)
                        <option value="{{ $team->id }}">{{ $team->name }}</option>
                    @endforeach
                </select>
            </div>
         <button class="btn btn-success my-2">Save</button>
        </form>
    </div>
</div>
@endsection