@extends('layouts.master')

@section('content')
<div class="card m-5">
    <div class="card-header d-flex justify-content-between">
        <p class="inline-block">Players</p>
        <a href="{{route('players.create')}}" class="btn btn-primary inline-block">Add new Player</a>
    </div>
    <div class="card-body">
        @if (Session::has('success'))
            <div class="alert alert-success">
                {{Session::get('success')}}
            </div>
        @elseif (Session::has('error'))
            <div class="alert alert-danger">
                {{Session::get('danger')}}
            </div>
        @endif
    <table class="table text-center">
        <thead>
            <tr>
                <th>Name</th>
                <th>Last Name</th>
                <th>Date of birth</th>
                <th>Team that playes</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
          @if (count($players))
          @foreach ($players as $player)
          <tr>
              <td>{{$player->name}}</td>
              <td>{{$player->lastname}}</td>
              <td>{{$player->birth_date}}</td>
              <td>{{$player->team->name}}</td>
              f
              <td><a href="{{route('players.edit', $player->id)}}" class=" btn btn-link text-success">Edit</a></td>
              <td> 
                  <form action="{{route('players.destroy', $player->id)}}" method="POST">
                      @csrf
                      @method('DELETE')
                  <button class="btn btn-link text-danger">Delete</button>
              </form>
              </td>
          </tr>
          @endforeach
          @endif  
        </tbody>
    </table>
    </div>
</div>
@endsection