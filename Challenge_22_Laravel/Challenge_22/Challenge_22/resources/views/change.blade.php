@extends('products')
@section('title', 'Change')
@section('add-change')
    <div class="container bg-light">
        <div class="row">
            <div class="col-sm-6 offset-sm-3 col-12">
                @if (Session::has('success'))
                    <div class="alert alert-success text-center">
                        {{ Session::get('success') }}
                    </div>
                @endif
                @if (Session::has('error'))
                    <div class="alert alert-danger text-center">
                        {{ Session::get('error') }}
                    </div>
                @endif
            </div>
        </div>
        <div class="row justify-content-center">

            @foreach ($products as $product)
                <div class="p-1 col-12 col-sm-6 d-flex justify-content-center col-lg-4">
                    <div class="card  text-center" style="width: 18rem" data-target="product{{ $product->id }}"
                        data-target2="form{{ $product->id }}">
                        <img src="{{ $product->url }}" class="card-img-top p-5" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">{{ $product->name }}</h5>
                            <h5 class="card-text">{{ $product->subtitle }}</h5>
                            <p class="card-text">{{ $product->description }}</p>
                            <div class="card-body" style="display: block" id="allow">
                                <button type="submit" class="btn btn-outline-secondary" id="product{{ $product->id }}"><i
                                        class="fas fa-edit fa-2x"></i></button>
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-outline-secondary" data-toggle="modal"
                                    data-target="#exampleModal{{ $product->id }}">
                                    <i class="fas fa-times fa-2x text-dark"></i>
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal{{ $product->id }}" tabindex="-1"
                                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Избриши</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                Дали сакате да го избришете продуктот со име {{ $product->name }}
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Откажи</button>
                                                <form action="{{ route('change.destroy', $product->id) }}" method="POST"
                                                    class="d-inline-block">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-warning">Избриши</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- <form action="{{route('change.destroy',$product->id)}}" method="POST" class="d-inline-block">
                      @csrf
                      @method('DELETE')
                      <button type="submit" class="btn btn-outline-secondary"><i class="fas fa-times fa-2x text-dark"></i></button>
                  </form> --}}
                            </div>
                        </div>
                    </div>
                    <form class="p-3 col-12" style="display: none" novalidate
                        action="{{ route('products.update', $product->id) }}" method="POST"
                        data-type='product{{ $product->id }}' data-type2='form{{ $product->id }}'>
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <p>Измени го продуктот</p>
                        </div>
                        <div class="form-group">
                            <label for="name">Име</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name"
                                aria-describedby="name" name="name" value="{{ old('name', $product->name) }}">
                            @error('name')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="subtitle">Поднаслов</label>
                            <input type="text" class="form-control  @error('subtitle') is-invalid @enderror" id="subtitle"
                                name="subtitle" value="{{ old('subtitle', $product->subtitle) }}">
                            @error('subtitle')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="picture">Слика</label>
                            <input type="text" class="form-control @error('picture') is-invalid @enderror" id="picture"
                                name="picture" value="{{ old('picture', $product->picture) }}">
                            @error('picture')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="url">URL</label>
                            <input type="text" class="form-control @error('url') is-invalid @enderror"" id=" url" name="url"
                                value="{{ old('url', $product->url) }}">
                            @error('url')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="description">Опис</label>
                            <textarea class="form-control @error('description') is-invalid @enderror" id="description"
                                rows="3" name="description"
                                value="{{ old('description', $product->description) }}"></textarea>
                            @error('description')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-warning btn-lg btn-block">Зачувај</button>
                        <div class="btn btn-warning btn-lg btn-block" id="form{{ $product->id }}">Откажи</div>
                    </form>

                </div>
            @endforeach
        </div>
    </div>

@endsection
<script>

</script>
