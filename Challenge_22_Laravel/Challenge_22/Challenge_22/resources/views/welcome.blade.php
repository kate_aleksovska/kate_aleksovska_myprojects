@extends('layouts.master')
@section('title', 'Brainster')
@section('admin')
<a type="button" class="btn btn-info mx-1" href="{{route('admin.index')}}" >
    <span class="btn-text">Log in admin</span>
 </a> 
@endsection

@section('content')
    <div class="container-fluid bg-image align-items-center">
        <div class="row">
            <div class="col d-flex align-items-center justify-content-center">
                <div class="row">
                    <div>
                        @if (Session::has('success'))
                        <div class="alert alert-success text-center">
                            {{ Session::get('success') }}
                        </div>
                    @endif
                    @if (Session::has('error'))
                        <div class="alert alert-danger text-center">
                            {{ Session::get('error') }}
                        </div>
                    @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container bg-light">
        <div class="row justify-content-center">

                @foreach ($products as $product)
            <div class="p-1 col-12 col-sm-6 col-lg-4 d-flex justify-content-center">
                <div class="card  text-center" style="width: 18rem;">
                    <img src="{{$product->url}}" class="card-img-top p-5" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">{{$product->name}}</h5>
                      <h5 class="card-text">{{$product->subtitle}}</h5>
                      <p class="card-text">{{$product->description}}</p>
                    </div>
                  </div>
            </div>
                @endforeach
        </div>
    </div>
@endsection
    
