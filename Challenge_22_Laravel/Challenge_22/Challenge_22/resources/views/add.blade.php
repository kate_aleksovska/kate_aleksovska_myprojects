@extends('products')
@section('title', 'Add')
@section('add-change')
    
<div class="row">
    <div class="h4">Додај нов производ:</div>
</div>
<div class="row">
    <div class="col-sm-6 col-12">
        @if (Session::has('success'))
        <div class="alert alert-success text-center">
            {{ Session::get('success') }}
        </div>
    @endif
    @if (Session::has('error'))
        <div class="alert alert-danger text-center">
            {{ Session::get('error') }}
        </div>
    @endif
    </div>
</div>
<div class="row justify-content-center">
    <form class="col-sm-6 col-12" novalidate action="{{route('add.store')}}" method="POST">
        @csrf
        <div class="form-group">
          <label for="name">Име</label>
          <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" aria-describedby="name" name="name" value="{{ old('name')}}">
          @error('name')
          <div class="invalid-feedback">
              {{ $message }}
          </div>
        @enderror
        </div>
        <div class="form-group">
          <label for="subtitle">Поднаслов</label>
          <input type="text" class="form-control  @error('subtitle') is-invalid @enderror" id="subtitle" name="subtitle" value="{{ old('subtitle')}}">
          @error('subtitle')
          <div class="invalid-feedback">
              {{ $message }}
          </div>
        @enderror
        </div>
        <div class="form-group">
            <label for="picture">Слика</label>
            <input type="text" class="form-control @error('picture') is-invalid @enderror"" id="picture" name="picture" value="{{ old('picture')}}">
            @error('picture')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
          @enderror
          </div>
          <div class="form-group">
            <label for="url">URL</label>
            <input type="text" class="form-control @error('url') is-invalid @enderror" id="url" name="url" value="{{ old('url')}}">
            @error('url')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
          @enderror
          </div>
          <div class="form-group">
            <label for="description">Опис</label>
            <textarea class="form-control @error('description') is-invalid @enderror" id="description" rows="3" name="description" value="{{old('description')}}"></textarea>
            @error('description')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
          @enderror
          </div>
        <button type="submit" class="btn btn-warning btn-lg btn-block">Испрати</button>
      </form>
</div>
@endsection

