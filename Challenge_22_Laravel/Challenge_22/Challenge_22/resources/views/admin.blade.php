@extends('layouts.master')
@section('title', 'Admin')
@section('content')
    <div class="container-fluid bg-height d-flex flex-column justify-content-center">
        <div class="row">

            <div class="col-6 offset-3">
                @if (Session::has('success'))
                    <div class="alert alert-success text-center">
                        {{ Session::get('success') }}
                    </div>
                @endif
                @if (Session::has('error'))
                    <div class="alert alert-danger text-center">
                        {{ Session::get('error') }}
                    </div>
                @endif
                <form novalidate method="POST" action="{{ route('admin.store') }}">
                    @csrf
                    <div class="form-group">
                        <label for="emailAdmin">Е-маил</label>
                        <input type="email" class="form-control @error('emailAdmin') is-invalid @enderror" id="emailAdmin"
                            aria-describedby="emailHelp" name="emailAdmin" value="{{ old('emailAdmin') }}" placeholder="katea911907@gmail.com">
                        @error('emailAdmin')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="passAdmin">Пасворд</label>
                        <input type="text" class="form-control @error('passAdmin') is-invalid @enderror" id="passAdmin"
                            name="passAdmin" value="{{ old('passAdmin') }}" placeholder="password">
                        @error('passAdmin')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-warning btn-lg btn-block">Логирај се</button>
                </form>
            </div>
        </div>
    </div>
@endsection
