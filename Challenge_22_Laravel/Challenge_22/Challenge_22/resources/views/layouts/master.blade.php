<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    @include('layouts.head')

    <!-- Bootstrap CSS -->
    @include('layouts.style')
    <title>@yield('title')</title>
  </head>
  <body>
@include('layouts.navbar')
      
@yield('content')




@include('layouts.footer')
@include('layouts.scripts')
  </body>
</html>