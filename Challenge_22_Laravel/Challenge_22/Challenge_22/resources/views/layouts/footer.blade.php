<footer class="container-fluid bg-light text-center py-2 py-sm-3 text-secondary m-0 p-0">
    <p class="mb-0 d-inline-block h5">Made with ❤</i>  by 
<div class="d-inline-block">
        <a class="navbar-brand d-lg-flex" href="index.html">
        <div class="d-flex">
          <div class=" ml-lg-3">
            <div class="text-center">
          <span class="width-25"><img src="{{ asset('images/logo.png') }}" alt="logo-png" class="img-logo"></span>
          <p class="h6 logo-text text-dark">BRAINSTER</p>
        </div>
        </div>
      </div>
      </a>
    </div> <span class="h5 mx-2"><a href="https://brainster.co/" class="text-success">-Say Hi</a></span><span class="h5">-Terms<span></p>
</footer>