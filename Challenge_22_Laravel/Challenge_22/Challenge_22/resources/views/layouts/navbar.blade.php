
<nav class="navbar navbar-expand-md navbar-light bg-warning fixed-top">
    <a class="navbar-brand d-lg-flex" href="#">
        <div class="d-flex">
          <div class=" ml-lg-3">
            <div class="text-center">
          <span class="width-25"><img src="{{ asset('images/logo.png') }}" alt="logo-png" class="img-logo"></span>
          <p class="h6 logo-text">BRAINSTER</p>
        </div>
        </div>
      </div>
      </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-around" id="navbarSupportedContent">
        <ul class="navbar-nav ml-0 d-flex">
            <li class="nav-item active px-lg-1 my-2">
                <a class="nav-link d-inline-block badge-pill mx-2 text-center" href="https://marketpreneurs.brainster.co"><big>Академија за маркетинг</big><span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active px-lg-2 my-2">
                <a class="nav-link d-inline-block badge-pill mx-2 text-center" href="https://codepreneurs.brainster.co"><big>Академија за програмирање</big><span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active px-lg-1 my-2">
                <a class="nav-link d-inline-block badge-pill mx-2 text-center" href="https://datascience.brainster.co"><span class="text-center"><big>Академија за data science</big></span><span class="sr-only">(current)</span></a>
            </li><li class="nav-item active px-lg-1 my-2">
                <a class="nav-link d-inline-block badge-pill mx-2 text-center" href="https://design.brainster.co"><big>Академија за дизајн</big><span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active px-lg-1 my-2">
                <a class="nav-link d-inline-block badge-pill mx-2 text-center" href="#"><big>Блог</big><span class="sr-only">(current)</span></a>
            </li>
        </ul>
        <!-- Button trigger modal -->
<button type="button" class="btn btn-info mx-1" data-toggle="modal" data-target="#basicExample" >
<span class="btn-text">Вработи наш студент</span>  
</button>
@yield('admin')
  
    </div>
  </nav>
  <!-- Modal vraboti-->
  <div class="modal fade" id="basicExample" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel">Вработете наш студент</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <p class="text-muted">Внесете Ваши информации за да стапиме во контакт</p>
            <form novalidate action="{{route('students.store')}}" method="POST">
              @csrf
                <div class="form-group">
                  <label for="email">Е-маил</label>
                  <input type="email" class="form-control  @error('email') is-invalid @enderror" id="exampleInputEmail1" aria-describedby="emailHelp" name="email" value="{{ old('email')}}">
                  @error('email')
                  <div class="invalid-feedback">
                      {{ $message }}
                  </div>
                @enderror
                </div>
                <div class="form-group">
                  <label for="phone">Телефоон</label>
                  <input type="text" class="form-control  @error('phone') is-invalid @enderror" id="phone" name="phone" value="{{ old('phone')}}">
                  @error('phone')
                  <div class="invalid-feedback">
                      {{ $message }}
                  </div>
                @enderror
                </div>
                <div class="form-group">
                    <label for="company">Компанија</label>
                    <input type="text" class="form-control  @error('company') is-invalid @enderror" id="company" name="company" value="{{ old('company')}}">
                    @error('company')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                  @enderror
                  </div>
                <button type="submit" class="btn btn-warning btn-lg btn-block">Испрати</button>
              </form>
        </div>
      </div>
    </div>
  </div>



