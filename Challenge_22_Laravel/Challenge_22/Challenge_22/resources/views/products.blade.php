@extends('layouts.master')
@section('title', 'Product')
@section('content')
   <div class="container {{ Route::currentRouteName() == 'add.index' || Route::currentRouteName() == 'products.index' ? 'bg-height' : '' }}">
     <div class="row">
         <div class="col">
            <nav class="navbar navbar-expand navbar-light bg-light">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item mx-2 {{ Route::currentRouteName() == 'add.index' ? 'active' : '' }}">
                            <a class="nav-link" href="{{route('add.index')}}">Додај<span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item mx-2 {{ Route::currentRouteName() == 'change.index' ? 'active' : '' }}">
                            <a class="nav-link" href="{{route('change.index')}}">Измени</a>
                        </li>
                </div>
            </nav>
         </div>
     </div>
     @yield('add-change')
   </div>
@endsection
