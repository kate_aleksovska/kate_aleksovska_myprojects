<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Requests\StudentFormRequest;

class StudentController extends Controller
{

    public function store(StudentFormRequest $request)
    {
        $student = new Student();
        $student->email = $request->email;
        $student->phone = $request->phone;
        $student->company=$request->company;
        if($student->save())
        {
        $to      = $request->email;
        $subject = "Благораиме за интересот за вработување на студент од академијата Бреинстер";
        $message = "Здраво,
    
        Благодариме на интересот што го пројавивте за вработување на студент од академијата Браинстер, 
        ќе ве контактираме во најскоро време за подетални информации.
        
        Благодариме на довербата,
        
        Ваш Браинстер";
        $headers = 'From: brainstertest@gmail.com' . "\r\n";
        if(mail($to, $subject, $message, $headers)) {
              return redirect()->route('/')->with('success', 'Благодариме на интересот, ќе ве контактираме во најскоро време!!');
              return redirect()->route('/')->with('error', 'Грешка е настаната, пробајте повторно!!');
            }
        }
    }
}
