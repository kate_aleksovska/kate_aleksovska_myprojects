<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddFormRequest;
use App\Models\Product;

class AddController extends Controller
{
    public function index()
    {
        return view('add');
    }
    public function store(AddFormRequest $request)
    {  
        $product = new Product();
        $product->name = $request->name;
        $product->subtitle = $request->subtitle;
        $product->picture=$request->picture;
        $product->url = $request->url;
        $product->description = $request->description;
        if($product->save())
              return redirect()->route('add.index')->with('success', 'Продуктот со име'.$request->name.' успешно е додаден!!');
              return redirect()->route('add.index')->with('error', 'Продуктот со име'.$request->name.' не е додаден!!');
    }
}
