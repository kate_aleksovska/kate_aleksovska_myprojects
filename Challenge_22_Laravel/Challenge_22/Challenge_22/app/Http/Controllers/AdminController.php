<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\AdminFormRequest;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin');
    }
    public function store(AdminFormRequest $request)
    {  
        $passAdmin = $request->passAdmin;
        $emailAdmin = $request->emailAdmin;
        $email = Admin::pluck('email');
        $password = Admin::pluck('password');
     
       if (!Hash::check($passAdmin, $password[0]) || $email[0] != $emailAdmin) {
            return redirect()->route('admin.index')->with('error', 'wrong username and password combination!');
       }
       return redirect()->route('products.index');
    }
}
