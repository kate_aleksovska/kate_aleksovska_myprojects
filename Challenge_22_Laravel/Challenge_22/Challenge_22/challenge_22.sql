-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 27, 2021 at 12:19 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `challenge_22`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'katea911807@gmail.com', '$2y$10$VzIB5yw1DSuR5Kw5t4Chwuep2SMX/TPw5.8sWQSWx7ZyBqOcA5pPC', '2021-12-25 12:49:59', '2021-12-25 12:49:59');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(12, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(13, '2021_12_25_033817_create_admins_table', 1),
(14, '2021_12_25_035026_create_students_table', 1),
(15, '2021_12_25_035330_create_products_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitle` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `subtitle`, `picture`, `url`, `description`, `created_at`, `updated_at`) VALUES
(17, 'Product 1', 'Product 1 subtitle', 'https://picsum.photos/id/1/200/300', 'https://picsum.photos/id/1/200/300', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quasi autem sequi perferendis adipisci dolore ex ea facere magnam labore repudiandae!', '2021-12-26 22:17:49', '2021-12-26 22:17:49'),
(18, 'Product 2', 'Product 2 subtitle', 'https://picsum.photos/id/2/200/300', 'https://picsum.photos/id/2/200/300', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quasi autem sequi perferendis adipisci dolore ex ea facere magnam labore repudiandae!', '2021-12-26 22:17:49', '2021-12-26 22:17:49'),
(19, 'Product 3', 'Product 3 subtitle', 'https://picsum.photos/id/3/200/300', 'https://picsum.photos/id/3/200/300', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quasi autem sequi perferendis adipisci dolore ex ea facere magnam labore repudiandae!', '2021-12-26 22:17:49', '2021-12-26 22:17:49'),
(20, 'Product 4', 'Product 4 subtitle', 'https://picsum.photos/id/4/200/300', 'https://picsum.photos/id/4/200/300', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quasi autem sequi perferendis adipisci dolore ex ea facere magnam labore repudiandae!', '2021-12-26 22:17:49', '2021-12-26 22:17:49'),
(21, 'Product 5', 'Product 5 subtitle', 'https://picsum.photos/id/5/200/300', 'https://picsum.photos/id/5/200/300', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quasi autem sequi perferendis adipisci dolore ex ea facere magnam labore repudiandae!', '2021-12-26 22:17:49', '2021-12-26 22:17:49'),
(22, 'Product 6', 'Product 2 subtitle', 'https://picsum.photos/id/6/200/300', 'https://picsum.photos/id/6/200/300', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quasi autem sequi perferendis adipisci dolore ex ea facere magnam labore repudiandae!', '2021-12-26 22:17:49', '2021-12-26 22:17:49');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `email`, `phone`, `company`, `created_at`, `updated_at`) VALUES
(1, 'katea911807@gmail.com', '0038972123453', 'Vox Taneo', '2021-12-26 01:02:33', '2021-12-26 01:02:33'),
(2, 'katea911807@gmail.com', '0038972123453', 'Vox Taneo', '2021-12-26 01:03:59', '2021-12-26 01:03:59'),
(3, 'katea911807@gmail.com', '0038972123453', 'Vox Taneo', '2021-12-26 15:40:05', '2021-12-26 15:40:05'),
(4, 'katea911807@gmail.com', '0038972123453', 'Vox Taneo', '2021-12-26 15:53:33', '2021-12-26 15:53:33'),
(5, 'katea911807@gmail.com', 'dfhdhdgh', 'Vox Taneo', '2021-12-26 21:51:55', '2021-12-26 21:51:55'),
(6, 'katea911807@gmail.com', '0038972123453', 'Vox Taneo', '2021-12-26 21:53:37', '2021-12-26 21:53:37'),
(7, 'katea911807@gmail.com', '0038972123453', 'Vox Taneo', '2021-12-26 21:54:48', '2021-12-26 21:54:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
