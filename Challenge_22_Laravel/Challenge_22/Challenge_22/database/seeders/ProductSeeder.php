<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        //product 1
        $product1 = new Product();
        $product1->name = 'Product 1';
        $product1->subtitle = 'Product 1 subtitle';
        $product1->picture = 'https://picsum.photos/id/1/200/300';
        $product1->url = 'https://picsum.photos/id/1/200/300';
        $product1->description = 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quasi autem sequi perferendis adipisci dolore ex ea facere magnam labore repudiandae!';
        $product1->save();
        //product 2
        $product2 = new Product();
        $product2->name = 'Product 2';
        $product2->subtitle = 'Product 2 subtitle';
        $product2->picture = 'https://picsum.photos/id/2/200/300';
        $product2->url = 'https://picsum.photos/id/2/200/300';
        $product2->description = 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quasi autem sequi perferendis adipisci dolore ex ea facere magnam labore repudiandae!';
        $product2->save();
        //product 3
        $product3 = new Product();
        $product3->name = 'Product 3';
        $product3->subtitle = 'Product 3 subtitle';
        $product3->picture = 'https://picsum.photos/id/3/200/300';
        $product3->url = 'https://picsum.photos/id/3/200/300';
        $product3->description = 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quasi autem sequi perferendis adipisci dolore ex ea facere magnam labore repudiandae!';
        $product3->save();
        //product 4
        $product4 = new Product();
        $product4->name = 'Product 4';
        $product4->subtitle = 'Product 4 subtitle';
        $product4->picture = 'https://picsum.photos/id/4/200/300';
        $product4->url = 'https://picsum.photos/id/4/200/300';
        $product4->description = 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quasi autem sequi perferendis adipisci dolore ex ea facere magnam labore repudiandae!';
        $product4->save();
        //product 5
        $product5 = new Product();
        $product5->name = 'Product 5';
        $product5->subtitle = 'Product 5 subtitle';
        $product5->picture = 'https://picsum.photos/id/5/200/300';
        $product5->url = 'https://picsum.photos/id/5/200/300';
        $product5->description = 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quasi autem sequi perferendis adipisci dolore ex ea facere magnam labore repudiandae!';
        $product5->save();
        //product 6
        $product6 = new Product();
        $product6->name = 'Product 6';
        $product6->subtitle = 'Product 2 subtitle';
        $product6->picture = 'https://picsum.photos/id/6/200/300';
        $product6->url = 'https://picsum.photos/id/6/200/300';
        $product6->description = 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quasi autem sequi perferendis adipisci dolore ex ea facere magnam labore repudiandae!';
        $product6->save();  
    }
}
