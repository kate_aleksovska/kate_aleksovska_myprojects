<?php

use App\Models\Product;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AddController;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ChangeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\StudentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/seed',function(){
      Artisan::call('db:seed --class=ProductSeeder');
      $products = Product::get();
    return view('welcome', compact('products'));
});
Route::get('/', function () {
    $products = Product::get();
    return view('welcome', compact('products'));
})->name('/');
Route::get('admin', [AdminController::class, 'index'])->name('admin.index');
Route::post('admin', [AdminController::class, 'store'])->name('admin.store');
Route::resource('products', ProductController::class);
Route::get('add',[AddController::class, 'index'])->name('add.index');
Route::post('add',[AddController::class, 'store'])->name('add.store');
Route::post('students',[StudentController::class, 'store'])->name('students.store');
Route::get('change',[ChangeController::class, 'index'])->name('change.index');
Route::delete('change/{id}', [ChangeController::class, 'destroy'])->name('change.destroy');

// Route::get('products', [ProductController::class, 'index'])->name('products.index');
// Route::get('products/create', [ProductController::class, 'product'])->name('products.create');
// Route::post('products', [ProductController::class, 'store'])->name('products.store');
// Route::get('products/{id}', [ProductController::class, 'show'])->name('products.show');
// Route::get('products/{id}/edit', [ProductController::class, 'edit'])->name('products.edit');
// Route::put('products/{id}', [ProductController::class, 'update'])->name('products.update');
// Route::delete('products/{id}', [ProductController::class, 'destroy'])->name('products.destroy');
