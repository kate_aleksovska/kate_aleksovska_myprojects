<?php

use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [UsersController::class, 'index'])->name('home');

Route::get('/login', [UsersController::class, 'create'])->name('login');

Route::post('/user', [UsersController::class, 'store'])->name('user');

Route::get("/logout", [UsersController::class, "logout"])->name('logout');
