<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ url('css/style.css') }} ">
    <title>Login</title>
  </head>
  <body class="m-0 p-0">
    <p class="display-2 text-center text-white my-5">BUSINESS CASUAL</p>
    <nav class="text-center text-white m-0 p-0">
        <ul class="d-flex flex-row justify-content-center align-items-center list-unstyled w-100">
            <a href="{{route('home')}}" class="btn btn-link text-decoration-none text-white"><li class="mx-4 fs-4">HOME</li></a>
            <a href="{{route('login')}}" class="btn btn-link text-decoration-none text-warning"><li class="mx-4 fs-4">LOG IN</li></a>
        </ul>
    </nav>
        <div class="row d-flex justify-content-center align-items-center m-0 p-0">
            <div class="col-8 text-white fw-bold fs-5">
                <form action="{{ route('user') }}" method="POST">
                    @csrf
                    <div class="mb-3">
                        <label for="firstname" class="form-label">First Name</label>
                        <input type="text" id="firstname" name="firstname" class="form-control @error('firstname') is-invalid @enderror" value="{{old('firstname')}}">
                        @error('firstname')
                        <div class="invalid-feedback alert alert-danger text-danger p-1">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label for="lastname" class="form-label">Last Name</label>
                        <input type="text" id="lastname" name="lastname" class="form-control @error('lastname') is-invalid @enderror"  value="{{old('lastname')}}">
                        @error('lastname')
                        <div class="invalid-feedback alert alert-danger text-danger p-1">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label for="email" class="form-label">Email</label>
                        <input type="text" id="email" name="email" class="form-control @error('email') is-invalid @enderror"  value="{{old('email')}}">
                        @error('email')
                        <div class="invalid-feedback alert alert-danger text-danger p-1">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>

    <footer class="text-white text-center w-100 p-2 mt-3 fixed-bottom">Copyright &copy; Brainster 2021</footer>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
  </body>
</html>