<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ url('css/style.css') }} ">
    <title>Home</title>
  </head>
  <body class="m-0 p-0">
    <p class="display-2 text-center text-white my-5">BUSINESS CASUAL</p>
    <nav class="text-center text-white m-0 p-0">
        <ul class="d-flex flex-row justify-content-center align-items-center list-unstyled w-100">
            <a href="{{route('home')}}" class="btn btn-link text-decoration-none text-warning"><li class="mx-4 fs-4">HOME</li></a>
            @if(!$username)
            <a href="{{route('login')}}" class="btn btn-link text-decoration-none text-white"><li class="mx-4 fs-4">LOG IN</li></a>
            @endif
            @if($username)
                <a href="{{route('logout')}}" class="btn btn-link text-decoration-none text-white"><li class="mx-4 fs-4">LOG OUT</li></a>
            @endif
        </ul>
    </nav>
    <div id="main">
        <div class="row m-0 p-0 imgMidle">
            <div id="coffee-shop-text-box-outer" class="text-center">
                <div id="coffee-shop-text-box-inner" class="bg-light p-3">
                    <p>Lorem Ipsum</p>
                    <h5>LOREM IPSUM</h5>
                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Iste velit eveniet, soluta autem iusto alias, dolorum reiciendis facere vero.</p>
                </div>
                <button id="coffee-shop-text-box-btn" class="btn btn-warning">Visit us today</button>
            </div>
          
           <div  id="coffee-shop" >
               
           </div>
           
        </div>
       
    </div>
    <div id="promise-div" class="bg-warning text-center w-100 my-5 p-5">
            <div id="promise-div-inner" class="w-75 bg-light p-2 mx-auto">
                <p class="fs-4">OUR PROMISE</p>
                
                <p class="display-4">
                    @if($username)
                    {{$username}}
                    @else
                    TO YOU
                    @endif
                </p>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi deserunt dolorum quis soluta eos doloremque cupiditate distinctio, recusandae obcaecati illum voluptatum officiis, sint earum, accusantium exercitationem beatae delectus dolore. Eligendi? Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quidem totam molestiae numquam mollitia, at quaerat unde, dicta doloremque nulla consequuntur animi perspiciatis non doloribus molestias facilis exercitationem placeat! Obcaecati, earum!</p>
            </div>
        </div>
    <footer class="text-white text-center w-100 p-2 mt-3">Copyright &copy; Brainster 2021</footer>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
  </body>
</html>