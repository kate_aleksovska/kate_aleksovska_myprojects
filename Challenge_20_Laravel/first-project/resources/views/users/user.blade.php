<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ url('css/style.css') }} ">
    <title>User</title>
  </head>
  <body class="m-0 p-0">
    <p class="display-2 text-center text-white my-5">BUSINESS CASUAL</p>
    <nav class="text-center text-white m-0 p-0">
        <ul class="d-flex flex-row justify-content-center align-items-center list-unstyled w-100">
            <a href="{{route('home')}}" class="btn btn-link text-decoration-none text-white"><li class="mx-4 fs-4">HOME</li></a>
            @if(!$username)
            <a href="{{route('login')}}" class="btn btn-link text-decoration-none text-white"><li class="mx-4 fs-4">LOG IN</li></a>
            @endif
            @if($username)
                <a href="{{route('logout')}}" class="btn btn-link text-decoration-none text-white"><li class="mx-4 fs-4">LOG OUT</li></a>
            @endif
        </ul>
    </nav>
        <div class="row d-flex justify-content-center align-items-center m-0 mt-5 p-0">
            <div class="col-2 text-white">
                <p class="fs-2">Your name is: </p>
                <p class="fs-2">Your surname is: </p>
                @if($user['email'])
                    <p class="fs-2">Your email is: </p>
                @endif
            </div>
            <div class="col-2 text-white">
                <p class="fs-2 fw-bold">{{$user['firstname']}}</p>
                <p class="fs-2 fw-bold">{{$user['lastname']}}</p>
                @if($user['email'])
                    <p class="fs-2 fw-bold">{{$user['email']}}</p>
                @endif
            </div>
        </div>

    <footer class="text-white text-center w-100 p-2 mt-3 fixed-bottom">Copyright &copy; Brainster 2021</footer>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
  </body>
</html>