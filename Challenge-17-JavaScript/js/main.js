let counterTrue = 0;
 let counterPage = 0;
 let questions = []
function getQuestion(counterPage) {
    let answer = [];
    let divQuestion = document.getElementById("question");
    let cardObj = questions[counterPage];
    for (let i = 0; i < cardObj.incorrect_answers.length; i++) {
      answer.push(cardObj.incorrect_answers[i]);
    }
    answer.push(cardObj.correct_answer);
    console.log(answer);
        let card = `<ul class="list-group mb-3 text-center" id="${counterPage}" style="display: block">
      <li class="list-group-item">${cardObj.question}</li><li class="list-group-item">`;
    for (let j = 0; j < answer.length; j++) {
      card += `<button class="btn btn-outline-secondary m-1" id="btnClick-${j}">${answer[j]}</button>`;
    }
    card += `</li><li class="list-group-item"><strong></strong>${cardObj.category}</li></ul>`;
    divQuestion.innerHTML = card;
    for (let j = 0; j < answer.length; j++) {
    document.getElementById("btnClick-"+j).addEventListener('click', function(e) {
        e.preventDefault()
     let correctAnswer = questions[counterPage].correct_answer
     let answer = document.getElementById("btnClick-"+j).innerText
     console.log(answer)
     counterPage++
     if(correctAnswer === answer) {
        counterTrue++
     }
   if(counterPage==questions.length) {
    document.getElementById("secondPage").style.display = "none";
    document.getElementById("thirdPage").style.display = "block";
    document.getElementById("correct").innerText = `Total correct answers: ${counterTrue}/20`
   }
      

     localStorage.setItem("counterTrue", counterTrue)
     localStorage.setItem("counterPage", counterPage)
     location.hash = `#question-${counterPage}`;
     document.getElementById("completed").innerText =`Completed: ${counterPage}/20`
     getQuestion(counterPage) 

    })
    localStorage.setItem("counterTrue", counterTrue)
    localStorage.setItem("counterPage", counterPage)
}
}   

window.addEventListener("load", function (e) {
  let hash = location.hash;
  if (hash == "" || hash == "#") {
    e.preventDefault();
    setTimeout(() => {
      document.getElementById("frontPage").style.opacity = "0.6";
    }, 0);
    setTimeout(() => {
      document.getElementById("frontPage").style.display = "none";
      document.getElementById("loadingQuiz").style.display = "block";
      document.getElementById("loadingQuiz").style.opacity = "0.5";
    }, 200);
    setTimeout(() => {
      document.getElementById("loadingQuiz").style.opacity = "1";
    }, 500);
    setTimeout(() => {
      document.getElementById("frontPage").style.display = "block";
      document.getElementById("loadingQuiz").style.display = "none";
      document.getElementById("frontPage").style.opacity = "0.6";
    }, 2900);
    setTimeout(() => {
      document.getElementById("frontPage").style.opacity = "1";
    }, 3000);
  } else {
    questions = JSON.parse(localStorage.getItem("questions"))
    counterTrue = localStorage.getItem("counterTrue")
    counterPage = localStorage.getItem("counterPage")
    document.getElementById("firstPage").style.display = "none";
    document.getElementById("secondPage").style.display = "block";
    document.getElementById("loadingQuiz").style.display = "none";
    getQuestion(counterPage)
    document.getElementById("completed").innerText =`Completed: ${counterPage}/20`
  }
});
let startQuiz = document.getElementById("startQuiz");
startQuiz.addEventListener("click", async function (e) {
  e.preventDefault();
  document.getElementById("firstPage").style.display = "none";
  document.getElementById("secondPage").style.display = "block";
  
  
  location.hash = `#question-${counterPage}`;
  let res = await fetch("https://opentdb.com/api.php?amount=20");
  let data = await res.json();
  questions = data.results;
  console.log(questions);
  localStorage.setItem("questions", JSON.stringify(questions))
  localStorage.setItem("counterTrue", 0)
  localStorage.setItem("counterPage", 0)

  getQuestion(counterPage) 
  document.getElementById("startFromBegining").addEventListener('click', function(e) {
      e.preventDefault()
      window.location.href=window.location.protocol + "//" + window.location.host + "/" + window.location.pathname.split('#')[0]
  })
  document.getElementById("startFromBegining1").addEventListener('click', function(e) {
    e.preventDefault()
    window.location.href=window.location.protocol + "//" + window.location.host + "/" + window.location.pathname.split('#')[0]
})
   
});
