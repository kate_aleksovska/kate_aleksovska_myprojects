<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" 
    integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <style>
        .bg-image {
background-image: linear-gradient(rgba(0, 0, 0, 0.8), rgba(0, 0, 0, 0.1)), url("bg-image.jpg");
background-repeat: no-repeat;
background-size: cover;
background-attachment: fixed;
opacity: 0.8;
        }
        a {
            font-size: 20px!important;
            font-weight: 700!important;
        }
    </style>
</head>
<body>
    <div class="container-fluid bg-image vh-100">
        <div class="row">
            <div class="col d-flex justify-content-center align-items-center vh-100"><a href="signup.php" class="btn btn-success px-3 border border-danger progress-bar-striped progress-bar-animated p-3 text-danger">Sign up</a></div>
            <div class="col d-flex justify-content-center align-items-center vh-100"><a href="login.php" class="btn btn-success px-3 border border-danger progress-bar-striped progress-bar-animated p-3 text-danger">Login</a></div>
        </div>
    </div>
</body>
</html>