<?php 
function checkRequestSignUp() {
    if($_SERVER['REQUEST_METHOD'] != "POST") {
        header('Location: signup.php');
    } 
}
function checkRequestLogIn() {
    if($_SERVER['REQUEST_METHOD'] != "POST") {
        header('Location: login.php');
    } 
}
function checkIssetInput($username, $password) {
    if(empty($username) || empty($password)) {
        header('Location: signup.php?error=allFieldsInserted');
        die();
    }
}
function checkIssetInputLogin($username, $password) {
    if(empty($username) || empty($password)) {
        header('Location: login.php?error=Allfieldsarerequired');
        die();
    }
}
function checkUsernameUnique($username) {
        $data = file_get_contents('users.txt');
    $users = explode("\n", trim($data));
    foreach($users as $user) {
        if($user != ""){
            $userData = explode("=", $user);
           if(strtolower($userData[0]) == strtolower($username)) {
            header('Location: signup.php?error=usernametaken');
            die();
           }
        }
    }
}
?>