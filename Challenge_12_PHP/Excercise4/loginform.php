<?php
   
if($_SERVER['REQUEST_METHOD'] == 'POST') {
    $username=$_POST['username'];
    $password=$_POST['password'];
    $email=$_POST['email'];
    $arr=array($email, $username);
    $checkUsernameEmail=implode(",",$arr);
    $data=explode("\n", trim(file_get_contents('users.txt')));
    $flag=false;
    foreach($data as $user) {
        $user=explode("=",$user);
        if($checkUsernameEmail == $user[0] && password_verify($password, $user[1]) && $username != '') {   
            $flag=true;
            break;
        }
    }
if($flag) {
    session_start();
    $_SESSION['username']=$username;
    $_SESSION['login']=true;
    header('Location: signupLoginPage.php');
    die();
} else {
        header('Location: login.php?error=wrongusernamepass');
        die();
        }
} else {
    header('Location: login.php');
    die();
}
?>