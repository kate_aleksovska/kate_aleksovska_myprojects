<?php 
include 'functionschallenge12.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" 
    integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <style>
        form {
            width: 50%;
            border: 1px dashed black;
        }
    </style>
</head>
<body>
<div class="container-fluid">
<a href="index.php" class="btn btn-primary m-3">Go back</a>
<?php 
checkAndPrintErrorMessage();
?>
    <div class="row">

        <div class="col d-flex justify-content-center align-items-center">
        <?php 
     echo '<form class="p-3" method="POST" action="signupform.php" novalidate>
     <h1 class="text-center">Sign up form</h1>
     <div class="form-group">
       <label for="email1">Enter email</label>
       <input type="email" class="form-control" id="email1" name="email">
     </div>
     <div class="form-group">
     
       <label for="username1">Enter username</label>
       <input type="text" class="form-control" id="username1" name="username">
     </div>
     <div class="form-group">
       <label for="password1">Password</label>
       <input type="password" class="form-control" id="password1" name="password">
     </div>
    <div class="text-center">
     <button type="submit" class="btn btn-primary text-center" name="submit">Sign up</button>
     </div>
   </form>'
    ?> 
        </div>
    </div>
</div>
   
</body>
</html>