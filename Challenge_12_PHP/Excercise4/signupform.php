<?php
include 'functionschallenge12.php';
include 'constants.php';

checkRequestSignUp();
$email=$_POST['email'];
$username=$_POST['username'];
$password=$_POST['password'];
$msg='';
checkIssetInput($email, $username, $password);
if(checkEmail($email)) {
    $msg .= "Email must contain at least 5 characters before @.";
}
if(checkUsernameEmailUnique($username, $email)) {
    $msg .= "A user with this email and username already exists.";
} else {
    if(checkEmailUnique($email)) {
        $msg .= "A user with this email already exists.";
    }
    
  if(checkUsernameUnique($username)) {
    $msg .= "A user with this username already exists.";
   }
}
if(checkUsername($username)) {
    $msg .= "Username must not contain empty spaces or special characters.";
}
if(checkPassword($password)) {
    $msg .= "Password must contain at least 1 uppercase, number and special character.";
}
if($msg !='') {
    header("Location: signup.php?error=$msg");
    die();
}
$password=password_hash($_POST['password'], PASSWORD_BCRYPT);
if(file_put_contents('users.txt', "$email,$username=$password\n", FILE_APPEND)) {
          session_start();
          $_SESSION['username'] = $username;
          header('Location: signupLoginPage.php');
          die();
      } 
header('Location: signup.php');
die();
?>