<?php 
function checkRequestSignUp() {
    if($_SERVER['REQUEST_METHOD'] != "POST") {
        redirect(SIGNUP_PAGE);
        die();
    } 
}
function checkRequestLogIn() {
    if($_SERVER['REQUEST_METHOD'] != "POST") {
        redirect(LOGIN_PAGE);
        die();
    } 
}
function checkIssetInput($email, $username, $password) {
    if(empty($email) || empty($username) || empty($password)) {
        header('Location: signup.php?error=All fields are required');
        die();
    }
}
function checkUsername($username) {
    if(strpbrk($username, ' ') !== false || preg_match('/[!@#\$%\^&\*]+/', $username)) {
        return true;
    }
    return false;
}
function checkEmail($email) {
    $emailArr=explode("@", $email);
    $emailCharCheck=strlen($emailArr[0]);
  if(!filter_var($email, FILTER_VALIDATE_EMAIL) || $emailCharCheck < 5) {
      return true;
}
return false;
}
function checkPassword($password) {
    if(!preg_match('/[A-Z]+/', $password) || !preg_match('/[0-9]+/', $password) || !preg_match('/[!@#\$%\^&\*]+/', $password)) { 
        return true;
    }
    return false;
}
function checkUsernameUnique($username) {
    $data = file_get_contents('users.txt');
    $users = explode("\n", trim($data));
    foreach($users as $user) {
        if($user != ""){
            $userData = explode("=", $user);
            $emailUsernames=explode(",",$userData[0]);
            $usernames=array($emailUsernames[1]);
            foreach($usernames as $key=>$usernamecheck) {
                if(strtolower($usernamecheck) == strtolower($username)) {
                    return true;
            }
        }
     }
    }
    return false;
}
function checkEmailUnique($email) {
    $data = file_get_contents('users.txt');
    $users = explode("\n", trim($data));
    foreach($users as $user) {
        if($user != ""){
            $userData = explode("=", $user);
            $emailUsernames=explode(",",$userData[0]);
            $emails=array($emailUsernames[0]);
            foreach($emails as $key=>$emailcheck) {
                    if($emailcheck == $email) {
                        return true;
                        
                    }
            }
        }
    }
    return false;
}
function checkUsernameEmailUnique($username, $email) {
    $arr=array($email, $username);
    $checkUsernameEmail=implode(",",$arr);
    $data = file_get_contents('users.txt');
    $users = explode("\n", trim($data));
    foreach($users as $user) {
        if($user != ""){
            $userData = explode("=", $user);
            if(strtolower($userData[0]) == strtolower($checkUsernameEmail)) {
                return true;
            }
            
        }
    }
    return false;
}

function checkAndPrintErrorMessage() {
    if(isset($_GET['error'])) {
        $arr=explode(".", trim($_GET['error']));
        $msg='<p class="alert alert-danger text-center text-danger pb=0">';
        foreach($arr as $error) {
        $msg .= $error .'.</br>';
        
    }
    echo $msg .'</p>';
    }
} 
function redirect($url, $queryString = '') {
    if($queryString != '') {
        $url .= "?$queryString";
    }

    header("Location:". $url);
    die();
}
?>