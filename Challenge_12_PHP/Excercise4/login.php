<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" 
    integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <style>
        form {
            width: 50%;
            border: 1px dashed black;
        }
    </style>
</head>
<body>
<div class="container-fluid">
<a href="index.php" class="btn btn-primary m-3">Go back</a>
<?php if(isset($_GET['error'])) {
    if($_GET['error'] == 'wrongusernamepass') {
     echo '<div class="alert alert-danger text-center" role="alert">
    Wrong username/password combination
</div>';
    }
}
?>
    <div class="row">
        <div class="col d-flex justify-content-center align-items-center">
        <?php 
     echo '<form class="p-3" method="POST" action="loginform.php">
     <h1 class="text-center">Login form</h1>
     <div class="form-group">
       <label for="email2">Enter email</label>
       <input type="email" class="form-control" id="email2" name="email">
     </div>
     <div class="form-group">
       <label for="username2">Enter username</label>
       <input type="text" class="form-control" id="username2" name="username">
     </div>
     <div class="form-group">
       <label for="password2">Password</label>
       <input type="password" class="form-control" id="password2" name="password">
     </div>
    <div class="text-center">
     <button type="submit" class="btn btn-primary text-center">Login</button>
     </div>
   </form>'
    ?> 
        </div>
    </div>
</div>
   
</body>
</html>