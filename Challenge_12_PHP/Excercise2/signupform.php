<?php
include 'functionschallenge12.php';

checkRequestSignUp();
$email=$_POST['email'];
$username=$_POST['username'];
$password=$_POST['password'];

checkIssetInput($email, $username, $password);
checkUsernameEmailUnique($username, $email);
checkUsernameUnique($username);
checkEmailUnique($email);
$password=password_hash($_POST['password'], PASSWORD_BCRYPT);
if(file_put_contents('users.txt', "$email,$username=$password\n", FILE_APPEND)) {
          session_start();
          $_SESSION['username'] = $username;
          header('Location: signupLoginPage.php');
          die();
      } 
header('Location: signup.php');
die();
?>