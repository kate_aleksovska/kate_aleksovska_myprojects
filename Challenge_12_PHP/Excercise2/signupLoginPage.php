<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" 
    integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <style>
        p {
            font-size: 30px!important;
            color: green!important;
            font-weight: 600!important;

        }
    </style>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col d-flex justify-content-center flex-column align-items-center vh-100">
        <?php 
     session_start();
        if(isset($_SESSION['username']) || $_SESSION['login'] == true) {
        echo "<p>Welcome {$_SESSION['username']}</p></br></br>";
        echo '<a href="logout.php" class="btn btn-warning d-inline-block w-25">Log out</a>';
    } else {
    header('Location: index.php');
    die();
    }
?>
        </div>
    </div>
</div>
</body>
</html>