<?php 

function checkRequestSignUp() {
    if($_SERVER['REQUEST_METHOD'] != "POST") {
        header('Location: signup.php');
    } 
}
function checkRequestLogIn() {
    if($_SERVER['REQUEST_METHOD'] != "POST") {
        header('Location: login.php');
    } 
}
function checkIssetInput($email, $username, $password) {
    if(empty($email) || empty($username) || empty($password)) {
        header('Location: signup.php?error=Allfieldsarerequired');
        die();
    }
}
function checkIssetInputLogin($email, $username, $password) {
    if(empty($email) || empty($username) || empty($password)) {
        header('Location: login.php?error=Allfieldsarerequired');
        die();
    }
}
function checkUsernameUnique($username) {
    $data = file_get_contents('users.txt');
    $users = explode("\n", trim($data));
    foreach($users as $user) {
        if($user != ""){
            $userData = explode("=", $user);
            $emailUsernames=explode(",",$userData[0]);
            $usernames=array($emailUsernames[1]);
            foreach($usernames as $key=>$usernamecheck) {
                if(strtolower($usernamecheck) == strtolower($username)) {
                        header('Location: signup.php?error=usernametaken');
                        die();
            }
        }
        }
    }
}
function checkEmailUnique($email) {
    $data = file_get_contents('users.txt');
    $users = explode("\n", trim($data));
    foreach($users as $user) {
        if($user != ""){
            $userData = explode("=", $user);
            $emailUsernames=explode(",",$userData[0]);
            $emails=array($emailUsernames[0]);
            foreach($emails as $key=>$emailcheck) {
                    if($emailcheck == $email) {
                            header('Location: signup.php?error=emailtaken');
                            die();
                    }
            }
        }
    }
}
function checkUsernameEmailUnique($username, $email) {
    $arr=array($email, $username);
    $checkUsernameEmail=implode(",",$arr);
    $data = file_get_contents('users.txt');
    $users = explode("\n", trim($data));
    foreach($users as $user) {
        if($user != ""){
            $userData = explode("=", $user);
            if(strtolower($userData[0]) == strtolower($checkUsernameEmail)) {
                header('Location: signup.php?error=emailusertaken');
                            die();
            }
            
        }
    }
}
?>