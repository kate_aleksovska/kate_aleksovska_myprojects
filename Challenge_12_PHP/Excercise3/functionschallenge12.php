<?php 
function checkRequestSignUp() {
    if($_SERVER['REQUEST_METHOD'] != "POST") {
        redirect(SIGNUP_PAGE);
        die();
    } 
}
function checkRequestLogIn() {
    if($_SERVER['REQUEST_METHOD'] != "POST") {
        redirect(LOGIN_PAGE);
        die();
    } 
}
function checkIssetInput($email, $username, $password) {
    if(empty($email) || empty($username) || empty($password)) {
        redirect(SIGNUP_PAGE, "error=allFieldsInserted");
        die();
    }
}
function checkIssetInputLogin($email, $username, $password) {
    if(empty($email) || empty($username) || empty($password)) {
        header('Location: login.php?error=Allfieldsarerequired');
        die();
    }
}
function checkUsername($username) {
    if(strpbrk($username, ' ') !== false || preg_match('/[!@#\$%\^&\*]+/', $username)) {
        redirect(SIGNUP_PAGE, "error=usernameInvalid");
        die();
    }
}
function checkEmail($email) {
    $emailArr=explode("@", $email);
    $emailCharCheck=strlen($emailArr[0]);
  if(!filter_var($email, FILTER_VALIDATE_EMAIL) || $emailCharCheck < 5) {
    redirect(SIGNUP_PAGE, "error=emailInvalid");
    die();
}
}
function checkPassword($password) {
    if(!preg_match('/[A-Z]+/', $password) || !preg_match('/[0-9]+/', $password) || !preg_match('/[!@#\$%\^&\*]+/', $password)) { 
        redirect(SIGNUP_PAGE, "error=passInvalid");
        die();
    }
}
function checkUsernameUnique($username) {
    $data = file_get_contents('users.txt');
    $users = explode("\n", trim($data));
    foreach($users as $user) {
        if($user != ""){
            $userData = explode("=", $user);
            $emailUsernames=explode(",",$userData[0]);
            $usernames=array($emailUsernames[1]);
            foreach($usernames as $key=>$usernamecheck) {
                if(strtolower($usernamecheck) == strtolower($username)) {
                    redirect(SIGNUP_PAGE, "error=usernametaken");
                    die();
            }
        }
        }
    }
}
function checkEmailUnique($email) {
    $data = file_get_contents('users.txt');
    $users = explode("\n", trim($data));
    foreach($users as $user) {
        if($user != ""){
            $userData = explode("=", $user);
            $emailUsernames=explode(",",$userData[0]);
            $emails=array($emailUsernames[0]);
            foreach($emails as $key=>$emailcheck) {
                    if($emailcheck == $email) {
                        redirect(SIGNUP_PAGE, "error=emailtaken");
                        die();
                        
                    }
            }
        }
    }
}
function checkUsernameEmailUnique($username, $email) {
    $arr=array($email, $username);
    $checkUsernameEmail=implode(",",$arr);
    $data = file_get_contents('users.txt');
    $users = explode("\n", trim($data));
    foreach($users as $user) {
        if($user != ""){
            $userData = explode("=", $user);
            if(strtolower($userData[0]) == strtolower($checkUsernameEmail)) {
                redirect(SIGNUP_PAGE, "error=emailusertaken");
                            die();
            }
            
        }
    }
}
function checkAndPrintErrorMessage() {
    $errorMsg = [
        'usernametaken' => 'Username is already taken',
        'emailtaken' => 'A user with this email already exists.',
        'emailusertaken' => 'A user with this email and username already exists.',
        'passInvalid' => 'Password must contain at least 1 uppercase, number and special character.',
        'emailInvalid' => 'Email must contain at least 5 characters before @.',
        'usernameInvalid' => 'Username must not contain empty spaces or special characters.',
        'allFieldsInserted' => 'All fields are required.',
    ];

    if(isset($_GET['error'])) {
        echo '<p class="alert alert-danger text-center text-danger">'. $errorMsg[$_GET['error']]  .'</p>';
    }
}
function redirect($url, $queryString = '') {
    if($queryString != '') {
        $url .= "?$queryString";
    }

    header("Location:". $url);
    die();
}
?>