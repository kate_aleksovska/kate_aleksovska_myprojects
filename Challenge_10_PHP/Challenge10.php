<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<!-- exercise 01 -->
    <?php
    $name = "Kathrin";

    if($name == "Kathrin") {
        echo "<div>Hello {$name}</div>";
    }
    else {
        echo "<div>Nice name</div>";
    }

echo "<hr>";

$rating = 11;
if($rating >=1 && $rating <=10) {
    echo "<div>Thank you for rating</div>";
} else 
echo "<div>Invalid rating, only numbers between 1 and 10.</div>";

// exercise 02

echo "<hr>";
$hour = date('H');
if ($name === "Kathrin") {
if($hour <= date('12')) {
    echo "<div>Good morning {$name}</div>";
} elseif($hour <= date('19')) {
    echo "<div>Good afternoon {$name}</div>";
}
else 
echo "<div>Good evening {$name}</div>";
} 

echo "<hr>";
$rating = 11;
$rated = false;
var_dump($rating);
if($rating >=1 && $rating <=10) {
    if($rated == true) {
    echo "<div>Thank you for voting</div>";
} else 
echo "<div>You already voted.</div>";
} else
echo "<div>Invalid rating, only numbers between 1 and 10.</div>";

// exercise 03
echo "<hr>";
echo "<br>"; 
$voters = [
    ["name" => "Maria", "rated" => "false", "rating" => 5],
    ["name" => "Nikola", "rated" => "true", "rating" => 8],
    ["name" => "Maria", "rated" => "false", "rating" => 90],
    ["name" => "Ivana", "rated" => "false", "rating" => 4],
    ["name" => "Suze", "rated" => "true", "rating" => 9],
    ["name" => "Blagica", "rated" => "false", "rating" => 3],
    ["name" => "Vladimir", "rated" => "true", "rating" => 50],
    ["name" => "Kate", "rated" => "true", "rating" => 6],
    ["name" => "Stefanija", "rated" => "false", "rating" => 40],
    ["name" => "Darko", "rated" => "false", "rating" => 1]
];
echo "<hr>";
echo "<br>"; 
 foreach($voters as $voter) {
    echo "{$voter["name"]} => {$voter["rated"]}". ",". "{$voter["rating"]}.<br>";
     if(($voter["rating"] >= 1) && ($voter["rating"] <=10)) {
       
        if($hour <= date('12')) {
           
            echo "<div>Good morning {$voter["name"]},<br> Thanks for voting with {$voter["rating"]}.</div>";
        } elseif($hour <= date('19')) {
            echo "<div>Good afternoon {$voter["name"]},<br> Thanks for voting with {$voter["rating"]}.</div></div>";
        }
        else 
        echo "<div>Good evening {$voter["name"]},<br> Thanks for voting with {$voter["rating"]}</div>.</div>";
         
    } elseif($voter["rating"] > 10) {
        if($hour <= date('12')) {
           
            echo "<div>Good morning {$voter["name"]},<br> {$voter["rating"]} is Invalid rating, only numbers between 1 and 10.</div>";
        } elseif($hour <= date('19')) {
            echo "<div>Good afternoon {$voter["name"]},<br> {$voter["rating"]} is Invalid rating, only numbers between 1 and 10.</div>";
        }
        else 
        echo "<div>Good evening {$voter["name"]},<br> {$voter["rating"]} is Invalid rating, only numbers between 1 and 10.</div>";
 }
}
    ?>

</body>
</html>