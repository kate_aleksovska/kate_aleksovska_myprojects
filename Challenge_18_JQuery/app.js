$(function(){
console.log('tuka')
let car1Result = $('#car1Results')
let car2Result = $('#car2Results')
let startOver = $('#startOver')
let startRace = $('#startRace')
let car1LocarStorage = localStorage.getItem('car1')
let car2LocarStorage = localStorage.getItem('car2')
if(car1LocarStorage && car2LocarStorage) {
	$('#results').css({display: 'block'})
	$('#results').html(`<h3 class="mb-5">Results from the previous time you played this game</h3>`+car1LocarStorage+car2LocarStorage)
}
startRace.on('click', function(e){


	startRace.attr('disabled', true);
	startOver.attr('disabled', true)
	$('#race').css({
		opacity: 0.5,
		backgroundColor: 'black'
	})
	let car1 = $('#car1')
	let car2 = $('#car2')
	let carWidth1 = $('#car1').width()
	let carWidth2 = $('#car2').width()
	let windowWidth = $(document).width()	
    let timer = 4
	let id = setInterval(function(){
		timer--
		$('#countDownFlag').css({
			opacity: 1,
			display: "block",
		})
		$('#countDownFlag').html(`<div class="timer">${timer}</div>`)
		if(timer == 0){
			clearInterval(id)
			$('#countDownFlag').css({
				display: "none",
			})
			$('#race').css({
				opacity: 1,
				backgroundColor: '#242021'
			})
			$('#timer').text(``)
			let timerCar1 = Math.floor((Math.random() * 3000)+10);
			let timerCar2 =  Math.floor((Math.random() * 3000)+10);	
			car1.animate({
				'margin-left': windowWidth - carWidth1
			}, timerCar1, function(e) {
				$('#countDownFlag').css({
					opacity: 1,
					display: "block",
				})
				$('#countDownFlag').html(`<img src="./img/finish.gif" alt="">`)
	            startOver.attr('disabled', false);
				timerCar1 > timerCar2 ? place = 'first' : place = 'second' 
				carFinalresult= `<p class="border border-light py-0 my-0">Finished in <b>${place}</b><br/>With a time of <b>${timerCar1}</b>ms</p>`

				car1Result.html(car1Result.html()+carFinalresult)
				localStorage.setItem("car1", `<p class="border border-light pb-0 mb-0"><b>Car1</b> Finished in <b>${place}</b> With a time of <b>${timerCar1}</b>ms</p>`)
				
			
			})
			car2.animate({
				'margin-left': windowWidth - carWidth2
		}, timerCar2, function(e) {
			$('#countDownFlag').css({
				opacity: 1,
				display: "block",
			})
			$('#countDownFlag').html(`<img src="./img/finish.gif" alt="">`)
			startOver.attr('disabled', false)
			timerCar1 < timerCar2 ? place = 'first' : place = 'second' 
			carFinalresult = `
			<p class="border border-light py-0 my-0">Finished in <b class="text-danger">${place}</b><br/>With a time of <b class="text-danger">${timerCar2}</b> ms</p></div>`
			car2Result.html(car2Result.html()+carFinalresult)
			localStorage.setItem("car2", `<p class="border border-light pt-0 mt-0"><b class="text-danger">Car2</b> Finished in <b>${place}</b> With a time of <b>${timerCar2}</b>ms</p>`)
		})
		startOver.on('click', function(){
			$('#countDownFlag').css({
				display: "none",
			})
			car1.css('margin-left', '0')
			car2.css('margin-left', '0')
			startRace.attr('disabled', false);
		})
		}
	}, 1000)
	

})


});