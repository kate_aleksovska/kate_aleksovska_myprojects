
$(function () {
  getAllBikesFromApi();
  getCounterForGender();
  let showAll = $("#showAll");
  showAll.on("click", function (e) {
    window.location.reload(false)
    if ($(this).hasClass("clickedFilter") && ($(this).children()).hasClass('badgeClass')) {
      $(".badge-pill").removeClass("badgeClass");
      $(".text-capitalize").removeClass("clickedFilter");
      $(".badge-pill").removeClass("badgeClass");
    }
    // Else, the element doesn't have the active class, so we remove it from every element before applying it to the element that was clicked
    else {
      $(".text-capitalize").removeClass("clickedFilter");
      $(this).addClass("clickedFilter");
      $(".badge-pill").removeClass("badgeClass");
      ($(this).children()).addClass("badgeClass")
    }
  });
  $(document).on("click", "#genderHTML p", function (e) {
    // $(this).removeClass('clickedFilter')
    if ($(this).hasClass("clickedFilter") && ($(this).children()).hasClass('badgeClass')) {
      $(".text-capitalize").removeClass("clickedFilter");
      $(".badge-pill").removeClass("badgeClass");
    }
    // Else, the element doesn't have the active class, so we remove it from every element before applying it to the element that was clicked
    else {
      $(".text-capitalize").removeClass("clickedFilter");
      $(".badge-pill").removeClass("badgeClass");
      $(this).addClass("clickedFilter");
      ($(this).children()).addClass("badgeClass")
    }
    e.preventDefault();
    $("#bikesCard").empty();
    const gender = $(this).data("gender").toUpperCase();
    console.log(gender);
    getBikesByGender(gender);
  });
  $(document).on("click", "#typeBikeHTML p", function (e) {
    e.preventDefault();
    
    $("#bikesCard").empty();
    const type = $(this).data("type");
    console.log(type);
    // $(this).addClass('clickedFilter')
    getBikesByType(type);
    if ($(this).hasClass("clickedFilter") && ($(this).children()).hasClass('badgeClass')) {
      $(".badge-pill").removeClass("badgeClass");
      $(".text-capitalize").removeClass("clickedFilter");
      $(".badge-pill").removeClass("badgeClass");
    }
    // Else, the element doesn't have the active class, so we remove it from every element before applying it to the element that was clicked
    else {
      $(".text-capitalize").removeClass("clickedFilter");
      $(this).addClass("clickedFilter");
      $(".badge-pill").removeClass("badgeClass");
      ($(this).children()).addClass("badgeClass")
    }
  

    // $(`[data-gender='male']`).addClass('clickedFilter');
    // $(`[data-gender='female']`).empty()
  });

  function getAllBikesFromApi() {
    $.ajax({
      method: "GET",
      url: `https://json-project3.herokuapp.com/products`,
      type: "json",
    })
      .then((Allbikes) => {
        let allGenderBikes = [];
        let allTypeOfBikes = [];
        let counter = 0;
        genderBike = [];
        $("#genderHTML").empty();
        $("typeBikeHTML").empty();
       
        $("#bikesCard").empty();
        emptyAllCounters()
            console.log(Allbikes);
        for (let bike of Allbikes) {
          counter++;
          let imgBikes = `<div class="card m-2" style="width: 18rem;">
            <img src="./img/${parseInt(
              bike.image
            )}.png" class="card-img-top" style="height: 176.36px;">
            <div class="text-uppercase bg-warning px-3 py-1">
              <p class="card-text font-weight-bolder">${bike.name}</p>
              <p>${bike.price} $</p>
            </div>
          </div>`;

          allGenderBikes.push(bike.gender);
          allTypeOfBikes.push(bike.brand);
          console.log(allTypeOfBikes);
          $("#bikesCard").append(imgBikes);
        }
        $("#allBikesCounter").text(counter);
        getGender(allGenderBikes);
        getBikesByName(allTypeOfBikes);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  function array_unique(array) {
    var unique = [];
    for (var i = 0; i < array.length; ++i) {
      if (unique.indexOf(array[i]) == -1) unique.push(array[i]);
    }
    return unique;
  }
  function getGender(allGenderBikes) {
    let genders = array_unique(allGenderBikes);
    for (let gender of genders) {
      genderBike = `<p data-gender="${gender.toLowerCase()}" class="text-capitalize d-flex justify-content-between">${gender.toLowerCase()}</p>`;
      $("#genderHTML").append(genderBike);
    }
  }
  function getBikesByName(allTypeOfBikes) {
    let typeOfBikes = array_unique(allTypeOfBikes);
    console.log(typeOfBikes);
    for (let typeOfBike of typeOfBikes) {
      typeOfBikeHTML = `<p data-type="${typeOfBike}" class="text-capitalize d-flex justify-content-between">${typeOfBike}</p>`;
      $("#typeBikeHTML").append(typeOfBikeHTML);
    }
  }
  function getBikesByGender(gender) {
    $.ajax({
      method: "GET",
      url: `https://json-project3.herokuapp.com/products`,
      type: "json",
    })
      .then((Allbikes) => {
        let filterByGender = Allbikes.filter((obj) => {
          return obj.gender == `${gender}`;
        });
        for (let bikeByGender of filterByGender) {
          let genderBikesArr = `<div class="card m-2" style="width: 18rem;">
         <img src="./img/${parseInt(
           bikeByGender.image
         )}.png" class="card-img-top" style="height: 176.36px;">
         <div class="text-uppercase bg-warning px-3 py-1">
           <p class="card-text font-weight-bolder">${bikeByGender.name}</p>
           <p>${bikeByGender.price} $</p>
         </div>
       </div>`;
          $("#bikesCard").append(genderBikesArr);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }
  function getBikesByType(type) {
    $.ajax({
      method: "GET",
      url: `https://json-project3.herokuapp.com/products`,
      type: "json",
    })
      .then((Allbikes) => {
        let filterByType = Allbikes.filter((obj) => {
          return obj.brand == `${type}`;
        });
        for (let bikeByType of filterByType) {
          let typeBikesArr = `<div class="card m-2" style="width: 18rem;">
         <img src="./img/${parseInt(
           bikeByType.image
         )}.png" class="card-img-top" style="height: 176.36px;">
         <div class="text-uppercase bg-warning px-3 py-1">
           <p class="card-text font-weight-bolder">${bikeByType.name}</p>
           <p>${bikeByType.price} $</p>
         </div>
       </div>`;
          $("#bikesCard").append(typeBikesArr);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }
  function emptyAllCounters() {
 
    $(`[data-gender='male']`).empty();
    $(`[data-gender='female']`).empty();
    $(`[data-type='LE GRAND BIKES']`).empty();
    $(`[data-type='KROSS']`).empty();
    $(`[data-type='EXPLORER']`).empty();
    $(`[data-type='VISITOR']`).empty();
    $(`[data-type='PONY']`).empty();
    $(`[data-type='FORCE']`).empty();
    $(`[data-type='E-BIKES']`).empty();
    $(`[data-type='IDEAL`).empty();
    $("#bikesCard").empty();
  }
  function getCounterForGender() {
  
     
    let maleCounter = 0;
    let femaleCounter = 0;
    let leGrandBikesCounter = 0;
    let krossCounter = 0;
    let explorerCounter = 0;
    let visitorCounter = 0;
    let ponyCounter = 0;
    let forceCounter = 0;
    let ebikeCounter = 0;
    let idealCounter = 0;
    $.ajax({
      method: "GET",
      url: `https://json-project3.herokuapp.com/products`,
      type: "json",
    })
      .then((Allbikes) => {
        let filterByMale = Allbikes.filter((obj) => {
          return obj.gender == `MALE`;
        });
        let filterByFemale = Allbikes.filter((obj) => {
          return obj.gender == `FEMALE`;
        });
        let filterByTypeLeGrand = Allbikes.filter((obj) => {
          return obj.brand == `LE GRAND BIKES`;
        });
        let filterByTypeKross = Allbikes.filter((obj) => {
          return obj.brand == `KROSS`;
        });
        let filterByTypeExplorer = Allbikes.filter((obj) => {
          return obj.brand == `EXPLORER`;
        });
        let filterByTypeVisitor = Allbikes.filter((obj) => {
          return obj.brand == `VISITOR`;
        });
        let filterByTypePony = Allbikes.filter((obj) => {
          return obj.brand == `PONY`;
        });
        let filterByTypeForce = Allbikes.filter((obj) => {
          return obj.brand == `FORCE`;
        });
        let filterByEbikes = Allbikes.filter((obj) => {
          return obj.brand == `E-BIKES`;
        });
        let filterByTypeIdeal = Allbikes.filter((obj) => {
          return obj.brand == `IDEAL`;
        });
        for (let male of filterByMale) {
          maleCounter++;
        }
        for (let female of filterByFemale) {
          femaleCounter++;
        }
        for (let leGrand of filterByTypeLeGrand) {
          leGrandBikesCounter++;
        }
        for (let kross of filterByTypeKross) {
          krossCounter++;
        }
        for (let explorer of filterByTypeExplorer) {
          explorerCounter++;
        }
        for (let visitor of filterByTypeVisitor) {
          visitorCounter++;
        }
        for (let pony of filterByTypePony) {
          ponyCounter++;
        }
        for (let force of filterByTypeForce) {
          forceCounter++;
        }
        for (let ebike of filterByEbikes) {
          ebikeCounter++;
        }
        for (let ideal of filterByTypeIdeal) {
          idealCounter++;
        }
        // emptyAllCounters()
        let counterFemaleDiv = `<span class="badge badge-pill badge-secondary text-dark">${femaleCounter}</span>`;
        let counterMaleDiv = `<span class="badge badge-pill badge-secondary text-dark">${maleCounter}</span>`;
        let counterleGrandDiv = `<span class="badge badge-pill badge-secondary text-dark">${leGrandBikesCounter}</span>`;
        let counterkrossCountersDiv = `<span class="badge badge-pill badge-secondary text-dark">${krossCounter}</span>`;
        let counterexplorerCounterDiv = `<span class="badge badge-pill badge-secondary text-dark">${explorerCounter}</span>`;
        let countervisitorCounterDiv = `<span class="badge badge-pill badge-secondary text-dark">${visitorCounter}</span>`;
        let counterponyCounterDiv = `<span class="badge badge-pill badge-secondary text-dark">${ponyCounter}</span>`;
        let counterforceCounterDiv = `<span class="badge badge-pill badge-secondary text-dark">${forceCounter}</span>`;
        let counterebikeCounterDiv = `<span class="badge badge-pill badge-secondary text-dark">${ebikeCounter}</span>`;
        let counteridealCounterDiv = `<span class="badge badge-pill badge-secondary text-dark">${idealCounter}</span>`;
        $(`[data-gender='male']`).append(counterMaleDiv);
        $(`[data-gender='female']`).append(counterFemaleDiv);
        $(`[data-type='LE GRAND BIKES']`).append(counterleGrandDiv);
        $(`[data-type='KROSS']`).append(counterkrossCountersDiv);
        $(`[data-type='EXPLORER']`).append(counterexplorerCounterDiv);
        $(`[data-type='VISITOR']`).append(countervisitorCounterDiv);
        $(`[data-type='PONY']`).append(counterponyCounterDiv);
        $(`[data-type='FORCE']`).append(counterforceCounterDiv);
        $(`[data-type='E-BIKES']`).append(counterebikeCounterDiv);
        $(`[data-type='IDEAL`).append(counteridealCounterDiv);
      })
      .catch((error) => {
        console.log(error);
      });
  }
 
});
