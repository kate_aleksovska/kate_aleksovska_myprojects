-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 20, 2021 at 07:29 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `challenge_08`
--

-- --------------------------------------------------------

--
-- Table structure for table `actors`
--

CREATE TABLE `actors` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(64) DEFAULT NULL,
  `lastname` varchar(64) DEFAULT NULL,
  `nickname` varchar(32) DEFAULT NULL,
  `code_of_agent` varchar(32) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `actors`
--

INSERT INTO `actors` (`id`, `firstname`, `lastname`, `nickname`, `code_of_agent`, `date_of_birth`) VALUES
(1, 'Ashton', 'Kutcher', 'AshtonKutcher', 'NF2015', '1974-02-02'),
(2, 'Jake', 'Gyllenhaal', 'JakeGyllenhaal', 'NF1647', '1982-03-04'),
(3, 'Morgan', 'Freeman', 'MorganFreeman', 'NF6341', '1952-04-03'),
(4, 'Brad', 'Pitt', 'BradPitt', 'NF1984', '1973-06-08'),
(5, 'Tom', 'Cruise', 'TomCruise', 'NF1346', '1983-06-03'),
(6, 'Denzel', 'Washington', 'DenzelWashington', 'NF1487', '1954-07-19'),
(7, 'Ethan', 'Hawke', 'EthanHawke', 'NF1487', '1957-03-04'),
(8, 'Al', 'Pacino', 'AlPacino', 'NF4875', '1950-06-27'),
(9, 'Robert', 'De Niro', 'RobertDeNiro', 'NF3645', '1960-06-03'),
(10, 'Christopher', 'Walken', 'ChristopherWalken', 'NF6341', '1971-07-01'),
(11, 'Dakota', 'Fanning', 'DakotaFanning', 'NF4761', '1986-03-03'),
(12, 'Meryl', 'Meryl', 'MerylStreep', 'NF4673', '1776-03-03'),
(13, 'Sigourney', 'Weaver', 'SigourneyWeaver', 'NF6458', '1949-10-08');

-- --------------------------------------------------------

--
-- Table structure for table `actors_in_movie`
--

CREATE TABLE `actors_in_movie` (
  `id` int(10) UNSIGNED NOT NULL,
  `actor_paid_per_movie` mediumint(8) UNSIGNED DEFAULT NULL,
  `movie_id` int(10) UNSIGNED DEFAULT NULL,
  `actor_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `actors_in_movie`
--

INSERT INTO `actors_in_movie` (`id`, `actor_paid_per_movie`, `movie_id`, `actor_id`) VALUES
(1, 15306403, 1, 8),
(2, 13452674, 1, 6),
(3, 12435677, 1, 2),
(4, 12342165, 2, 2),
(5, 12435746, 3, 3),
(6, 12435856, 3, 2),
(7, 12344621, 4, 4),
(8, 12434614, 7, 4),
(9, 4572257, 10, 5),
(10, 12124586, 9, 6),
(11, 12487652, 8, 10),
(12, 7455623, 12, 13),
(13, 14526655, 13, 13),
(14, 12467842, 16, 13),
(15, 12478956, 17, 13),
(16, 4578961, 18, 7),
(17, 1425639, 19, 9),
(18, 4589321, 20, 11),
(19, 4578965, 21, 12),
(20, 7866345, 23, 9),
(21, 146789, 23, 1),
(22, 7853461, 11, 1),
(23, 5467823, 20, 9),
(24, 4578963, 23, 7),
(25, 4876569, 6, 3),
(26, 4512378, 15, 13),
(27, 4578965, 5, 11),
(28, 7894562, 24, 6),
(29, 455000, 14, 10),
(30, 12435677, 23, 4),
(32, 15306403, 21, 8),
(34, 13452674, 22, 2);

-- --------------------------------------------------------

--
-- Table structure for table `country_of_origin`
--

CREATE TABLE `country_of_origin` (
  `id` int(10) UNSIGNED NOT NULL,
  `country` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `country_of_origin`
--

INSERT INTO `country_of_origin` (`id`, `country`) VALUES
(1, 'Sweden'),
(2, 'Italy'),
(3, 'Australia'),
(4, 'England'),
(5, 'Ireland'),
(6, 'USA'),
(7, 'Norway'),
(8, 'Island'),
(9, 'France'),
(10, 'Spain'),
(11, 'Belgium'),
(12, 'China'),
(13, 'Japan'),
(14, 'Mexico'),
(15, 'Africa');

-- --------------------------------------------------------

--
-- Table structure for table `critics`
--

CREATE TABLE `critics` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(32) DEFAULT NULL,
  `lastname` varchar(64) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `critics`
--

INSERT INTO `critics` (`id`, `firstname`, `lastname`, `password`) VALUES
(1, 'Liam', 'Andersson', 'Forddriving456'),
(2, 'Noah', 'Johansson', 'chickensoup54'),
(3, 'Oliver', 'Karlsson', 'Olivetree12'),
(4, 'Elijah', 'Nilsson', 'Mynameisthis133'),
(5, 'William', 'Eriksson', 'Cookingpie1'),
(6, 'James', 'Larsson', 'Hereiam123'),
(7, 'Olivia', 'Olsson', 'Pencilcase5'),
(8, 'Benjamin', 'Persson', 'Avenue5'),
(9, 'Emma', 'Svensson', 'SkopjeHome456'),
(10, 'Ava', 'Gustafsson', 'FineStuff4'),
(11, 'Lucas', 'Pettersson', 'Historyclass25'),
(12, 'Sophia', 'Jonsson', 'Weddingnight45'),
(13, 'Charlotte', 'Jansson', 'Popularband12'),
(14, 'Henry', 'Hansson', '45KateAleksovska45'),
(15, 'Amelia', 'Bengtsson', 'Registar256');

-- --------------------------------------------------------

--
-- Table structure for table `critique_actor`
--

CREATE TABLE `critique_actor` (
  `id` int(10) UNSIGNED NOT NULL,
  `actor_id` int(10) UNSIGNED DEFAULT NULL,
  `critic_id` int(10) UNSIGNED DEFAULT NULL,
  `film_id` int(10) UNSIGNED DEFAULT NULL,
  `rate_actor_acting` double(2,1) UNSIGNED DEFAULT NULL,
  `rate_actor_expression` double(2,1) UNSIGNED DEFAULT NULL,
  `rate_actor_naturalness` double(2,1) UNSIGNED DEFAULT NULL,
  `rate_actor_devotion` double(2,1) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `critique_actor`
--

INSERT INTO `critique_actor` (`id`, `actor_id`, `critic_id`, `film_id`, `rate_actor_acting`, `rate_actor_expression`, `rate_actor_naturalness`, `rate_actor_devotion`) VALUES
(1, 8, 5, 1, 3.0, 5.1, 3.1, 2.0),
(2, 6, 6, 1, 0.5, 4.6, 8.7, 9.7),
(3, 2, 7, 1, 6.3, 4.1, 3.1, 9.4),
(4, 2, 3, 2, 6.4, 7.3, 1.2, 4.2),
(5, 3, 4, 3, 5.6, 9.9, 2.7, 2.6),
(6, 2, 9, 3, 8.0, 1.8, 5.7, 6.0),
(7, 4, 1, 4, 2.0, 2.7, 7.2, 1.4),
(8, 11, 6, 5, 9.8, 7.8, 1.2, 9.7),
(9, 3, 13, 6, 1.1, 8.5, 2.9, 2.3),
(10, 4, 10, 7, 6.9, 0.2, 0.5, 2.1),
(11, 10, 2, 8, 4.0, 0.8, 5.2, 6.2),
(12, 6, 7, 9, 1.1, 8.4, 9.0, 2.5),
(13, 5, 7, 10, 2.2, 3.4, 2.8, 0.8),
(14, 1, 8, 11, 3.8, 8.1, 7.5, 4.1),
(15, 13, 9, 12, 2.8, 4.1, 9.1, 8.8),
(16, 13, 9, 13, 9.8, 1.8, 9.1, 5.2),
(17, 10, 11, 14, 0.5, 5.2, 6.3, 4.6),
(18, 13, 11, 15, 3.6, 7.9, 4.2, 6.4),
(19, 13, 12, 16, 1.7, 8.3, 6.0, 5.7),
(20, 13, 1, 17, 4.7, 7.2, 2.0, 0.6),
(21, 7, 1, 18, 0.4, 3.5, 2.5, 5.4),
(22, 9, 8, 19, 7.8, 6.6, 8.7, 4.1);

-- --------------------------------------------------------

--
-- Table structure for table `critique_film`
--

CREATE TABLE `critique_film` (
  `id` int(10) UNSIGNED NOT NULL,
  `film_id` int(10) UNSIGNED DEFAULT NULL,
  `critic_id` int(10) UNSIGNED DEFAULT NULL,
  `rate_film` double(2,1) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `critique_film`
--

INSERT INTO `critique_film` (`id`, `film_id`, `critic_id`, `rate_film`) VALUES
(1, 1, 3, 9.9),
(2, 1, 2, 9.3),
(3, 1, 6, 3.5),
(4, 2, 4, 8.8),
(5, 2, 1, 4.5),
(6, 3, 4, 8.1),
(7, 4, 5, 8.7),
(8, 4, 8, 7.3),
(9, 5, 6, 9.6),
(10, 6, 6, 7.6),
(11, 7, 7, 7.6),
(12, 9, 10, 8.0),
(13, 9, 12, 6.0),
(14, 10, 13, 6.7),
(15, 11, 8, 2.3),
(16, 12, 13, 6.7),
(17, 13, 4, 8.7),
(18, 13, 6, 7.2),
(19, 14, 6, 4.3),
(20, 15, 9, 0.3),
(21, 16, 6, 8.9),
(22, 17, 3, 4.5),
(23, 18, 9, 9.0),
(24, 19, 10, 2.2),
(25, 8, 11, 9.9),
(26, 8, 2, 9.3),
(27, 8, 6, 3.5),
(28, 5, 14, 8.8),
(29, 2, 14, 4.5),
(30, 3, 15, 8.1),
(31, 4, 15, 8.7),
(32, 8, 8, 7.3),
(33, 14, 6, 9.6),
(34, 6, 11, 7.6);

-- --------------------------------------------------------

--
-- Table structure for table `directors`
--

CREATE TABLE `directors` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(32) DEFAULT NULL,
  `lastname` varchar(64) DEFAULT NULL,
  `expertise` varchar(32) DEFAULT NULL,
  `genre_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `directors`
--

INSERT INTO `directors` (`id`, `firstname`, `lastname`, `expertise`, `genre_id`) VALUES
(1, 'Dodo', 'Abashidze', 'SciFi', 10),
(2, 'George', 'Abbott', 'Fantasy', 6),
(3, 'Abiola', 'Abrams', 'Action', 1),
(4, 'J.', 'J. Abrams', 'Adventure', 2),
(5, 'Ivan', 'Abramson', 'Romance', 9),
(6, 'Hany', 'Abu-Assad', 'Historical', 7),
(7, 'Tengiz', 'Abuladze', 'Horror', 8),
(8, 'Herbert', 'Achternbusch', 'Comedy', 3),
(9, 'Andrew', 'Adamson', 'Horror', 8),
(10, 'Maren', 'Ade', 'Mystery', 5),
(11, 'Carine', 'Adler', 'Adventure', 2),
(12, 'Percy', 'Adlon', 'Fantasy', 6),
(13, 'John', 'G. Adolfi', 'Thriller', 11),
(14, 'Francis', 'Ford Coppola', 'Crime', 4);

-- --------------------------------------------------------

--
-- Table structure for table `films`
--

CREATE TABLE `films` (
  `id` int(10) UNSIGNED NOT NULL,
  `movie_id` int(10) UNSIGNED DEFAULT NULL,
  `sequel_id` int(10) UNSIGNED DEFAULT NULL,
  `director_id` int(10) UNSIGNED DEFAULT NULL,
  `format` enum('2D','3D') NOT NULL,
  `money_firrst_week` mediumint(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `films`
--

INSERT INTO `films` (`id`, `movie_id`, `sequel_id`, `director_id`, `format`, `money_firrst_week`) VALUES
(1, 1, NULL, 1, '2D', 1264000),
(2, 2, NULL, 2, '2D', 1870001),
(3, 3, NULL, 3, '2D', 1024561),
(4, 4, NULL, 4, '2D', 7045684),
(5, 5, NULL, 5, '3D', 7465611),
(6, 6, NULL, 5, '2D', 6012430),
(7, 7, NULL, 6, '2D', 4786130),
(8, 8, NULL, 7, '3D', 7841623),
(9, 9, NULL, 8, '2D', 7872486),
(10, 10, NULL, 9, '2D', 7843113),
(11, 11, NULL, 10, '3D', 7546128),
(12, 12, NULL, 11, '2D', 4578912),
(13, 13, 12, 12, '2D', 1457893),
(14, 14, NULL, 12, '3D', 4578912),
(15, 15, 14, 12, '3D', 7546128),
(16, 16, 15, 12, '2D', 4579612),
(17, 17, 16, 12, '3D', 7564234),
(18, 18, 17, 12, '3D', 5483478),
(19, 19, 18, 12, '3D', 1456789);

-- --------------------------------------------------------

--
-- Table structure for table `genre`
--

CREATE TABLE `genre` (
  `id` int(10) UNSIGNED NOT NULL,
  `genre` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `genre`
--

INSERT INTO `genre` (`id`, `genre`) VALUES
(1, 'Action'),
(2, 'Adventure'),
(3, 'Comedy'),
(4, 'Crime'),
(5, 'Mystery'),
(6, 'Fantasy'),
(7, 'Historical'),
(8, 'Horror'),
(9, 'Romance'),
(10, 'SciFi'),
(11, 'Thriller'),
(12, 'Drama');

-- --------------------------------------------------------

--
-- Table structure for table `movie`
--

CREATE TABLE `movie` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_of_movie` varchar(64) DEFAULT NULL,
  `premiere` tinyint(1) NOT NULL DEFAULT 1,
  `premiere_date` date DEFAULT NULL,
  `genre_id` int(10) UNSIGNED DEFAULT NULL,
  `production_id` int(10) UNSIGNED DEFAULT NULL,
  `country_of_origin` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `movie`
--

INSERT INTO `movie` (`id`, `name_of_movie`, `premiere`, `premiere_date`, `genre_id`, `production_id`, `country_of_origin`) VALUES
(1, 'The Godfather', 1, '1987-02-01', 4, 1, 2),
(2, 'No Country for Old Men', 1, '2007-05-17', 11, 5, 14),
(3, 'The Shawshank Redemption', 1, '1994-09-10', 12, 3, 9),
(4, 'Seven', 1, '1995-09-15', 4, 2, 5),
(5, 'The Game', 1, '2006-10-01', 11, 3, 4),
(6, 'The Social Network', 1, '2010-01-10', 12, 1, 4),
(7, 'La La Land', 1, '2016-11-09', 9, 2, 10),
(8, 'Black Panter', 1, '2020-07-08', 10, 7, 14),
(9, 'Once Upon a Time in ... Hollywood', 1, '2019-07-24', 3, 8, 12),
(10, 'Dinkirk', 1, '2017-07-13', 7, 2, 15),
(11, 'Lady Bird', 1, '2018-02-15', 3, 1, 6),
(12, 'Alien 1', 1, '1979-05-25', 10, 6, 13),
(13, 'Alien 2', 1, '1986-07-18', 10, 6, 4),
(14, 'Predator 1', 1, '1987-06-12', 10, 6, 1),
(15, 'Predator 2', 1, '1990-10-21', 10, 6, 6),
(16, 'Alien 3', 1, '1992-05-22', 10, 6, 8),
(17, 'Alien Resurrection', 1, '1997-05-22', 10, 6, 6),
(18, 'Alien vs Predator', 1, '2004-08-13', 10, 6, 7),
(19, 'Prometheus', 1, '2012-06-08', 10, 6, 6),
(20, 'The Witcher', 0, NULL, 10, 9, 11),
(21, 'Ozark', 0, NULL, 12, 9, 9),
(22, 'Stranger Things', 0, NULL, 8, 9, 6),
(23, 'Loki', 0, NULL, 6, 9, 4),
(24, 'Dr. House', 0, NULL, 12, 9, 3);

-- --------------------------------------------------------

--
-- Table structure for table `oscars_for_actor`
--

CREATE TABLE `oscars_for_actor` (
  `id` int(10) UNSIGNED NOT NULL,
  `film_id` int(10) UNSIGNED DEFAULT NULL,
  `actor_id` int(10) UNSIGNED DEFAULT NULL,
  `oscars` enum('Best Actor','Best Actress','Best Supporting Actor','Best Supporting Actress') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `oscars_for_actor`
--

INSERT INTO `oscars_for_actor` (`id`, `film_id`, `actor_id`, `oscars`) VALUES
(2, 6, 6, 'Best Supporting Actor'),
(4, 2, 3, 'Best Actor'),
(5, 3, 13, 'Best Supporting Actress'),
(6, 4, 10, 'Best Actor'),
(8, 5, 7, 'Best Actor');

-- --------------------------------------------------------

--
-- Table structure for table `production`
--

CREATE TABLE `production` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_of_production` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `production`
--

INSERT INTO `production` (`id`, `name_of_production`) VALUES
(1, 'Paramount Pictures'),
(2, 'Warner Bros'),
(3, 'Sony Pictures'),
(4, 'Walt Disney'),
(5, 'Universal Pictures'),
(6, '20th Century Fox'),
(7, 'Marvel'),
(8, 'Columbia Pictures'),
(9, 'Netflix');

-- --------------------------------------------------------

--
-- Table structure for table `tv_series`
--

CREATE TABLE `tv_series` (
  `id` int(10) UNSIGNED NOT NULL,
  `movie_id` int(10) UNSIGNED DEFAULT NULL,
  `tv_channels` enum('ABC','CBS','The CW','Fox','ION','MyNetworkTV','NBC') NOT NULL,
  `n_session` int(10) UNSIGNED DEFAULT NULL,
  `series_per_session` int(10) UNSIGNED DEFAULT NULL,
  `n_serries` int(10) UNSIGNED GENERATED ALWAYS AS (`series_per_session` * `n_session`) VIRTUAL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tv_series`
--

INSERT INTO `tv_series` (`id`, `movie_id`, `tv_channels`, `n_session`, `series_per_session`) VALUES
(1, 20, 'Fox', 2, 15),
(2, 21, 'ABC', 5, 10),
(3, 22, 'MyNetworkTV', 5, 9),
(4, 23, 'The CW', 5, 15),
(5, 24, 'NBC', 8, 12);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actors`
--
ALTER TABLE `actors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `actors_in_movie`
--
ALTER TABLE `actors_in_movie`
  ADD PRIMARY KEY (`id`),
  ADD KEY `movie_id` (`movie_id`),
  ADD KEY `actor_id` (`actor_id`);

--
-- Indexes for table `country_of_origin`
--
ALTER TABLE `country_of_origin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `critics`
--
ALTER TABLE `critics`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `password` (`password`);

--
-- Indexes for table `critique_actor`
--
ALTER TABLE `critique_actor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `film_id` (`film_id`),
  ADD KEY `critic_id` (`critic_id`),
  ADD KEY `actor_id` (`actor_id`);

--
-- Indexes for table `critique_film`
--
ALTER TABLE `critique_film`
  ADD PRIMARY KEY (`id`),
  ADD KEY `film_id` (`film_id`),
  ADD KEY `critic_id` (`critic_id`);

--
-- Indexes for table `directors`
--
ALTER TABLE `directors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `genre_id` (`genre_id`);

--
-- Indexes for table `films`
--
ALTER TABLE `films`
  ADD PRIMARY KEY (`id`),
  ADD KEY `movie_id` (`movie_id`),
  ADD KEY `director_id` (`director_id`),
  ADD KEY `sequel_id` (`sequel_id`);

--
-- Indexes for table `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `movie`
--
ALTER TABLE `movie`
  ADD PRIMARY KEY (`id`),
  ADD KEY `genre_id` (`genre_id`),
  ADD KEY `country_of_origin` (`country_of_origin`),
  ADD KEY `production_id` (`production_id`);

--
-- Indexes for table `oscars_for_actor`
--
ALTER TABLE `oscars_for_actor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `film_id` (`film_id`),
  ADD KEY `actor_id` (`actor_id`);

--
-- Indexes for table `production`
--
ALTER TABLE `production`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tv_series`
--
ALTER TABLE `tv_series`
  ADD PRIMARY KEY (`id`),
  ADD KEY `movie_id` (`movie_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actors`
--
ALTER TABLE `actors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `actors_in_movie`
--
ALTER TABLE `actors_in_movie`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `country_of_origin`
--
ALTER TABLE `country_of_origin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `critics`
--
ALTER TABLE `critics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `critique_actor`
--
ALTER TABLE `critique_actor`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `critique_film`
--
ALTER TABLE `critique_film`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `directors`
--
ALTER TABLE `directors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `films`
--
ALTER TABLE `films`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `genre`
--
ALTER TABLE `genre`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `movie`
--
ALTER TABLE `movie`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `oscars_for_actor`
--
ALTER TABLE `oscars_for_actor`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `production`
--
ALTER TABLE `production`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tv_series`
--
ALTER TABLE `tv_series`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `actors_in_movie`
--
ALTER TABLE `actors_in_movie`
  ADD CONSTRAINT `actors_in_movie_ibfk_1` FOREIGN KEY (`actor_id`) REFERENCES `actors` (`id`),
  ADD CONSTRAINT `actors_in_movie_ibfk_2` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`id`);

--
-- Constraints for table `critique_actor`
--
ALTER TABLE `critique_actor`
  ADD CONSTRAINT `critique_actor_ibfk_1` FOREIGN KEY (`film_id`) REFERENCES `films` (`id`),
  ADD CONSTRAINT `critique_actor_ibfk_2` FOREIGN KEY (`critic_id`) REFERENCES `critics` (`id`),
  ADD CONSTRAINT `critique_actor_ibfk_3` FOREIGN KEY (`actor_id`) REFERENCES `actors` (`id`);

--
-- Constraints for table `critique_film`
--
ALTER TABLE `critique_film`
  ADD CONSTRAINT `critique_film_ibfk_1` FOREIGN KEY (`film_id`) REFERENCES `films` (`id`),
  ADD CONSTRAINT `critique_film_ibfk_2` FOREIGN KEY (`critic_id`) REFERENCES `critics` (`id`);

--
-- Constraints for table `directors`
--
ALTER TABLE `directors`
  ADD CONSTRAINT `directors_ibfk_1` FOREIGN KEY (`genre_id`) REFERENCES `genre` (`id`);

--
-- Constraints for table `films`
--
ALTER TABLE `films`
  ADD CONSTRAINT `films_ibfk_1` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`id`),
  ADD CONSTRAINT `films_ibfk_2` FOREIGN KEY (`director_id`) REFERENCES `directors` (`id`),
  ADD CONSTRAINT `films_ibfk_3` FOREIGN KEY (`sequel_id`) REFERENCES `films` (`id`);

--
-- Constraints for table `movie`
--
ALTER TABLE `movie`
  ADD CONSTRAINT `movie_ibfk_1` FOREIGN KEY (`genre_id`) REFERENCES `genre` (`id`),
  ADD CONSTRAINT `movie_ibfk_2` FOREIGN KEY (`country_of_origin`) REFERENCES `country_of_origin` (`id`),
  ADD CONSTRAINT `movie_ibfk_3` FOREIGN KEY (`production_id`) REFERENCES `production` (`id`);

--
-- Constraints for table `oscars_for_actor`
--
ALTER TABLE `oscars_for_actor`
  ADD CONSTRAINT `oscars_for_actor_ibfk_1` FOREIGN KEY (`film_id`) REFERENCES `films` (`id`),
  ADD CONSTRAINT `oscars_for_actor_ibfk_2` FOREIGN KEY (`actor_id`) REFERENCES `actors` (`id`);

--
-- Constraints for table `tv_series`
--
ALTER TABLE `tv_series`
  ADD CONSTRAINT `tv_series_ibfk_1` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
