<!DOCTYPE html>
<html lang="en">
<?php
//Add script to read MySQL data and export to excel
include "export.php";

?>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous" />

    <!-- Local CSS -->
    <link rel="stylesheet" type="text/css" href="./style.css" />

    <!-- Font-awesome 5 cdn -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous" />
</head>
<style>
@import url('https://fonts.googleapis.com/css2?family=Kings&display=swap');
</style>
<body>
    <div class="container-fluid bg-light">
        <div class="row">
            <div class="col">
                <div class="container ">
                    <div class="row">
                        <div class="col">
                            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                                <a class="navbar-brand" href="#">
                                    <img src="./smetki/logo-jeger.png" width="50" height="50" alt="logo-jegermaister">
                                    <img src="./smetki/jeger-text.png" width="170" height="30" alt="logo-jegermaister">
                                </a>
                                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul class="navbar-nav ml-auto">
                                        <li class="nav-item">
                                            <a class="nav-link king-btn" href="./main.php">Pending<span class="sr-only">(current)</span></a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link king-btn" href="./awardReceipt.php">Awarded</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link king-btn" href="./rejectReceipt.php">Rejected</a>
                                        </li>
                                        <li class="nav-item active">
                                            <a class="nav-link king-btn" href="./createReport.php">Report</a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col">
                <center><br /><br />
                    <h2 style='color:green' class="king-btn">Regermaister Table Information</h2>
                </center>
                <div class="col-sm-12">
                    <div>
                        <form action="#" method="post">
                            <button type="submit" id="export" name="export" value="Export to excel" class="btn btn-success king-btn">Export To Excel</button>
                        </form>
                    </div>
                </div>
                <br />
                <table id="" class="table table-striped table-bordered">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Image Text</th>
                        <th>Image Status</th>
                        <th>Declined Status</th>
                        <th>award</th>
                    </tr>
                    <tbody>
                        <?php  foreach ($items as $key=>$item) { ?>
                            <tr>
                                <td><?php echo $key+1; ?></td>
                                <td><?php echo $item['email']; ?></td>
                                <td><?php echo $item['image_name']; ?></td>
                                <td><?php echo substr($item['image_text'], 0, 200);; ?></td>
                                <td><?php echo $item['img_status']== '1' ? "receipt" : "maybe receipt" ?></td>
                                <td><?php echo $item['image_declined']==1 ? "declined" : "not declined" ?></td>
                                <td><?php echo $item['award']; ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js" integrity="sha384-VHvPCCyXqtD5DqJeNxl2dtTyhF78xXNXdkwX1CZeRusQfRKp+tA7hAShOK/B/fQ2" crossorigin="anonymous"></script>
</body>

</html>