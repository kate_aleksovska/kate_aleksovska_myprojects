<?php
session_start();
include 'db.php';
include 'functions.php';

$sqlUser = "SELECT * FROM users";
$stmtUser = $pdo->query($sqlUser);
$sqlAward = "SELECT * FROM award";
$stmtAward = $pdo->query($sqlAward);
$arrAward = [];

while ($arr = $stmtAward->fetch()) {
    $settings[$arr['id']] = $arr['award'];
}
$arr_r = [];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous" />

    <!-- Local CSS -->
    <link rel="stylesheet" type="text/css" href="./style.css" />

    <!-- Font-awesome 5 cdn -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous" />
</head>
<style>
@import url('https://fonts.googleapis.com/css2?family=Kings&display=swap');
</style>
<body>
    <div class="container-fluid bg-light">
        <div class="row">
            <div class="col">
                <div class="container ">
                    <div class="row">
                        <div class="col">
                            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                                <a class="navbar-brand" href="#">
                                    <img src="./smetki/logo-jeger.png" width="50" height="50" alt="logo-jegermaister">
                                    <img src="./smetki/jeger-text.png" width="170" height="30" alt="logo-jegermaister">
                                </a>
                                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul class="navbar-nav ml-auto">
                                        <li class="nav-item active">
                                            <a class="nav-link king-btn" href="./main.php">Pending<span class="sr-only">(current)</span></a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link king-btn" href="./awardReceipt.php">Awarded</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link king-btn" href="./rejectReceipt.php">Rejected</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link king-btn" href="./createReport.php">Report</a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-3">
                <button class='btn btn-warning inline-block ml-2 mr-auto king-btn' onclick="rand()">Award random user</button>
            </div>
            <div class="col-9 d-flex justify-content-end">
                <button class='btn .bg-gradient-primary ml-1 inline-block king-btn' id="allReceipt">All receipt</button>
                <button class='btn bg-gradient-primary ml-1 nline-block king-btn' id="maybeReceipt">Maybe Receipt</button>
                <button class='btn bg-gradient-primary ml-1 inline-block king-btn' id="isReceipt">Is Receipt</button>
            </div>

        </div>
        <div class="row justify-content-center">
            <?php if (isset($_SESSION['success'])) {
                echo "<div class='alert bg-transparent text-center text-success m-2 king-btn m-2' role='alert'>
                                {$_SESSION['success']}";
                echo   "</div>";
                unset($_SESSION['success']);
            }
            if (isset($_SESSION['error'])) {
                echo "<div class='alert bg-transparent text-center text-danger m-2 king-btn m-2' role='alert'>
                {$_SESSION['error']}";

                echo   "</div>";
                unset($_SESSION['error']);
            }
            ?>
        </div>
        <div class="row justify-content-center">
            <?php while ($row = $stmtUser->fetch()) {

                if (is_null($row['image_declined']) && is_null($row['award_id'])) {
                    array_push($arr_r, $row['id']);
                    $text = substr($row['image_text'], 0, 200);
                    echo    "<div class='card m-3 col-3 px-0 king-btn' style='width: 18rem;' id='{$row['id']}'>
               <img class='card-img-top img-fluid' src='./images/{$row['image_name']}' alt='Card image cap'>
               <div class='card-body'>
                 <h6 class='card-title'>{$row['email']}</h6>
                 <p class='card-text'>Receipt text:<br>{$text}</p>";
            ?>
                    <form class='inline-block' action="rejected.php?id=<?= urlencode(encrypt($row['id'])) ?>" method='POST'>

                        <button class='btn btn-danger inline-block ml-2'>Reject</button>
                    </form>
                    <?php echo "<button type='button' class='btn btn-success text-capitalize mr-2' data-toggle='modal' data-target='#exampleModal{$row['id']}' data-whatever='@{$row['id']}'>
                 Award <i class='fas fa-chevron-circle-right text-warning'></i>
                </button>
                
             <div class='modal fade bg-image2' id='exampleModal{$row['id']}' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
                 <div class='modal-dialog box-shadow' role='document'>
                     <div class='modal-content'>
                         <div class='modal-body m-3'>
                         <div class='card' style='width: 18rem;'>
                         <img class='card-img-top img-fluid' src='./images/{$row['image_name']}' alt='Card image cap'>
                         <div class='card-body'>
                         <h6 class='card-title'>{$row['email']}</h6>
                           <p class='card-text'>{$text}</p>         
                         </div>
                         </div>"
                    ?>
                    <form action="awarded.php?id=<?= urlencode(encrypt($row['id'])) ?>&email=<?= $row['email'] ?>" method='POST'>

                        <?php echo "<div class='form-group'>
                         <label for='exampleFormControlSelect{$row['id']}'>Select award for client</label>
                         <select class='form-control' id='exampleFormControlSelect{$row['id']}' name='award_id'>"
                        ?>

                        <?php foreach ($settings as $key => $value) {
                            echo  "<option value='{$key}'>{$value}</option>";
                        }

                        echo "</select>";
                        echo "</div>";
                        ?>
                <?php echo "<button class='btn btn-success' value='{$row['id']}'>Award</button>
            
                           <button type='button' class='close btn btn-danger' data-dismiss='modal' aria-label='Close' aria-hidden='true'>
                                 Dismiss
                             </button>
                             </form>
                         </div>
                     </div>
                 </div>
             </div>
               </div>
</div>";
                }
            }
                ?>
        </div>
        <!-- </div> -->
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js" integrity="sha384-VHvPCCyXqtD5DqJeNxl2dtTyhF78xXNXdkwX1CZeRusQfRKp+tA7hAShOK/B/fQ2" crossorigin="anonymous"></script>

    <script>
        var randArr = <?php echo json_encode($arr_r) ?>;

        function rand() {


            randArr.forEach(el => {
                document.getElementById(el).style.display = 'block';
            })
            var item = randArr[Math.floor(Math.random() * randArr.length)]
            randArr.forEach(el => {
                el != item ? document.getElementById(el).style.display = 'none' : document.getElementById(el).style.display = 'block';
            })
        }
        $(function() {
            $('#allReceipt').on('click', function(e) {
                randArr.forEach(el => {
                    document.getElementById(el).style.display = 'block';
                })
            })
            $('#maybeReceipt').on('click', function(e) {
                randArr.forEach(el => {
                    document.getElementById(el).style.display = 'block';
                })
                $.ajax({
                    url: "maybeReceipt.php",
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: "json",
                    enctype: "multipart/form-data",
                    success: function(data) {
                        let maybeReceipt = data
                        console.log(data);
                        console.log(randArr)
                        res = randArr.filter(item => !maybeReceipt.includes(item));
                        console.log(res);
                        res.forEach(el => {
                            document.getElementById(el).style.display = 'none';
                        })
                    }
                })
            })
            $('#isReceipt').on('click', function(e) {
                randArr.forEach(el => {
                    document.getElementById(el).style.display = 'block';
                })
                $.ajax({
                    url: "isReceipt.php",
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: "json",
                    enctype: "multipart/form-data",
                    success: function(data) {
                        let isReceipt = data
                        console.log(data);
                        console.log(randArr)
                        res = randArr.filter(item => !isReceipt.includes(item));
                        console.log(res);
                        res.forEach(el => {
                            document.getElementById(el).style.display = 'none';
                        })
                    }
                })
            })
        })
    </script>
</body>

</html>