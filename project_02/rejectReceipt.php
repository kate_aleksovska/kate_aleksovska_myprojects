<?php
session_start();
include 'db.php';
include 'functions.php';

$sqlUser = "SELECT * FROM users";
$stmtUser = $pdo->query($sqlUser);


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous" />

    <!-- Local CSS -->
    <link rel="stylesheet" type="text/css" href="./style.css" />

    <!-- Font-awesome 5 cdn -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous" />
</head>
<style>
@import url('https://fonts.googleapis.com/css2?family=Kings&display=swap');
</style>
<body>
    <div class="container-fluid bg-light">
        <img id='loading' src='./smetki/loader-final.gif' style='display: none;' class="loader">
        <div class="row">
            <div class="col">
                <div class="container ">
                    <div class="row">
                        <div class="col">
                            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                                <a class="navbar-brand" href="#">
                                    <img src="./smetki/logo-jeger.png" width="50" height="50" alt="logo-jegermaister">
                                    <img src="./smetki/jeger-text.png" width="170" height="30" alt="logo-jegermaister">
                                </a>
                                <div class="collapse navbar-collapse " id="navbarSupportedContent">
                                    <ul class="navbar-nav ml-auto">
                                        <li class="nav-item">
                                            <a class="nav-link king-btn" href="./main.php">Pending<span class="sr-only">(current)</span></a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link king-btn" href="./awardReceipt.php">Awarded</a>
                                        </li>
                                        <li class="nav-item active">
                                            <a class="nav-link king-btn" href="./rejectReceipt.php">Rejected</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link king-btn" href="./createReport.php">Report</a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <?php if (isset($_SESSION['success'])) {
                echo "<div class='alert bg-transparent text-center text-success king-btn m-2' role='alert'>
                                {$_SESSION['success']}";
                
                echo   "</div>";
                unset($_SESSION['success']);
            }
            if (isset($_SESSION['error'])) {
                echo "<div class='alert bg-transparent text-center text-danger king-btn m-2' role='alert'>
                                {$_SESSION['error']}";
                
                echo   "</div>";
                unset($_SESSION['error']);
            }
            ?>
        </div>
        <div class="row justify-content-center">
            <!-- <div class="col"> -->
            <?php while ($row = $stmtUser->fetch()) {
                if ($row['image_declined']==1 && is_null($row['award_id'])) {
                    $text = substr($row['image_text'], 0, 200);
                    echo    "<div class='card m-3 king-btn col-3 px-0' style='width: 18rem;'>
               <img class='card-img-top img-fluid' src='./images/{$row['image_name']}' alt='Card image cap'>
               <div class='card-body'>
                 <h6 class='card-title'>{$row['email']}</h6>
                 <p class='card-text'>Receipt text:<br>{$text}</p>";
                 ?>
                 <form class='inline-block' action="deleted.php?id=<?= urlencode(encrypt($row['id']))?>" method='POST'>
                               
                 <button class='btn btn-danger inline-block' value='<?=$row['id']?>'>Delete</button>
                 </form>
                </div>
                </div>
           <?php     }
            } ?>
        </div>
        <!-- </div> -->
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js" integrity="sha384-VHvPCCyXqtD5DqJeNxl2dtTyhF78xXNXdkwX1CZeRusQfRKp+tA7hAShOK/B/fQ2" crossorigin="anonymous"></script>

    <script>

    </script>
</body>

</html>