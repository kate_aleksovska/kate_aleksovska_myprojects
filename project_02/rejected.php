<?php
session_start();
include 'db.php';
include 'functions.php';

checkRequestSignIn();
$get_id = $_GET['id'];


$id = decrypt($get_id);
$image_declined = 1;

$sqlUpdate = "UPDATE users SET image_declined=:image_declined WHERE id = :id ";
$stmtUpdate = $pdo->prepare($sqlUpdate);
$stmtUpdate->bindParam(':image_declined', $image_declined, PDO::PARAM_INT);
$stmtUpdate->bindParam(':id', $id, PDO::PARAM_INT);
$stmtUpdate->execute();
$sqlUser = "SELECT users.email as email FROM `users`
WHERE users.id =:id";

$stmtUser = $pdo->prepare($sqlUser);
   
$stmtUser->execute(['id' => $id]);
if($stmtUpdate->execute()) {
    $rejectedUser=$stmtUser->fetch();
    $_SESSION['error'] = "The user with email {$rejectedUser['email']} has been rejected";
    header('Location: main.php?error');
    die();
}



?>