<?php 
session_start();
include 'db.php';
include 'constants.php';
include 'functions.php';
checkRequestSignIn();
$email = $_POST['email'];
$uploadImage=$_FILES['uploadImage']['name'];
checkIssetUserInput($email, $uploadImage);
if ($_FILES['uploadImage']['size'] > 100000) {
   echo json_encode(["msg" => "File too large, file must be less than 100000kb", "status" => 600]);
   die();
}
if(!empty($email) && (!empty($uploadImage))){
   if(!empty($email)) {
       checkEmail($email);
   }
   if(!empty($uploadImage)) {
    $seconds = 9;
    sleep($seconds);
       $file = checkUploadedImage();
       $image_path = $base_url. basename($file);
       $api = 'https://jager.brainster.xyz/api';
      
       $response = json_decode(file_get_contents($api.'?img='.$image_path.'&iwantstatus=1'));
       if($response->code == 200) {
        if($response->img_status == 0) {
        unlink($file);
         echo  json_encode(["msg" => "Not an image, try again, you have ", "status" => 500]);
            die();
           } else {
              $response_arr["email"] = $email;
              $response_arr["image_name"] = basename($file);
              $response_arr["image_text"] = $response->text;
              $response_arr["img_status"] = $response->img_status;
            $sqlInsert = "INSERT INTO users
            (email, image_name, image_text, img_status)
            VALUES (:email, :image_name, :image_text, :img_status)";
            
            $stmtInsert = $pdo->prepare($sqlInsert);
            if($stmtInsert->execute($response_arr)) {
           echo  json_encode(["msg" => "Successfuly uploaded image", "status" => 200]);
                   die();
                } else {
             echo  json_encode(["msg" => "Error while uploading data in database", "status" => 700]);  
                    die();
                }
            
           }
       }
   
   }
}
