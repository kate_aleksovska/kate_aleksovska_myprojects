<?php session_start();
include 'db.php';
include 'functions.php';

checkRequestSignIn();
$get_id = $_GET['id'];
$id = decrypt($get_id);
$sqlUser = "SELECT users.email as email, users.image_name as imageUrl FROM `users`
WHERE users.id =:id";
$stmtUser = $pdo->prepare($sqlUser);
$stmtUser->execute(['id' => $id]);
$sqlDelete = " DELETE FROM users WHERE id = :id ";
$stmtDelete = $pdo->prepare($sqlDelete);

$stmtDelete->bindParam(':id', $id, PDO::PARAM_INT);
$stmtDelete->execute();

if($stmtDelete->execute()) {
    $rejectedUser=$stmtUser->fetch();
    $rejectedImg = './images/'. $rejectedUser['imageUrl'];
    unlink($rejectedImg);
    $_SESSION['error'] = "The user with email {$rejectedUser['email']} has been deleted";
    header('Location: rejectReceipt.php?error');
    die();
}
?>