<?php
function checkRequestSignIn() {
    if($_SERVER['REQUEST_METHOD'] != "POST") {
        redirect(INDEX_PAGE);
        die();
    } 
}
function checkIssetAdminInput($username, $password) {
    if(empty($username) || empty($password)) {
        $_SESSION['errorAdmin'] = "admin must enter all fields";
        header('Location: index.php');
        die();
    } 
}
function checkIssetUserInput($email, $uploadImage) {
    if(empty($email) || empty($uploadImage)) {
        echo  json_encode(["msg" => "User must enter all field", "status" => 600]);
        die();
    } 
}

function generateRandomString($length) {
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}
function checkUploadedImage() {
    $target_dir = "./images/";
    $imageFileType = strtolower(pathinfo($_FILES['uploadImage']['name'],PATHINFO_EXTENSION));
$target_file = $target_dir . generateRandomString(32). '.'. $imageFileType;
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" ) {
    echo  json_encode(["msg" => "Sorry, only JPG, JPEG, PNG & GIF files are allowed.", "status" => 600]);
        die();  
}
move_uploaded_file($_FILES['uploadImage']['tmp_name'], $target_file);
return $target_file;
}
function checkAndPrintErrorMessage() {
    if(isset($_GET['error'])) {
        $arr=explode(".", trim($_GET['error']));
        $msg='<p class="alert alert-danger text-center text-danger pb=0">';
        foreach($arr as $error) {
        $msg .= $error .'.</br>';
        
    }
    echo $msg .'</p>';
    }
}
function checkEmail($email) {
    $emailArr=explode("@", $email);
    $emailCharCheck=strlen($emailArr[0]);
  if(!filter_var($email, FILTER_VALIDATE_EMAIL) || $emailCharCheck < 5) {
    echo  json_encode(["msg" => "Email is not valid, must enter minimum 5 characters before @", "status" => 600]);
    die();
}
}
$encryptionKey = 'lkjasfhkj2hj2123qw';

function encrypt($plaintext)
{
    global $encryptionKey;
    $ivlen = openssl_cipher_iv_length($cipher = "AES-128-CBC");
    $iv = openssl_random_pseudo_bytes($ivlen);
    $ciphertext_raw = openssl_encrypt($plaintext, $cipher, $encryptionKey, $options = OPENSSL_RAW_DATA, $iv);
    $hmac = hash_hmac('sha256', $ciphertext_raw, $encryptionKey, $as_binary = true);
    $ciphertext = base64_encode($iv . $hmac . $ciphertext_raw);

    return $ciphertext;
}


function decrypt($ciphertext)
{
    global $encryptionKey;
    $c = base64_decode($ciphertext);
    $ivlen = openssl_cipher_iv_length($cipher = "AES-128-CBC");
    $iv = substr($c, 0, $ivlen);
    $hmac = substr($c, $ivlen, $sha2len = 32);
    $ciphertext_raw = substr($c, $ivlen + $sha2len);
    $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $encryptionKey, $options = OPENSSL_RAW_DATA, $iv);
    $calcmac = hash_hmac('sha256', $ciphertext_raw, $encryptionKey, $as_binary = true);
    if (hash_equals($hmac, $calcmac)) // timing attack safe comparison
    {
        return $original_plaintext;
    }
}

function redirect($url, $queryString = '') {
    if($queryString != '') {
        $url .= "?$queryString";
    }

    header("Location:". $url);
    die();
}