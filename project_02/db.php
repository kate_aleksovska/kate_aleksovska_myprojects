<?php

try {
  $pdo = new PDO("mysql:host=localhost;dbname=jegermaister", "root", "", [PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC]);
}catch(PDOException $e) {
    file_put_contents("log.txt", date("Y-m-d H:i:s"). ": {$e->getMessage()}". PHP_EOL, FILE_APPEND);
    die("connection failed");
}