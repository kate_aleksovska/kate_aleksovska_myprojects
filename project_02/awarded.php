<?php

session_start();
include 'db.php';
include 'functions.php';


checkRequestSignIn();
$getId=$_GET['id'];
$email=$_GET['email'];
$id = decrypt($getId);

$award_id= $_POST['award_id'];

$sqlInsert = "UPDATE users SET award_id=:award_id WHERE id = :id ";

$stmtInsert = $pdo->prepare($sqlInsert);
$stmtInsert->bindParam(':award_id', $award_id, PDO::PARAM_INT);
$stmtInsert->bindParam(':id', $id, PDO::PARAM_INT);
$stmtInsert->execute();
$sqlUser = "SELECT users.*, award.award as award
FROM `users` INNER JOIN award on users.award_id = award.id
WHERE users.id =:id";

$stmtUser = $pdo->prepare($sqlUser);
   
$stmtUser->execute(['id' => $id]);
    
if($stmtInsert->execute() && $stmtUser->rowCount() > 0) {
    $awardedUser=$stmtUser->fetch();
    $to      = $email;
    $subject = "Congrats! You’ve won the {$awardedUser['award']} from Jagermeister Giveaway’s contest";
    $message = "Hi,

    Congratulations, you’ve won the {$awardedUser['award']} from Jagermeister Giveaway’s contest!
    
    To claim your prize, please follow these steps:
    
    Confirm that you meet all of the entry requirements
    Send an email to  brainstertest@gmail.com within 5 days to claim your prize
    In the email, please confirm that’s it OK for us to publish your name on our social channels
    This one is totally optional, but if you’re excited about winning, take a selfie and share it with us!
    If you have any questions, just email us :)
    
    Yours sincerely,
    
    Jagermeister";
    $headers = 'From: brainstertest@gmail.com' . "\r\n";
    if(mail($to, $subject, $message, $headers)) {
    
  //  echo "Email successfully sent to $to_email...";
    header("Location: main.php?success");
$_SESSION['success'] = "The user with email $email has been awarded with {$awardedUser['award']}";  
die(); 
} else {
    header("Location: main.php?error");
    $_SESSION['error'] = "The user with email $email has not been awarded with ";  
}  
} else {
        $_SESSION['error'] = "Error in database";
        header('Location: main.php?error');
        die();
     }
?>