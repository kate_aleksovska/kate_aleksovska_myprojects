<?php
session_start();
include 'db.php';
include 'functions.php';

$sqlUser = "SELECT users.*, award.award as award
FROM `users` LEFT JOIN award on users.award_id = award.id";
$stmtUser = $pdo->query($sqlUser);

$items = array();

//Store table records into an array
while($row = $stmtUser->fetch()) {
$items[] = $row;
}
//Check the export button is pressed or not
if(isset($_POST["export"])) {
//Define the filename with current date
$fileName = "itemdata-".date('d-m-Y').".xls";

//Set header information to export data in excel format
header("Content-Type: application/xls"); 
header('Content-Disposition: attachment; filename='.$fileName);
header("Pragma: no-cache"); 
header("Expires: 0");

//Set variable to false for heading
echo '<table border="1">';
//make the column headers what you want in whatever order you want
echo '<tr><th>id</th><th>Email</th><th>Image</th><th>Image Text</th><th>Image Status</th><th>Image Declined</th><th>Award</th></tr>';
//loop the query data to the table in same order as the headers
foreach ($items as $key=>$item){
    echo "<tr><td>".($key+1)."</td><td>".$item['email']."</td><td>".$item['image_name']."</td>
    <td>".substr($item['image_text'], 0, 200)."</td>
    <td>".($item['img_status']== '1' ? 'receipt' : 'maybe receipt')."</td>
    <td>".($item['image_declined']==1 ? 'declined' : 'not declined')."</td><td>".$item['award']."</td></tr>";
}
echo '</table>';
exit();
}

?>