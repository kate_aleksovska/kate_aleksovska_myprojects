<?php
session_start();
include 'functions.php';

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jagermeister</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous" />

    <!-- Local CSS -->
    <link rel="stylesheet" type="text/css" href="./style.css" />

    <!-- Font-awesome 5 cdn -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous" />
    <link rel="stylesheet" href="./css/animate.css">
</head>
<style>
@import url('https://fonts.googleapis.com/css2?family=Kings&display=swap');
</style>
<body>
    <div class="container-fluid bg-transparent navbar-dark bg-image">
        <div  style='display: none;' class="bg-image3 p-0 m-0" id="bg-image3"></div>
        <img id='loading' src='./smetki/gif-loader-flad.gif' style='display: none;' class="loader">
       
        <div class="row">
            <div class="col ">
                <div class="container-fluid bg-transparent navbar-dark m-0 p-0">
                    <div class="row">
                        <div class="col ">
                            <nav class="navbar navbar-expand-sm bg-transparent navbar-dark m-0 p-0">
                                <a class="navbar-brand" href="#">
                                </a>
                                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul class="navbar-nav mr-auto">

                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                             <span class="king-style">  Admin</span>
                                            </a>
                                            <div class="dropdown-menu bg-transparent box-shadow" aria-labelledby="navbarDropdown">
                                                <?php
                                                echo '<form class="p-2 king-btn bg-transparent navbar text-warning" method="POST" action="signInAdmin.php" novalidate>
                                                    <div class="form-group bg-transparent">
                                                        <label for="username">Enter username</label>
                                                        <input type="text" class="form-control" id="username" name="username">
                                                    </div>
                                                    <div class="form-group bg-transparent" >
                                                        <label for="password">Password</label>
                                                        <input type="password" class="form-control" id="password" name="password">
                                                    </div>
                                                    <div class="text-center">
                                                        <button type="submit" class="btn btn-outline-warning text-capitalize text-center" name="submit">Log in <img src="./smetki/jagermeister.png" alt="" srcset=""> </button>
                                                    </div>
                                                </form>'
                                                ?>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  
    <div class="container-fluid  ">
        <div class="row flex-column justify-content-end">
            <div class="col">
                <div class="container" style="padding-top: 175px">
                    <div class="row flex-column justify-content-end  m-5 align-items-center">
                        <div class="col">
                            <?php

                            if (isset($_SESSION['errorAdmin'])) {
                                echo "<div class='alert bg-transparent text-center text-danger m-2 king-style' role='alert'>
                                {$_SESSION['errorAdmin']}";
                                unset($_SESSION['errorAdmin']);
                                echo   "</div>";
                            }
                            if (isset($_SESSION['error'])) {
                                echo "<div class='alert bg-transparent text-center text-danger m-2 king-style' role='alert'>
                                {$_SESSION['error']}";
                                unset($_SESSION['error']);
                                echo   "</div>";
                            }
                            if (isset($_SESSION['errorUser'])) {
                                echo "<div class='alert bg-gransparent text-danger text-center m-2 king-style' role='alert'>
                                {$_SESSION['errorUser']}";
                                unset($_SESSION['errorUser']);
                                echo   "</div>";
                            }
                            ?>
                            <div id="msg"></div>
                        </div>
                        <div class="col">
                            <div id="carouselExampleInterval" class="carousel slide m-0" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="carousel-item active text-center" data-interval="10000">
                                         <div class="text-light king-style">No band and alcohol are associated more closer than Slayer and Jager.🤟</div>
                                    </div> 
                                    <div class="carousel-item text-center" data-interval="10000">
                                    <img src="./smetki/ice-cold-jeger.gif" alt="pointer-arrow" width="100" height="100">
                                    </div> 
                                    <div class="carousel-item text-center king-style" data-interval="2000">
                                        <label class="king-style text-light">Tap here</label>
                                        <img src="./smetki/tap-here-yellow-arrowgif.gif" alt="pointer-arrow" class="pointer-arrow" width="100" height="100">
                                    </div>
                                </div>
                                <button class="carousel-control-prev" type="button" data-target="#carouselExampleInterval" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </button>
                                <button class="carousel-control-next" type="button" data-target="#carouselExampleInterval" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </button>
                            </div>
                        </div>
                        <div class="p-3 text-center">

                            <button type="button" class="btn btn-outline-warning text-capitalize " data-toggle="modal" data-target="#exampleModal" id="givAway">
                                Enter the givaway <i class="fas fa-chevron-circle-right text-warning"></i>
                            </button>
                            <div class="modal fade bg-image2" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content king-btn">
                                        <div class="modal-header p-0">
                                            <span class="px-3">Participant information</span>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form class="p-2" id="formUser" novalidate>
                                                <div class="form-group">
                                                    <label for="email">Enter email:</label>
                                                    <input type="email" class="form-control" id="email" name="email">
                                                </div>
                                                <div class="form-group">
                                                    <label for="uploadImage">Upload image:</label>
                                                    <input type="file" class="form-control" id="uploadImage" name="uploadImage">
                                                </div>
                                                <input type="submit" class="btn btn-primary" id="submitUser">
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js" integrity="sha384-VHvPCCyXqtD5DqJeNxl2dtTyhF78xXNXdkwX1CZeRusQfRKp+tA7hAShOK/B/fQ2" crossorigin="anonymous"></script>
    <script>
        $(function(e) {
           let myCookie = getCookie('cookie');
            console.log(myCookie)
             let k = localStorage.getItem('counter');
             if(myCookie) {
                $('#givAway').prop('disabled', true);
             } else {
            $('#formUser').on('submit', (function(e) {
                e.preventDefault();
                console.log("ima");
                var formData = new FormData(this);
                $('#loading').attr("style", "display: block");
                $('#bg-image3').attr("style", "display: block");
                $(".close").click()
                $.ajax({
                    type: 'POST',
                    url: 'signInUser.php',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: "text json",
                    enctype: "multipart/form-data",
                }).then(data => {
                    if (data.status == 200) {
                        $('#msg').html(`<div class='alert  bg-transparent text-center text-success m-2 king-style' role='alert'>${data.msg}</div>`)
                        $('#loading').attr("style", "display: none")
                        $('#bg-image3').attr("style", "display: none");
                    } else if (data.status == 500) {
                        $('#loading').attr("style", "display: none")
                        $('#bg-image3').attr("style", "display: none");
                        let n = localStorage.getItem('counter');
                        if (n === null) {
                            n = 1;
                            r=3-n
                            $('#givAway').prop('disabled', false);
                            $('#msg').html(`<div class='alert  bg-transparent text-center text-danger m-2 king-style' role='alert'>${data.msg}${r} times left</div>`)
                        } else if(n<2){
                            n++;
                            k=3-n
                            $('#msg').html(`<div class='alert bg-transparent text-center text-danger m-2 king-style' role='alert'>${data.msg}${k} time left</div>`)
                            $('#givAway').prop('disabled', false);
                        } else {
                            $('#msg').html(`<div class='alert bg-transparent text-center text-danger m-2 king-style' role='alert'>Try again in 24h</div>`) 
                            $('#givAway').prop('disabled', true);
                            display()
                            return
                        }
                        console.log(n);
                        localStorage.setItem("counter", n);
                    } else if (data.status == 600) {
                        $('#msg').html(`<div class='alert bg-transparent text-center text-danger m-2 king-style' role='alert'>${data.msg}</div>`)
                        $('#loading').attr("style", "display: none")
                        $('#bg-image3').attr("style", "display: none");
                      
                    }
                }).fail(error => {
                    console.log("Error: ", error)
                });
            }));
        }
        });
function display() { 
  var now = new Date();
  var time = now.getTime();
  var expireTime = time + 24 * 60 * 60 * 1000;
  now.setTime(expireTime);
  document.cookie = 'cookie=ok;expires='+now.toUTCString()+';path=/';
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

    </script>

</body>

</html>