-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 12, 2021 at 10:04 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jegermaister`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(10) UNSIGNED NOT NULL,
  `user` varchar(64) DEFAULT NULL,
  `pass` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `user`, `pass`) VALUES
(1, 'mkaleka', '$2y$10$rFdEvtNa9.vOA4w/5NGNF.a6.Y.9runAUXNQtkKrXR7OZhVJdbuFS');

-- --------------------------------------------------------

--
-- Table structure for table `award`
--

CREATE TABLE `award` (
  `id` int(10) UNSIGNED NOT NULL,
  `award` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `award`
--

INSERT INTO `award` (`id`, `award`) VALUES
(1, 'Jegermaister metal cap'),
(2, 'Metal T-shirt'),
(3, 'Limited edition slayer bottle'),
(4, 'Slayer concert tickets');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(64) NOT NULL,
  `image_name` varchar(64) DEFAULT NULL,
  `image_text` longtext NOT NULL,
  `img_status` int(11) DEFAULT NULL,
  `image_declined` tinyint(1) DEFAULT NULL,
  `award_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `image_name`, `image_text`, `img_status`, `image_declined`, `award_id`) VALUES
(2, 'katea911807@gmail.com', 'SrMogwPZHUfTuQn3qYmtxi9sdC1yN2DG.png', 'Nulla ut enim quis mi interdum blandit ut sit amet arcu. Ut molestie turpis at egestas eleifend. Nunc vel leo arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nullam tempus quam vitae blandit venenatis. Morbi vitae magna quam. Phasellus quis semper odio. Nam nec nibh faucibus, maximus tellus at, aliquam tortor. Sed vulputate in ipsum ornare tempus. Proin metus libero, vehicula a justo et, tempus maximus justo. Nam in nisi dignissim, convallis arcu id, volutpat diam. Cras non dolor sed purus commodo pulvinar at ut massa. Nunc placerat dolor eget turpis aliquam fringilla. Duis sed commodo eros, nec hendrerit libero.', 2, NULL, 1),
(93, 'katea911807@gmail.com', 'H4FU5wbXm7qacZ0tTJyPDvSolQh8suYC.jpg', 'Suspendisse potenti. Cras ut risus sapien. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam vestibulum nibh non congue aliquet. Ut nec suscipit ipsum. Sed mauris lectus, cursus nec est a, sodales hendrerit ligula. Fusce blandit, risus nec sodales consectetur, tortor risus feugiat eros, eget dapibus leo ante quis tellus. Quisque sit amet risus porta, volutpat leo sed, elementum mi. Aliquam ornare facilisis diam ut interdum. Vivamus dignissim quis urna ac sagittis. Praesent nibh mauris, commodo sed malesuada quis, lobortis ut ante. Aenean vel egestas justo. Maecenas sed massa a velit malesuada interdum. Etiam commodo porta lorem, at elementum nisl eleifend ut.', 2, NULL, 2),
(96, 'katea911807@gmail.com', 'fQRgP0iTD85kE1HnYlpB39aCjVxcsozd.jpg', 'Quisque quis leo auctor diam fringilla dapibus. Suspendisse et urna fringilla, rhoncus justo eu, lacinia diam. Nunc maximus risus nulla, lacinia consequat leo iaculis et. Pellentesque nisi est, scelerisque non tempus accumsan, lacinia a nunc. Donec nec massa neque. Aliquam blandit tristique tellus, quis efficitur dolor dictum vitae. Ut eget tempus leo. Morbi varius semper erat, eget dictum nibh semper at. Suspendisse sollicitudin in nibh eu faucibus. Sed hendrerit nibh at elit vestibulum, quis finibus elit blandit. Quisque nibh quam, rutrum sit amet tortor in, convallis ornare nisl. Nunc accumsan aliquam est, vitae posuere nibh pellentesque aliquam. Aliquam scelerisque vitae odio non semper. Mauris convallis ipsum ac odio tempor ultricies. Quisque at vulputate ipsum.', 2, NULL, 1),
(99, 'katea911807@gmail.com', 'FVkwpgAZeQnaMjSWD6iltLJTOGv14q7h.jpg', 'Phasellus efficitur quam quis aliquam malesuada. Duis nec enim fringilla nisi fermentum blandit id a ligula. Integer luctus, mi ut pharetra viverra, libero sapien venenatis lectus, in gravida quam eros eu ex. Praesent luctus interdum mattis. Ut placerat suscipit enim, eu gravida libero fringilla id. Nullam neque ipsum, bibendum quis felis nec, porta mattis magna. Aliquam vehicula orci ac elit posuere, quis efficitur eros egestas. Mauris vulputate, ipsum eu auctor molestie, massa quam blandit mi, eu vestibulum est orci viverra dui. Morbi quis tempor velit. Integer ac elementum purus. Morbi commodo est id nunc euismod, sit amet molestie nisl tristique. Duis malesuada erat sed efficitur pharetra.', 2, NULL, 1),
(106, 'katea911807@gmail.com', '0y7CIzsep61dA9NwLcgqaDfHSKOoGBJr.jpg', 'Nullam eleifend, nunc eu egestas suscipit, elit urna fringilla ligula, vel pharetra purus arcu sit amet odio. Integer pellentesque ultrices ultricies. Vestibulum maximus venenatis mi non auctor. Aenean molestie pretium quam, eget molestie ex molestie ut. Maecenas in vulputate ligula, nec tincidunt velit. Nullam sodales pharetra accumsan. Vivamus vitae vulputate nisi. Aenean ornare elementum magna, ac tempor orci sollicitudin vitae. Vivamus sodales sem quis ipsum congue fermentum. Nam tincidunt turpis eu odio dapibus tristique. Proin in blandit nulla, nec sollicitudin ipsum. Curabitur sapien mi, malesuada id eros non, varius elementum augue.', 1, NULL, NULL),
(107, 'katea911807@gmail.com', 'kr0jiv8xOwH9Gm71b4DgAnYIfu6dTapl.png', 'Praesent eget est eget ipsum scelerisque tincidunt. Morbi vehicula lorem nec est ullamcorper, id luctus nunc tristique. Fusce eget pellentesque mi. Praesent at diam placerat, aliquam ligula non, laoreet sem. Praesent eget luctus est. Quisque magna erat, tincidunt eu nulla et, pellentesque egestas tortor. Aenean porttitor nibh in metus accumsan pretium. Aliquam posuere arcu at lectus varius, id venenatis metus dignissim. Sed eget arcu ac est imperdiet mollis eu sed tortor. Nunc tincidunt sagittis tellus ut venenatis. Morbi elementum nisl dui, ut ultricies ipsum placerat at.', 1, 1, NULL),
(108, 'kate_a91@yahoo.com', 'NKAg1E830Y7fmcj2wPRzubZeDUJQSLIa.jpg', 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Maecenas quis facilisis urna. Vivamus in pellentesque sapien. Quisque nec facilisis leo. Donec non vehicula justo. Integer molestie risus vel nisl luctus gravida. Nullam feugiat congue cursus. Sed sed purus finibus, iaculis mauris sollicitudin, mattis odio. Cras nec dolor diam. Vivamus maximus est vel libero accumsan rhoncus. Duis semper, turpis venenatis sollicitudin sagittis, erat urna tincidunt lacus, eget consectetur purus mauris sit amet elit. Sed convallis ligula et eros luctus posuere. Maecenas erat ante, congue vitae purus et, facilisis posuere mi. Morbi faucibus non magna vitae mollis.', 1, NULL, NULL),
(109, 'katea911807@gmail.com', 'hbMpswo7Km0i3HUSjtzVIyZNFPDBf9e1.jpg', 'Praesent eget est eget ipsum scelerisque tincidunt. Morbi vehicula lorem nec est ullamcorper, id luctus nunc tristique. Fusce eget pellentesque mi. Praesent at diam placerat, aliquam ligula non, laoreet sem. Praesent eget luctus est. Quisque magna erat, tincidunt eu nulla et, pellentesque egestas tortor. Aenean porttitor nibh in metus accumsan pretium. Aliquam posuere arcu at lectus varius, id venenatis metus dignissim. Sed eget arcu ac est imperdiet mollis eu sed tortor. Nunc tincidunt sagittis tellus ut venenatis. Morbi elementum nisl dui, ut ultricies ipsum placerat at.', 1, NULL, NULL),
(110, 'katea911807@gmail.com', 'kDNlMeA2VbJ9PoHgirXEzyLYpmZ6wQjs.jpg', 'Nulla ut enim quis mi interdum blandit ut sit amet arcu. Ut molestie turpis at egestas eleifend. Nunc vel leo arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nullam tempus quam vitae blandit venenatis. Morbi vitae magna quam. Phasellus quis semper odio. Nam nec nibh faucibus, maximus tellus at, aliquam tortor. Sed vulputate in ipsum ornare tempus. Proin metus libero, vehicula a justo et, tempus maximus justo. Nam in nisi dignissim, convallis arcu id, volutpat diam. Cras non dolor sed purus commodo pulvinar at ut massa. Nunc placerat dolor eget turpis aliquam fringilla. Duis sed commodo eros, nec hendrerit libero.', 1, NULL, NULL),
(111, 'katea911807@gmail.com', 'pmbID92vesFGfV3lS50KdcRB1Y6kyWuw.jpg', 'Aliquam euismod velit nibh, sed finibus mi porttitor quis. Ut rutrum, augue ut dignissim egestas, velit elit lacinia turpis, sed sagittis augue turpis nec erat. Phasellus sit amet accumsan dui. In hac habitasse platea dictumst. In sed risus cursus, mattis metus a, pulvinar elit. In tortor diam, venenatis sit amet lorem a, consequat hendrerit nibh. Donec vitae consectetur nibh, vitae tristique ex. Fusce ornare tempor erat, a iaculis lacus ornare a. Ut rhoncus vehicula sodales. Maecenas suscipit tortor eleifend ornare fermentum. Nunc rhoncus, libero nec convallis fringilla, orci nulla convallis ligula, nec mollis est ipsum non augue. Mauris a nibh quis tellus vehicula elementum. Morbi dui tellus, sodales nec orci eget, dignissim iaculis dolor. Donec quis sagittis nulla. Sed ac magna nulla. Donec varius arcu vitae mauris mollis dictum.', 1, NULL, NULL),
(112, 'katea911807@gmail.com', 'VpSfvjki40J5uRxTAOEd2QbBaLNz7ew6.jpg', 'Pellentesque laoreet sodales magna ac faucibus. Nam semper lorem non elit auctor rutrum. Sed eget imperdiet enim. Phasellus gravida sodales massa, eget malesuada nunc condimentum non. Nunc rutrum ligula non urna vulputate, vitae tincidunt justo porttitor. Praesent volutpat facilisis efficitur. Nam ut imperdiet velit, vel tempus dui.', 1, NULL, NULL),
(113, 'katea911807@gmail.com', 'gcO5oaEFy2fQGPAhxn3uvtDMwBJY0slI.jpg', 'Praesent eget est eget ipsum scelerisque tincidunt. Morbi vehicula lorem nec est ullamcorper, id luctus nunc tristique. Fusce eget pellentesque mi. Praesent at diam placerat, aliquam ligula non, laoreet sem. Praesent eget luctus est. Quisque magna erat, tincidunt eu nulla et, pellentesque egestas tortor. Aenean porttitor nibh in metus accumsan pretium. Aliquam posuere arcu at lectus varius, id venenatis metus dignissim. Sed eget arcu ac est imperdiet mollis eu sed tortor. Nunc tincidunt sagittis tellus ut venenatis. Morbi elementum nisl dui, ut ultricies ipsum placerat at.', 1, NULL, NULL),
(114, 'katea911807@gmail.com', 'SHgsmaGyY1xzvLqpEcRKojNfBPX9iWn7.jpg', 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Maecenas quis facilisis urna. Vivamus in pellentesque sapien. Quisque nec facilisis leo. Donec non vehicula justo. Integer molestie risus vel nisl luctus gravida. Nullam feugiat congue cursus. Sed sed purus finibus, iaculis mauris sollicitudin, mattis odio. Cras nec dolor diam. Vivamus maximus est vel libero accumsan rhoncus. Duis semper, turpis venenatis sollicitudin sagittis, erat urna tincidunt lacus, eget consectetur purus mauris sit amet elit. Sed convallis ligula et eros luctus posuere. Maecenas erat ante, congue vitae purus et, facilisis posuere mi. Morbi faucibus non magna vitae mollis.', 1, NULL, NULL),
(115, 'katea911807@gmail.com', '9ZATd1k5DOzyLoYefqSmnNhiQ6FCpVHt.jpg', 'Phasellus efficitur quam quis aliquam malesuada. Duis nec enim fringilla nisi fermentum blandit id a ligula. Integer luctus, mi ut pharetra viverra, libero sapien venenatis lectus, in gravida quam eros eu ex. Praesent luctus interdum mattis. Ut placerat suscipit enim, eu gravida libero fringilla id. Nullam neque ipsum, bibendum quis felis nec, porta mattis magna. Aliquam vehicula orci ac elit posuere, quis efficitur eros egestas. Mauris vulputate, ipsum eu auctor molestie, massa quam blandit mi, eu vestibulum est orci viverra dui. Morbi quis tempor velit. Integer ac elementum purus. Morbi commodo est id nunc euismod, sit amet molestie nisl tristique. Duis malesuada erat sed efficitur pharetra.', 1, NULL, NULL),
(116, 'katea911807@gmail.com', 'mIJdYqtKsAziVEhfZ7PS8UrT2Xa3xCu6.jpg', 'Quisque quis leo auctor diam fringilla dapibus. Suspendisse et urna fringilla, rhoncus justo eu, lacinia diam. Nunc maximus risus nulla, lacinia consequat leo iaculis et. Pellentesque nisi est, scelerisque non tempus accumsan, lacinia a nunc. Donec nec massa neque. Aliquam blandit tristique tellus, quis efficitur dolor dictum vitae. Ut eget tempus leo. Morbi varius semper erat, eget dictum nibh semper at. Suspendisse sollicitudin in nibh eu faucibus. Sed hendrerit nibh at elit vestibulum, quis finibus elit blandit. Quisque nibh quam, rutrum sit amet tortor in, convallis ornare nisl. Nunc accumsan aliquam est, vitae posuere nibh pellentesque aliquam. Aliquam scelerisque vitae odio non semper. Mauris convallis ipsum ac odio tempor ultricies. Quisque at vulputate ipsum.', 1, NULL, NULL),
(117, 'katea911807@gmail.com', 'AEF4B0nkHz1etLGvNDoOS7YyigaqljRM.jpg', 'Integer consectetur mauris dolor, at condimentum turpis sagittis sit amet. Aenean auctor ligula purus, mattis ultricies lorem eleifend ut. Donec sed convallis sapien, sed congue quam. Sed porta arcu mi, et viverra nibh malesuada quis. Mauris cursus congue eleifend. Nunc pharetra tempor iaculis. Curabitur convallis convallis leo ut ultricies. Sed porttitor nunc dui, sagittis pharetra turpis blandit vel. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pulvinar urna ac lectus feugiat, non vehicula urna tristique. Nam id ipsum diam.', 1, NULL, NULL),
(118, 'katea911807@gmail.com', 'tjN0O5LAVRSQBMDiJUaovgem7GkXxrFq.jpg', 'Aliquam euismod velit nibh, sed finibus mi porttitor quis. Ut rutrum, augue ut dignissim egestas, velit elit lacinia turpis, sed sagittis augue turpis nec erat. Phasellus sit amet accumsan dui. In hac habitasse platea dictumst. In sed risus cursus, mattis metus a, pulvinar elit. In tortor diam, venenatis sit amet lorem a, consequat hendrerit nibh. Donec vitae consectetur nibh, vitae tristique ex. Fusce ornare tempor erat, a iaculis lacus ornare a. Ut rhoncus vehicula sodales. Maecenas suscipit tortor eleifend ornare fermentum. Nunc rhoncus, libero nec convallis fringilla, orci nulla convallis ligula, nec mollis est ipsum non augue. Mauris a nibh quis tellus vehicula elementum. Morbi dui tellus, sodales nec orci eget, dignissim iaculis dolor. Donec quis sagittis nulla. Sed ac magna nulla. Donec varius arcu vitae mauris mollis dictum.', 1, NULL, NULL),
(119, 'katea911807@gmail.com', 'gBxu3IApG57QXio6ONSb9CYlyPm8tw2Z.jpg', 'In hac habitasse platea dictumst. Quisque vitae tempor mi. Cras sed consequat enim, id varius dui. Mauris et tristique lorem. Vestibulum in dapibus nulla. Praesent dapibus eget lectus a euismod. Praesent gravida mauris ac justo ultrices blandit.', 1, NULL, NULL),
(120, 'katea911807@gmail.com', 'kVxJyT51Hcsq0YvlmDFS7eaOBiWrQhM6.jpg', 'Nullam eleifend, nunc eu egestas suscipit, elit urna fringilla ligula, vel pharetra purus arcu sit amet odio. Integer pellentesque ultrices ultricies. Vestibulum maximus venenatis mi non auctor. Aenean molestie pretium quam, eget molestie ex molestie ut. Maecenas in vulputate ligula, nec tincidunt velit. Nullam sodales pharetra accumsan. Vivamus vitae vulputate nisi. Aenean ornare elementum magna, ac tempor orci sollicitudin vitae. Vivamus sodales sem quis ipsum congue fermentum. Nam tincidunt turpis eu odio dapibus tristique. Proin in blandit nulla, nec sollicitudin ipsum. Curabitur sapien mi, malesuada id eros non, varius elementum augue.', 1, NULL, NULL),
(121, 'katea911807@gmail.com', 'PErtdBAWIMmp9x2iO5CaqYHhvcoJT8bU.jpg', 'Integer consectetur mauris dolor, at condimentum turpis sagittis sit amet. Aenean auctor ligula purus, mattis ultricies lorem eleifend ut. Donec sed convallis sapien, sed congue quam. Sed porta arcu mi, et viverra nibh malesuada quis. Mauris cursus congue eleifend. Nunc pharetra tempor iaculis. Curabitur convallis convallis leo ut ultricies. Sed porttitor nunc dui, sagittis pharetra turpis blandit vel. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pulvinar urna ac lectus feugiat, non vehicula urna tristique. Nam id ipsum diam.', 1, NULL, NULL),
(122, 'katea911807@gmail.com', 'TqDUGoXO3SulYhEsM625ndW1CQZFbw9e.jpg', 'Phasellus efficitur quam quis aliquam malesuada. Duis nec enim fringilla nisi fermentum blandit id a ligula. Integer luctus, mi ut pharetra viverra, libero sapien venenatis lectus, in gravida quam eros eu ex. Praesent luctus interdum mattis. Ut placerat suscipit enim, eu gravida libero fringilla id. Nullam neque ipsum, bibendum quis felis nec, porta mattis magna. Aliquam vehicula orci ac elit posuere, quis efficitur eros egestas. Mauris vulputate, ipsum eu auctor molestie, massa quam blandit mi, eu vestibulum est orci viverra dui. Morbi quis tempor velit. Integer ac elementum purus. Morbi commodo est id nunc euismod, sit amet molestie nisl tristique. Duis malesuada erat sed efficitur pharetra.', 1, NULL, NULL),
(123, 'katea911807@gmail.com', 'QVFprljtxX5K814Z29BLfAe3WYusDyhi.jpg', 'Praesent convallis condimentum metus, in auctor sem malesuada quis. Nam vulputate arcu dolor, quis fermentum metus pellentesque vel. Etiam tincidunt enim lacus, ut scelerisque est venenatis at. Donec tortor diam, dictum eget nunc ut, pellentesque sodales lectus. Nullam pellentesque tellus purus. Mauris pulvinar nulla a facilisis congue. Aliquam fermentum leo a mauris semper, vel interdum ligula accumsan. Maecenas eget tincidunt metus.', 1, NULL, NULL),
(124, 'katea911807@gmail.com', 'XCegJzd2rHwFcs15KP8xYNTkjbRa4lpZ.jpg', 'Aliquam euismod velit nibh, sed finibus mi porttitor quis. Ut rutrum, augue ut dignissim egestas, velit elit lacinia turpis, sed sagittis augue turpis nec erat. Phasellus sit amet accumsan dui. In hac habitasse platea dictumst. In sed risus cursus, mattis metus a, pulvinar elit. In tortor diam, venenatis sit amet lorem a, consequat hendrerit nibh. Donec vitae consectetur nibh, vitae tristique ex. Fusce ornare tempor erat, a iaculis lacus ornare a. Ut rhoncus vehicula sodales. Maecenas suscipit tortor eleifend ornare fermentum. Nunc rhoncus, libero nec convallis fringilla, orci nulla convallis ligula, nec mollis est ipsum non augue. Mauris a nibh quis tellus vehicula elementum. Morbi dui tellus, sodales nec orci eget, dignissim iaculis dolor. Donec quis sagittis nulla. Sed ac magna nulla. Donec varius arcu vitae mauris mollis dictum.', 1, NULL, NULL),
(125, 'katea911807@gmail.com', 'Ha4RpOblzoI8gFfkiC2yPXnJd5QMLVSr.jpg', 'Quisque quis leo auctor diam fringilla dapibus. Suspendisse et urna fringilla, rhoncus justo eu, lacinia diam. Nunc maximus risus nulla, lacinia consequat leo iaculis et. Pellentesque nisi est, scelerisque non tempus accumsan, lacinia a nunc. Donec nec massa neque. Aliquam blandit tristique tellus, quis efficitur dolor dictum vitae. Ut eget tempus leo. Morbi varius semper erat, eget dictum nibh semper at. Suspendisse sollicitudin in nibh eu faucibus. Sed hendrerit nibh at elit vestibulum, quis finibus elit blandit. Quisque nibh quam, rutrum sit amet tortor in, convallis ornare nisl. Nunc accumsan aliquam est, vitae posuere nibh pellentesque aliquam. Aliquam scelerisque vitae odio non semper. Mauris convallis ipsum ac odio tempor ultricies. Quisque at vulputate ipsum.', 1, NULL, NULL),
(126, 'katea911807@gmail.com', 'sTYAtMmcv1K3Zu0Qp8hCPIO4BqWXbewr.jpg', 'Pellentesque laoreet sodales magna ac faucibus. Nam semper lorem non elit auctor rutrum. Sed eget imperdiet enim. Phasellus gravida sodales massa, eget malesuada nunc condimentum non. Nunc rutrum ligula non urna vulputate, vitae tincidunt justo porttitor. Praesent volutpat facilisis efficitur. Nam ut imperdiet velit, vel tempus dui.', 1, NULL, NULL),
(127, 'katea911807@gmail.com', 'agjWt6DUpJdwBOl0eTX4R97L3My1VzuS.jpg', 'Praesent convallis condimentum metus, in auctor sem malesuada quis. Nam vulputate arcu dolor, quis fermentum metus pellentesque vel. Etiam tincidunt enim lacus, ut scelerisque est venenatis at. Donec tortor diam, dictum eget nunc ut, pellentesque sodales lectus. Nullam pellentesque tellus purus. Mauris pulvinar nulla a facilisis congue. Aliquam fermentum leo a mauris semper, vel interdum ligula accumsan. Maecenas eget tincidunt metus.', 1, NULL, NULL),
(128, 'katea911807@gmail.com', 'STa2CdgevHMk7GPWAwjs63RBNyDfuLc5.jpg', 'Nullam eleifend, nunc eu egestas suscipit, elit urna fringilla ligula, vel pharetra purus arcu sit amet odio. Integer pellentesque ultrices ultricies. Vestibulum maximus venenatis mi non auctor. Aenean molestie pretium quam, eget molestie ex molestie ut. Maecenas in vulputate ligula, nec tincidunt velit. Nullam sodales pharetra accumsan. Vivamus vitae vulputate nisi. Aenean ornare elementum magna, ac tempor orci sollicitudin vitae. Vivamus sodales sem quis ipsum congue fermentum. Nam tincidunt turpis eu odio dapibus tristique. Proin in blandit nulla, nec sollicitudin ipsum. Curabitur sapien mi, malesuada id eros non, varius elementum augue.', 1, NULL, NULL),
(129, 'katea911807@gmail.com', '0smGKnDa5zFTL2kI4E9cSYq8f3RxCWrQ.jpg', 'Praesent convallis condimentum metus, in auctor sem malesuada quis. Nam vulputate arcu dolor, quis fermentum metus pellentesque vel. Etiam tincidunt enim lacus, ut scelerisque est venenatis at. Donec tortor diam, dictum eget nunc ut, pellentesque sodales lectus. Nullam pellentesque tellus purus. Mauris pulvinar nulla a facilisis congue. Aliquam fermentum leo a mauris semper, vel interdum ligula accumsan. Maecenas eget tincidunt metus.', 1, NULL, NULL),
(130, 'katea911807@gmail.com', 'yYKtRvGXLfnS98alhFuo56dVsW3CbrPm.jpg', 'Aliquam euismod velit nibh, sed finibus mi porttitor quis. Ut rutrum, augue ut dignissim egestas, velit elit lacinia turpis, sed sagittis augue turpis nec erat. Phasellus sit amet accumsan dui. In hac habitasse platea dictumst. In sed risus cursus, mattis metus a, pulvinar elit. In tortor diam, venenatis sit amet lorem a, consequat hendrerit nibh. Donec vitae consectetur nibh, vitae tristique ex. Fusce ornare tempor erat, a iaculis lacus ornare a. Ut rhoncus vehicula sodales. Maecenas suscipit tortor eleifend ornare fermentum. Nunc rhoncus, libero nec convallis fringilla, orci nulla convallis ligula, nec mollis est ipsum non augue. Mauris a nibh quis tellus vehicula elementum. Morbi dui tellus, sodales nec orci eget, dignissim iaculis dolor. Donec quis sagittis nulla. Sed ac magna nulla. Donec varius arcu vitae mauris mollis dictum.', 1, NULL, NULL),
(131, 'katea911807@gmail.com', 'd6wyVAOk4zg2u3m8NqULJQ7Z1EhfnPcr.jpg', 'Praesent convallis condimentum metus, in auctor sem malesuada quis. Nam vulputate arcu dolor, quis fermentum metus pellentesque vel. Etiam tincidunt enim lacus, ut scelerisque est venenatis at. Donec tortor diam, dictum eget nunc ut, pellentesque sodales lectus. Nullam pellentesque tellus purus. Mauris pulvinar nulla a facilisis congue. Aliquam fermentum leo a mauris semper, vel interdum ligula accumsan. Maecenas eget tincidunt metus.', 1, NULL, NULL),
(132, 'katea911807@gmail.com', 'Qx1ToCGl0FUvjpaRbyLIB4EOtdcruh92.jpg', 'Nulla augue diam, mollis in metus sed, bibendum faucibus ex. Donec facilisis purus a iaculis elementum. Sed quis nibh porta, bibendum sem eu, placerat sapien. Duis imperdiet rhoncus ligula, non commodo nisl pretium non. Nulla vel leo sit amet diam commodo suscipit a in ligula. In hac habitasse platea dictumst. Suspendisse tempor pharetra justo at vestibulum. Donec non pharetra felis. Fusce laoreet vitae odio et eleifend. ', 1, NULL, NULL),
(133, 'katea911807@gmail.com', 'uvZ3NsPqak6Kh8DtnpJ7TFBEVe5xRXMz.jpg', 'Nullam eleifend, nunc eu egestas suscipit, elit urna fringilla ligula, vel pharetra purus arcu sit amet odio. Integer pellentesque ultrices ultricies. Vestibulum maximus venenatis mi non auctor. Aenean molestie pretium quam, eget molestie ex molestie ut. Maecenas in vulputate ligula, nec tincidunt velit. Nullam sodales pharetra accumsan. Vivamus vitae vulputate nisi. Aenean ornare elementum magna, ac tempor orci sollicitudin vitae. Vivamus sodales sem quis ipsum congue fermentum. Nam tincidunt turpis eu odio dapibus tristique. Proin in blandit nulla, nec sollicitudin ipsum. Curabitur sapien mi, malesuada id eros non, varius elementum augue.', 1, NULL, NULL),
(134, 'katea911807@gmail.com', 'OTstNQ8ECASXKhG1465JFDlxdLMVnzPm.jpg', 'Praesent eget est eget ipsum scelerisque tincidunt. Morbi vehicula lorem nec est ullamcorper, id luctus nunc tristique. Fusce eget pellentesque mi. Praesent at diam placerat, aliquam ligula non, laoreet sem. Praesent eget luctus est. Quisque magna erat, tincidunt eu nulla et, pellentesque egestas tortor. Aenean porttitor nibh in metus accumsan pretium. Aliquam posuere arcu at lectus varius, id venenatis metus dignissim. Sed eget arcu ac est imperdiet mollis eu sed tortor. Nunc tincidunt sagittis tellus ut venenatis. Morbi elementum nisl dui, ut ultricies ipsum placerat at.', 1, NULL, NULL),
(135, 'katea911807@gmail.com', 'BfC7aerlIQ3Zt1j0iv9R5WM4bwVDnOXh.jpg', 'Phasellus efficitur quam quis aliquam malesuada. Duis nec enim fringilla nisi fermentum blandit id a ligula. Integer luctus, mi ut pharetra viverra, libero sapien venenatis lectus, in gravida quam eros eu ex. Praesent luctus interdum mattis. Ut placerat suscipit enim, eu gravida libero fringilla id. Nullam neque ipsum, bibendum quis felis nec, porta mattis magna. Aliquam vehicula orci ac elit posuere, quis efficitur eros egestas. Mauris vulputate, ipsum eu auctor molestie, massa quam blandit mi, eu vestibulum est orci viverra dui. Morbi quis tempor velit. Integer ac elementum purus. Morbi commodo est id nunc euismod, sit amet molestie nisl tristique. Duis malesuada erat sed efficitur pharetra.', 1, NULL, NULL),
(136, 'katea911807@gmail.com', 'CT4c7DnXjWoOVlRB39bwiMFAGQeY0HzZ.jpg', 'Pellentesque laoreet sodales magna ac faucibus. Nam semper lorem non elit auctor rutrum. Sed eget imperdiet enim. Phasellus gravida sodales massa, eget malesuada nunc condimentum non. Nunc rutrum ligula non urna vulputate, vitae tincidunt justo porttitor. Praesent volutpat facilisis efficitur. Nam ut imperdiet velit, vel tempus dui.', 1, NULL, NULL),
(137, 'katea911807@gmail.com', 'ch6qaldb9OPVMDosI0itSBA4LJTgwZHU.jpg', 'Integer consectetur mauris dolor, at condimentum turpis sagittis sit amet. Aenean auctor ligula purus, mattis ultricies lorem eleifend ut. Donec sed convallis sapien, sed congue quam. Sed porta arcu mi, et viverra nibh malesuada quis. Mauris cursus congue eleifend. Nunc pharetra tempor iaculis. Curabitur convallis convallis leo ut ultricies. Sed porttitor nunc dui, sagittis pharetra turpis blandit vel. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pulvinar urna ac lectus feugiat, non vehicula urna tristique. Nam id ipsum diam.', 1, NULL, NULL),
(138, 'katea911807@gmail.com', 'qEcdurDQtw9v03hPOnZ6XlBkT1sj7ULI.jpg', 'Nulla ut enim quis mi interdum blandit ut sit amet arcu. Ut molestie turpis at egestas eleifend. Nunc vel leo arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nullam tempus quam vitae blandit venenatis. Morbi vitae magna quam. Phasellus quis semper odio. Nam nec nibh faucibus, maximus tellus at, aliquam tortor. Sed vulputate in ipsum ornare tempus. Proin metus libero, vehicula a justo et, tempus maximus justo. Nam in nisi dignissim, convallis arcu id, volutpat diam. Cras non dolor sed purus commodo pulvinar at ut massa. Nunc placerat dolor eget turpis aliquam fringilla. Duis sed commodo eros, nec hendrerit libero.', 1, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `award`
--
ALTER TABLE `award`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `award_id` (`award_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `award`
--
ALTER TABLE `award`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`award_id`) REFERENCES `award` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
