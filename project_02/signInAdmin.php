<?php
session_start();
include 'db.php';
include 'constants.php';
include 'functions.php';


$username=$_POST['username'];
$password=$_POST['password'];
$email = $_POST['email'];
$uploadImage=$_POST['uploadImage'];

checkRequestSignIn();
$msg='';

checkIssetAdminInput($username, $password);

$sqlAdmin = "SELECT * FROM `admin` WHERE admin.id =:id LIMIT 1";
$stmtAdmin = $pdo->prepare($sqlAdmin);
$stmtAdmin->execute(['id' => 1]);
if($stmtAdmin->rowCount() == 0) {
     header("Location: index.php");
die();
} 
$admin = $stmtAdmin->fetch();
$adminUsername = $admin['user'];
$adminPass = $admin['pass'];
$isPasswordCorrect = password_verify($password, $adminPass);
if($username != $adminUsername || $isPasswordCorrect == 0) {
    $_SESSION['error'] = "wrong username and password combination";
    header('Location: index.php?error');
} else {
    header('Location: main.php');
}

?>