<?php
require_once 'Product.php';
require_once 'MarketStall.php';
require_once 'Cart.php';
require_once 'Orange.php';
require_once 'Kiwi.php';
require_once 'Potato.php';
require_once 'Nuts.php';
require_once 'Pepper.php';
require_once 'Raspberry.php';


$orange = new Orange('Orange', 35, false);
$potato = new Potato('Potato', 240, false);
$nuts = new Nuts('Nuts', 850, true);
$kiwi = new Kiwi('Kiwi', 670, false);
$pepper = new Pepper('Pepper', 330, true);
$raspberry = new Raspberry('Raspberry', 555, false);


$arr1 = [$orange, $potato, $nuts];
$arr2 = [$kiwi, $pepper, $raspberry];
$MarketStall1 = new MarketStall($arr1);
$MarketStall2 = new MarketStall($arr2);

$cart = New Cart();

$cart->addToCart($MarketStall1->getItem('potato',4));
$cart->addToCart($MarketStall1->getItem('orange',2));
$cart->addToCart($MarketStall1->getItem('nuts',5));
$cart->addToCart($MarketStall2->getItem('kiwi',4));
$cart->addToCart($MarketStall2->getItem('pepper',4));
$cart->addToCart($MarketStall2->getItem('raspberry',4));
$cart->returnCart();
