<?php

class MarketStall {
    public $storingProducts;
    
    public function __construct(array $storingProducts) {
        $this->storingProducts=($this->addProductToMarket($storingProducts));
    }
    public function addProductToMarket($storingProducts) {
            $products = [];
            foreach($storingProducts as $value => $product) {
                if(!isset($products[$product->getName()])) {
                    $products[$product->getName()] = [];
                }
                array_push($products[$product->getName()], $product);
            }
            return $products;
    }
    public function getProductToMarket() {
        return  $this->storingProducts;
    }
    public function getItem(string $item, int $amount) {
               $item=ucfirst($item);
                if(array_key_exists($item,$this->storingProducts)) {
                      
                    return ['amount'=>$amount, $item=>$this->storingProducts[$item]];
                } else {
                    return false;
                }
        }
}