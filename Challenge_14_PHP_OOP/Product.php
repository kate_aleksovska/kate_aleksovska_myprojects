<?php 
class Product {
    public $name;
    protected $price;
    protected $sellingByKg;

    public function __construct(string $name, float $price, bool $sellingByKg) {
       $this->name=$name;
       $this->price=$price;
       $this->sellingByKg=$sellingByKg;     
    }

    public function getsellingByKg() {
       return ($this->sellingByKg == "true" ? "Kg":"gunny sack");
    }
    public function getPrice() {
        return $this->price;
    }
    public function getName() {
        return $this->name;
    }
}