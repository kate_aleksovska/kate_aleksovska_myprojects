<?php

use App\Models\Category;
use App\Models\Discussion;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AddController;
use App\Http\Controllers\ApproveController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\DiscussionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    $categories = Category::all();
    $discussions = Discussion::all();
    return view('welcome', compact('categories', 'discussions'));
})->name('welcome');
    Route::get('add',              [AddController::class, 'index'])->name('add.index');
    Route::get('add/create',       [AddController::class, 'create'])->name('add.create');
    Route::get('add/{id}',         [AddController::class, 'show'])->name('add.show');


Route::middleware(['auth'])->group(function(){


Route::get('/dashboard', function () {
    $categories = Category::all();
    $discussions = Discussion::all();
    return view('dashboard', compact('categories', 'discussions'));
})->name('dashboard');

    

    Route::get('disc',              [DiscussionController::class, 'index'])->name('disc.index');
    Route::get('disc/create',       [DiscussionController::class, 'create'])->name('disc.create');
    Route::post('disc',             [DiscussionController::class, 'store'])->name('disc.store');
    Route::get('disc/{id}',         [DiscussionController::class, 'show'])->name('disc.show');
    Route::get('disc/{id}/edit',    [DiscussionController::class, 'edit'])->name('disc.edit');
    Route::put('disc/{id}',         [DiscussionController::class, 'update'])->name('disc.update');
    Route::delete('disc/{id}',      [DiscussionController::class, 'destroy'])->name('disc.destroy');
    
    Route::put('approve/{id}',      [ApproveController::class, 'update'])->name('approve.update');

    Route::get('comment',              [CommentController::class, 'index'])->name('comment.index');
    Route::get('comment/create',       [CommentController::class, 'create'])->name('comment.create');
    Route::post('comment',             [CommentController::class, 'store'])->name('comment.store');
    Route::get('comment/{id}',         [CommentController::class, 'show'])->name('comment.show');
    Route::get('comment/{id}/edit',    [CommentController::class, 'edit'])->name('comment.edit');
    Route::put('comment/{id}',         [CommentController::class, 'update'])->name('comment.update');
    Route::delete('comment/{id}',      [CommentController::class, 'destroy'])->name('comment.destroy');

});
require __DIR__.'/auth.php';
