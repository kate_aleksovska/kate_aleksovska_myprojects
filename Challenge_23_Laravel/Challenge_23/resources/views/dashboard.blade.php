<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-center text-gray-800 leading-tight">
            {{ __('Welcome to the Forum') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                @if (Session::has('success'))
                                    <div class="alert alert-success text-center">
                                        {{ Session::get('success') }}
                                    </div>
                                @endif
                                @if (Session::has('error'))
                                    <div class="alert alert-danger text-center">
                                        {{ Session::get('error') }}
                                    </div>
                                @endif
                            </div>
                            <div class="col-12">
                                @if (Auth::user()->role->name == 'User')

                                    <button class="btn btn-secondary m-3" id="discussion">Add new discussion</button>

                                @endif
                                @if (Auth::user()->role->name == 'Admin')

                                    <button class="btn btn-secondary m-3" id="discussion">Add new discussion</button>

                                    <button class="btn m-3 btn-primary" id="approve">Approve discussion</button>

                                @endif
                            </div>

                            <div class="col-6 offset-3" id="form" style="display: none">
                                <form method="POST" action="{{ route('disc.store') }}" novalidate
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="title">Title</label>
                                        <input type="text" class="form-control @error('title') is-invalid @enderror"
                                            id="title" value="{{ old('title') }}" name="title">
                                        @error('title')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="formFile">Upload Photo</label>

                                        <input class="form-control  @error('photo') is-invalid @enderror" name="photo"
                                            type="file" id="formFile" value="{{ old('photo') }}">

                                        @error('photo')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="desc">Description</label>
                                        <textarea class="form-control @error('desc') is-invalid @enderror" id="desc"
                                            rows="3" name="desc"></textarea>
                                        @error('desc')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="category">Category</label>
                                        <select class="form-control" id="category" name="category">
                                            @foreach ($categories as $category)
                                                <option value="{{ $category->id }}">{{ $category->category }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-primary d-inline text-dark"
                                        id="submitDescription">Submit</button>
                                </form>
                            </div>
                            @if ($discussions->isEmpty())
                                <div class="col-12 text-center" id="topic">
                                    <div class="h3">Nothing here yet! Start a topic!</div>
                                </div>
                            @else
                                <div class="col-12" id="cards">
                                    @foreach ($discussions as $discussion)
                                        @if ($discussion->approved == true && Auth::user()->role->name == 'User')
                                            <div class="card mb-3">
                                                <div data-target="product{{ $discussion->id }}"
                                                    data-target2="form{{ $discussion->id }}">
                                                    <a href="{{route('disc.show', $discussion->id)}}">
                                                    <img src="{{ asset("storage/$discussion->photo") }}"
                                                        class="card-img-top" alt="..."
                                                        style="height:200px; width:200px">
                                                    </a>
                                                    <div class="card-body">
                                                        <h5 class="card-title">{{ $discussion->title }}</h5>
                                                        <p class="card-text">{{ $discussion->desc }}</p>
                                                        <p class="card-text"><small
                                                                class="text-muted">{{ $discussion->user->username . '|' . $discussion->category->category }}</small>
                                                        </p>
                                                    </div>
                                                
                                                    @if (Auth::user()->id == $discussion->user_id)
                                                        <button class="btn btn-secondary d-inline"
                                                            id="product{{ $discussion->id }}">Edit</button>
                                                        <form action="{{ route('disc.destroy', $discussion->id) }}"
                                                            method="POST" class="d-inline-block">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit"
                                                                class="btn btn-danger text-dark">Delete</button>
                                                        </form>
                                                    @endif
                                                </div>
                                                <form method="POST"
                                                    action="{{ route('disc.update', $discussion->id) }}" novalidate
                                                    enctype="multipart/form-data"
                                                    data-type='product{{ $discussion->id }}'
                                                    data-type2='form{{ $discussion->id }}' style="display: none" class="m-3">
                                                    @csrf
                                                    @method('PUT')
                                                    <div class="form-group">
                                                        <label for="title">Title</label>
                                                        <input type="text"
                                                            class="form-control @error('title') is-invalid @enderror"
                                                            id="title" value="{{ old('title', $discussion->title) }}"
                                                            name="title">
                                                        @error('title')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="formFile">Upload Photo</label>

                                                        <input
                                                            class="form-control  @error('photo') is-invalid @enderror"
                                                            name="photo" type="file" id="formFile"
                                                            value="{{ old('photo', $discussion->photo) }}">

                                                        @error('photo')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="desc">Description</label>
                                                        <textarea
                                                            class="form-control @error('desc') is-invalid @enderror"
                                                            id="desc" rows="3" name="desc"
                                                            value="{{ old('desc', $discussion->desc) }}">{{ $discussion->desc }}</textarea>
                                                        @error('desc')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="category">Category</label>
                                                        <select class="form-control" id="category" name="category">
                                                            @foreach ($categories as $category)
                                                                <option
                                                                    {{ $discussion->category_id == $category->id ? 'selected' : '' }}
                                                                    value="{{ $category->id }}">
                                                                    {{ $category->category }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <button type="submit" class="btn btn-primary d-inline text-dark"
                                                        id="submitDescription">Update</button>
                                                    <div class="btn btn-warning d-inline btn-block"
                                                        id="form{{ $discussion->id }}">Decline</div>
                                                </form>

                                            </div>
                                        @endif
                                        @auth
                                            @if (Auth::user()->role->name == 'Admin')
                                                <div class="card mb-3">
                                                    <div data-target="product{{ $discussion->id }}"
                                                        data-target2="form{{ $discussion->id }}">
                                                        <a href="{{route('disc.show', $discussion->id)}}">
                                                            <img src="{{ asset("storage/$discussion->photo") }}"
                                                            class="card-img-top" alt="..."
                                                            style="height:200px; width:200px">
                                                        </a>
                                                        <div class="card-body">
                                                            <h5 class="card-title">{{ $discussion->title }}</h5>
                                                            <p class="card-text">{{ $discussion->desc }}</p>
                                                            <p class="card-text"><small
                                                                    class="text-muted">{{ $discussion->user->username . '|' . $discussion->category->category }}</small>
                                                            </p>
                                                        </div>
                                                        <form action="{{ route('approve.update', $discussion->id) }}"
                                                            method="POST" class="d-inline m-1">
                                                            @csrf
                                                            @method('PUT')
                                                            <button class="btn btn-success d-inline" @if ($discussion->approved == true)
                                                                disabled
                                            @endif>Approve</button>
                                            </form>
                                            <button class="btn btn-secondary d-inline"
                                                id="product{{ $discussion->id }}">Edit</button>
                                            <form action="{{ route('disc.destroy', $discussion->id) }}" method="POST"
                                                class="d-inline m-1">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger d-inline text-dark">Delete</button>
                                            </form>
                                    </div>
                                    <form method="POST" action="{{ route('disc.update', $discussion->id) }}" novalidate
                                        enctype="multipart/form-data" data-type='product{{ $discussion->id }}'
                                        data-type2='form{{ $discussion->id }}' style="display: none" class="m-3">
                                        @csrf
                                        @method('PUT')
                                        <div class="form-group">
                                            <label for="title">Title</label>
                                            <input type="text" class="form-control @error('title') is-invalid @enderror"
                                                id="title" value="{{ old('title', $discussion->title) }}" name="title">
                                            @error('title')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="formFile">Upload Photo</label>

                                            <input class="form-control  @error('photo') is-invalid @enderror" name="photo"
                                                type="file" id="formFile" value="{{ old('photo', $discussion->photo) }}">

                                            @error('photo')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="desc">Description</label>
                                            <textarea class="form-control @error('desc') is-invalid @enderror" id="desc"
                                                rows="3" name="desc"
                                                value="{{ old('desc', $discussion->desc) }}">{{ $discussion->desc }}</textarea>
                                            @error('desc')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="category">Category</label>
                                            <select class="form-control" id="category" name="category">
                                                @foreach ($categories as $category)
                                                    <option
                                                        {{ $discussion->category_id == $category->id ? 'selected' : '' }}
                                                        value="{{ $category->id }}">{{ $category->category }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <button type="submit" class="btn d-inline btn-primary text-dark"
                                            id="submitDescription">Update</button>
                                        <div class="btn btn-warning d-inline btn-block" id="form{{ $discussion->id }}">
                                            Decline</div>
                                    </form>

                            </div>
                            @endif
                        @endauth



                        @endforeach

                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>

</x-app-layout>
