<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-center text-gray-800 leading-tight">
            {{ __('Welcome to the Forum') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                @if (Session::has('success'))
                                    <div class="alert alert-success text-center">
                                        {{ Session::get('success') }}
                                    </div>
                                @endif
                                @if (Session::has('error'))
                                    <div class="alert alert-danger text-center">
                                        {{ Session::get('error') }}
                                    </div>
                                @endif
                            </div>
                   
                           
                </div>
                <div class="row">
                    <div class="col-12" id="cardComment">
                    <div class="card mb-3 text-center" >
                        <img src="{{ asset("storage/$discussion->photo") }}"
                            class="card-img-top m-5" alt="..."
                            style="height:500px; width:500px">
                        <div class="card-body">
                            <h5 class="card-title">{{ $discussion->title }}</h5>
                            <p class="card-text">{{ $discussion->desc }}</p>
                            <p class="card-text"><small
                                    class="text-muted">{{ $discussion->user->username . '|' . $discussion->category->category }}</small>
                            </p>
                        </div>
                     @foreach ($discussion->comments as $comment)
                     <div class="card m-3 text-center" data-target="edit{{$comment->id}}" data-target2="decline{{$comment->id}}">
                        <div class="card-body">
                            <h5 class="card-title">{{ $comment->user->username. ' says:' }}</h5>
                            <h5 class="card-title">{{ $comment->comments }}</h5>
                            <p class="card-text"><small
                                    class="text-muted">{{ $comment->created_at}}</small>
                            </p>
                        </div>
                        @auth
                         @if(Auth::user()->role->name == 'Admin' || Auth::user()->id == $comment->user_id)
                         <form action="{{route('comment.destroy', $comment->id)}}" method="POST" class="d-inline">
                             @csrf
                             @method('DELETE')
                         <button class="btn d-inline btn-danger">Delete</button> 
                        </form>
                        <button class="btn d-inline btn-secondary" id="edit{{$comment->id}}">Edit</button> 
                         @endif
                        @endauth
                    </div>
                    <form action="{{route('comment.update', $comment->id)}}" method="POST" enctype="multipart/form-data" style="display: none" data-type="edit{{$comment->id}}" class="m-3" data-type2='decline{{$comment->id}}'>
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Edit comment</label>
                            <textarea class="form-control @error('comment') is-invalid @enderror" id="exampleFormControlTextarea1" rows="3" name="comment" value="{{ old('comment', $comment->comments) }}">{{$comment->comments}}</textarea>
                            @error('comment')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                           @enderror
                        <button class="btn btn-primary">Submit</button>
                        <div class="btn btn-warning" id="decline{{$comment->id}}">Decline</div>
                        </div>
                        <input type="text" value="{{$discussion->id}}" name="discussion_id" style="display: none">
                    </form> 
                     @endforeach   
                        
                 </div>
               @auth
                   
                 <button  class="btn btn-primary" id="comment">Comment</button>
                 <a class="btn btn-secondary" href="{{route('dashboard')}}">Back</a>
                 @endauth  
                </div>
                <div class="col-12" id="commentDiscussion" style="display: none">
                    <form action="{{route('comment.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Enter comment</label>
                            <textarea class="form-control @error('comment') is-invalid @enderror" id="exampleFormControlTextarea1" rows="3" name="comment" value="{{ old('comment') }}"></textarea>
                            @error('comment')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                           @enderror
                        <button class="btn btn-primary">Submit</button>
                        </div>
                        <input type="text" value="{{$discussion->id}}" name="discussion_id" style="display: none">
                    </form>   
                </div> 
                  </div>
            </div>
        </div>
    </div>
    </div>
    </div>

</x-app-layout>
