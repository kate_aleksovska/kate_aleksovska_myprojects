<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User();
        $admin->username = 'mkaleka';
        $admin->email = 'katea911807@gmail.com';
        $admin->password = Hash::make('password');
        $admin->role_id = Role::where('name', 'Admin')->first()->id;
        $admin->save();

    }
}
