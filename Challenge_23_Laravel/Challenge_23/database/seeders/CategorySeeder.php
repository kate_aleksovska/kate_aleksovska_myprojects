<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = new Category();
        $category->category = 'general';
        $category->save();

        $category1 = new Category();
        $category1->category = 'entertainment';
        $category1->save();

        $category2 = new Category();
        $category2->category = 'sports';
        $category2->save();

        $category2 = new Category();
        $category2->category = 'movies';
        $category2->save();

        $category3 = new Category();
        $category3->category = 'politics';
        $category3->save();

        $category4 = new Category();
        $category4->category = 'cars';
        $category4->save();
    }
}
