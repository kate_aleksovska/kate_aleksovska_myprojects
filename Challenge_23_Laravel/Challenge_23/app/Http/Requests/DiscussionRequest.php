<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DiscussionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=> 'required',
            'photo' => 'sometimes|file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'desc' => 'required|max:255',
            'category' => 'required'
        ];
    }
}
