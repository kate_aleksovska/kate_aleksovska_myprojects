<?php

namespace App\Http\Controllers;

use App\Models\Discussion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\DiscussionRequest;

class DiscussionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('discussion.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DiscussionRequest $request)
    {
        $attributes = $request->validated();
        $attributes['user_id'] = auth()->user()->id;
        $attributes['category_id'] = $request->category;
        $attributes['title'] = $request->title;
        $attributes['desc'] = $request->desc;
        if ($request->hasFile('photo')) 
        {
            $attributes['photo'] = $request->file('photo')->store('files', 'public');
        }

        Discussion::create($attributes);
        if(Auth::user()->role->name == 'Admin'){
        return redirect('/dashboard')->with('success', "Successfully created discussion");
        } else {
            return redirect('/dashboard')->with('success', "Successfully created discussion!It needs to be approved before you dig into through");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $discussion = Discussion::find($id);
        return view('show', compact('discussion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DiscussionRequest $request, $id)
    {
        
        $discussion = Discussion::find($id);

        $discussion->title = $request->title;
        if ($request->hasFile('photo')) 
        {
            $discussion->photo = $request->file('photo')->store('files', 'public');
        }
        $discussion->category_id=$request->category;
        $discussion->desc=$request->desc;
        if($discussion->save())
              return redirect()->route('dashboard')->with('success', 'The discussion with '.$discussion->title.' is updated!!');
              return redirect()->route('dashboard')->with('error', 'The discussion with '.$discussion->title.' is not updated!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $discussion = Discussion::find($id);
        $name = $discussion->title;
       if($discussion->delete())
        return redirect()->route('dashboard')->with('success', 'The disscussion with title '.$name. ' is deleted');
        return redirect()->route('dashboard')->with('error', 'The disscussion with title '.$name. ' is not deleted');
    }
}
