<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>project01 client page</title>
    <link rel="stylesheet" href="./css/style.css">
        <link rel="stylesheet" href="awesome_fonts/css/all.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
     <!-- CREATING NAVBAR -->
     <nav class="navbar navbar-expand-md navbar-light bg-warning fixed-top">
        <a class="navbar-brand d-lg-flex" href="index.html">
      <div class="d-flex">
        <div class=" ml-lg-3">
          <div class="text-center">
        <span class="width-25"><img src="images/Logo.png" alt="logo-png" class="img-logo"></span>
        <p class="h6 logo-text">BRAINSTER</p>
      </div>
      </div>
    </div>
  </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon">
      </span>
      <span class="large material-icons icon-flipped text-dark">
        signal_cellular_alt
      </span>
  </button>
        <div class="collapse navbar-collapse justify-content-around" id="navbarSupportedContent">
            <ul class="navbar-nav ml-0 d-flex">
                <li class="nav-item active px-lg-1 my-2">
                    <a class="nav-link d-inline-block progress-bar-striped progress-bar-animated badge-pill mx-2 text-center" href="https://marketpreneurs.brainster.co"><big>Академија за маркетинг</big><span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active px-lg-2 my-2">
                    <a class="nav-link d-inline-block progress-bar-striped progress-bar-animated badge-pill mx-2 text-center" href="https://codepreneurs.brainster.co"><big>Академија за програмирање</big><span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active px-lg-1 my-2">
                    <a class="nav-link d-inline-block progress-bar-striped progress-bar-animated badge-pill mx-2 text-center" href="https://datascience.brainster.co"><span class="text-center"><big>Академија за data science</big></span><span class="sr-only">(current)</span></a>
                </li><li class="nav-item active px-lg-1 my-2">
                    <a class="nav-link d-inline-block progress-bar-striped progress-bar-animated badge-pill mx-2 text-center" href="https://design.brainster.co"><big>Академија за дизајн</big><span class="sr-only">(current)</span></a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <a class="btn btn-danger my-2 my-sm-0 btn-hover progress-bar-striped progress-bar-animated mx-2" href="contact.php">Вработи наш студент</a>
              </form>
        </div>
        <!-- NAVBAR FOR SMALL AND MOBILE DEVICES -->
        <div id="navBar">
          <ul class="nav-items d-inline-block">
            <li>
              <a href="#" id="closebtn">×</a>
            </li>
            <li>
                <a href="https://marketpreneurs.brainster.co" class="progress-bar-striped progress-bar-animated badge-pill my-3"><big>Академија за маркетинг</big><span class="sr-only">(current)</span></a>
            </li>
            <li>
                <a href="https://codepreneurs.brainster.co" class="progress-bar-striped progress-bar-animated badge-pill my-3"><big>Академија за програмирање</big><span class="sr-only">(current)</span></a>
            </li>
            <li>
                <a href="https://datascience.brainster.co" class="progress-bar-striped progress-bar-animated badge-pill my-3" ><span class="text-center"><big>Академија за data science</big></span><span class="sr-only">(current)</span></a>
            </li><li>
                <a href="https://design.brainster.co" class="progress-bar-striped progress-bar-animated badge-pill my-3"><big>Академија за дизајн</big><span class="sr-only">(current)</span></a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
          <a class="btn btn-danger p-2 my-sm-0 btn-hover progress-bar-striped progress-bar-animated" type="submit" href="contact.php">Вработи наш студент</a>
        </form>
        </div>
        <div id="sliderBtn">
        <span class="large material-icons icon-flipped another-icon text-dark" >
          signal_cellular_alt
        </span>
        </div>
        <span class="large material-icons icon-flipped text-dark btn1" >
          signal_cellular_alt
        </span>
    </nav>
    <!-- TABLE PART OF CLIENT PAGE -->
    <div class="container-fluid bg-warning input-banner">
    <div class="row flex-column justify-content-between">
        <div class="col">
            <div class="container">
                <div class="row">
                    <div class="col text-center my-5">
                       <div class="title">Регистрирани клиенти</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
    <table class="table table-striped">
        <tr>
        <thead class="thead-dark">
            <th scope="col">Id</th>
            <th scope="col">Име и презиме</th>
            <th scope="col">Име на компанија</th>
            <th scope="col">Емајл</th>
            <th scope="col">Телефон</th>
            <th scope="col">Тип на студент</th>
        </thead>
        </tr>
        <tbody>
        <?php   
          $conn = mysqli_connect("localhost", "root", "", "register");
          if($conn-> connect_error) {
              die("Connection failed:". $conn-> connect_error);
          }
          $sql = "SELECT id, NameLastname, NameCompany, email, phone, type_of_student from register";
          $result = $conn-> query($sql);

          if ($result-> num_rows > 0) {
              while ($row = $result-> fetch_assoc()) {
                echo "<tr><td>". $row["id"] ."</td><td>". $row["NameLastname"] ."</td><td>". $row["NameCompany"] ."</td><td>". $row["email"] ."</td><td>". $row["phone"] ."</td><td>". $row["type_of_student"] ."</td></tr>";
              }
              echo "</table>";  
          } else {
              echo "0 result";
          }

          $conn-> close();
        ?>
        </tbody>
       
    </table>
                    </div>
                </div>
            </div>
        
        </div>
    </div>
</div>
      <!-- CREATING FOOTER -->
      <footer class="container-fluid bg-dark text-center py-2 py-sm-3 text-white">
      <p class="mb-0">Изработено со <i class="fas fa-heart text-danger"></i> од студентите на Brainster</p>
</footer>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="./js/project_1.js"></script>
       <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
       integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
      </script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
       integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
      </script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js"
       integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous">
      </script>   
</body>
</html>