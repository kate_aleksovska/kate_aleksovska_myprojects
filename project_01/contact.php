<?php 

$flag = 0;

if ( isset($_POST) && !empty($_POST['NameLastname']) && !empty($_POST['NameCompany']) && !empty($_POST['email']) && !empty($_POST['phone']) || !empty($_POST['type_of_student'])) {
  $NameLastname = $_POST['NameLastname'];
  $NameCompany = $_POST['NameCompany'];
  $email = $_POST['email'];
  $phone = $_POST['phone'];
  $type_of_student = $_POST['type_of_student'];
  #code
    $host = "localhost";
    $dbUsername = "root";
    $dbPassword = "";
    $dbname = "register";

    // connection with database
    $conn = new mysqli($host, $dbUsername, $dbPassword, $dbname);
    mysqli_set_charset($conn, "utf8");
    if (mysqli_connect_error()) {
        die('Connect Error('.mysqli_connect_errno().')'.mysqli_connect_error());
    } else {
        $INSERT = "INSERT INTO register (NameLastname, NameCompany, email, phone, type_of_student) VALUES('". $_POST["NameLastname"] . "','" . $_POST["NameCompany"] . "','" . $_POST["email"] . "'," . $_POST["phone"] . ",'" . $_POST["type_of_student"]."')";
        if ($conn->query($INSERT) === TRUE) {
            $flag=1;
          } else {
            $flag=3;
          }
          
          $conn->close();
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Project01 contact page</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="./awesome_fonts/css/all.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script>
    function reloadPage(){
        location.reload(true);
    }
</script>
</head>
<!-- STYLE FOR CONTACT PAGE -->
<style>
  .formControl {
    position: relative;
  }

  .formControl label {
    display: inline-block;
    margin-bottom: 5px;
  }
  .formControl select,
  .formControl input {
    border: 2px solid #f0f0f0;
    border-radius: 4px;
    font-family: inherit;
    font-size: 14px;
    padding: 10px;
    width: 100%;
    display: inline-block;
  }
  .formControl i {
    position: absolute;
    top: 40px;
    right: 20px;
    visibility: hidden;
  }
  .formControl small {
    position: relative;
    visibility: hidden;
    font-weight: 500;
  }
  form .button-submit {
    width: 100%;
    font-size: 14.7px;
    padding: 10px;
  }
  .form {
    width: 90%;
  }
  .formControl.success input {
  border: 2px solid rgb(84, 187, 58);
  box-shadow: 2px 2px 5px rgb(84, 187, 58);
  }
  .formControl.success select {
    border: 2px solid rgb(84, 187, 58);
  box-shadow: 2px 2px 5px rgb(84, 187, 58);
  }
  .formControl.success i.fa-check {
    visibility: visible;
  }
  .formControl.error input {
    border-color: red;
  box-shadow: 2px 2px 5px red;
  }
  .formControl.error i.fa-exclamation {
    visibility: visible;
  }
  .formControl.error small {
    visibility: visible;
  }
  option {
    font-size: 1rem;
    font-weight: 500;
  }
  @media(max-width:767.6px) {
    .input-banner {
    height:100%!important;
}
.form {
    width: 100%!important;
}
.title {
    font-size: 35px;
    font-weight: 500;
}
#inputState > option {
    position: static!important;
}
  }
  @media(max-width:321px) {
    .title {
    font-size: 30px;
}
.badge-pill {
  font-size: 15px!important;
}
  }
</style>
<body>
  <!-- NAVBAR OF CONTACT PAGE -->
<nav class="navbar navbar-expand-md navbar-light bg-warning fixed-top">
  <a class="navbar-brand d-lg-flex" href="index.html">
      <div class="d-flex">
        <div class=" ml-lg-3">
          <div class="text-center">
        <span class="width-25"><img src="images/Logo.png" alt="logo-png" class="img-logo"></span>
        <p class="h6 logo-text">BRAINSTER</p>
      </div>
      </div>
    </div>
  </a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
  <span class="navbar-toggler-icon">
    
    <span class="large material-icons icon-flipped text-dark">
      signal_cellular_alt
    </span>
  </span>
</button>
    <div class="collapse navbar-collapse justify-content-around" id="navbarSupportedContent">
        <ul class="navbar-nav ml-0 d-flex">
            <li class="nav-item active px-lg-2 my-2">
                <a class="nav-link d-inline-block progress-bar-striped badge-pill progress-bar-animated mx-2 text-center" href="https://marketpreneurs.brainster.co"><big>Академија за маркетинг</big><span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active px-lg-2 my-2">
                <a class="nav-link d-inline-block progress-bar-striped badge-pill progress-bar-animated mx-2 text-center" href="https://codepreneurs.brainster.co"><big>Академија за програмирање</big><span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active px-lg-2 my-2">
                <a class="nav-link d-inline-block progress-bar-striped badge-pill progress-bar-animated mx-2 text-center" href="https://datascience.brainster.co"><span class="text-center"><big>Академија за data science</big></span><span class="sr-only">(current)</span></a>
            </li><li class="nav-item active px-lg-2 my-2">
                <a class="nav-link d-inline-block progress-bar-striped badge-pill progress-bar-animated mx-2 text-center" href="https://design.brainster.co"><big>Академија за дизајн</big><span class="sr-only">(current)</span></a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <button class="btn btn-danger my-2 my-sm-0 btn-hover progress-bar-striped progress-bar-animated" type="submit">Вработи наш студент</button>
          </form>
    </div>
    <div id="navBar">
      <ul class="nav-items d-inline-block">
        <li>
          <a href="#" id="closebtn">×</a>
        </li>
        <li>
            <a href="https://marketpreneurs.brainster.co" class="progress-bar-striped progress-bar-animated badge-pill my-3"  target="_blank"><big>Академија за маркетинг</big><span class="sr-only">(current)</span></a>
        </li>
        <li>
            <a href="https://codepreneurs.brainster.co" class="progress-bar-striped progress-bar-animated badge-pill my-3"  target="_blank"><big>Академија за програмирање</big><span class="sr-only">(current)</span></a>
        </li>
        <li>
            <a href="https://datascience.brainster.co" class="progress-bar-striped progress-bar-animated badge-pill my-3"  target="_blank"><span class="text-center"><big>Академија за data science</big></span><span class="sr-only">(current)</span></a>
        </li><li>
            <a href="https://design.brainster.co" class="progress-bar-striped progress-bar-animated badge-pill my-3"  target="_blank"><big>Академија за дизајн</big><span class="sr-only">(current)</span></a>
        </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <button class="btn btn-danger p-2 my-sm-0 btn-hover progress-bar-striped progress-bar-animated" href="contact.php" type="submit">Вработи наш студент</button>
    </form>
    </div>
    <div id="sliderBtn">
    <span class="large material-icons icon-flipped another-icon text-dark" >
      signal_cellular_alt
    </span>
    </div>
    <span class="large material-icons icon-flipped text-dark btn1">
      signal_cellular_alt
    </span>
</nav>
<!-- FORM PART -->
<div class="bg-warning">
    <div class="container d-flex justify-content-around flex-column input-banner">
        <div class="row flex-column justify-content-center align-items-center">
            <div class="col text-center py-3">
                <div class="title">Вработи студенти</div>
            </div>
            <div class="col text-center">
            <?php
               if($flag==1){
                 echo "<br><div class=\"text-success title\">Успешно се регистриравте</div>";
              }
              else if($flag==3){
                echo "<br><div class=\"text-danger title\">Веќе сте регистрирани со овој имејл</div>";
              }
              else echo "<br><div class=\"text-danger title\"></div>";
            ?>
            </div>
        </div>
        <div class="row justify-content-center align-items-center mb-5">
            <div class="col d-flex justify-content-center align-items-start">
              <form class="form" id="form" action="contact.php" onsubmit="return checkInputs(form)" method="POST">
                <div class="form-row">
                  <div class="col-md-6">
                    <div class="formControl mb-0 pb-0">
                      <label for=""><b>Име и презиме</b></label>
                      <input type="text" class="" name="NameLastname" id="NameLastname" placeholder="Вашето име и презиме" style="font-style:italic">
                      <i class="fas fa-check text-success"></i>
                      <i class="fas fa-exclamation text-danger"></i>
                      <small class="text-danger"><b>Error message</b></small> 
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="formControl mb-0 pb-0">
                      <label for="NameCompany"><b>Име на компанија</b> </label>
                      <input type="text" class="" id="NameCompany" name="NameCompany" placeholder="Име на вашата компанија" style="font-style:italic">
                      <i class="fas fa-check text-success"></i>
                      <i class="fas fa-exclamation text-danger error"></i>
                      <small class="text-danger"><b>Error message</b></small> 
                    </div>
                  </div>
                </div>
                <div class="form-row">
                  <div class="col-md-6">
                    <div class="formControl mb-0 pb-0">
                      <label for="email"><b>Контакт имејл</b> </label>
                      <input type="email" class="" name="email" id="email" placeholder="Контакт имејл на вашата компанија" style="font-style:italic">
                      <i class="fas fa-check text-success"></i>
                      <i class="fas fa-exclamation text-danger"></i>
                      <small class="text-danger"><b>Error message</b></small> 
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="formControl mb-0 pb-0">
                      <label for="phone"><b>Контакт телефон</b></label>
                            <input type="text" class="" id="phone" name="phone" placeholder="Контакт телефон на вашата компанија" style="font-style:italic">
                            <i class="fas fa-check text-success"></i>
                            <i class="fas fa-exclamation text-danger"></i>
                            <small class="text-danger"><b>Error messsage</b></small>
                    </div>
                  </div>
                </div>
                <div class="form-row">
                  <div class="col-md-6">
                    <div class="formControl mb-0 pb-0">
                      <label for=""><b>Тип на студенти</b> </label>
                            <select id="selection" class="" aria-label="select example" name="type_of_student" placeholder="Изберете тип на студент">
                            <option value="" hidden>Изберете тип на студент</option>
                            <?php   
                                $conn = mysqli_connect("localhost", "root", "", "register");
                                if($conn-> connect_error) {
                                    die("Connection failed:". $conn-> connect_error);
                                }
                                $sql = "SELECT id, academy from academy";
                                $result = $conn-> query($sql);
                      
                                if ($result-> num_rows > 0) {
                                    while ($row = $result-> fetch_assoc()) {
                                      echo "<option>". $row["academy"] ."</option>";
                                    }
                                    echo "</select>";  
                                } else {
                                    echo "0 result";
                                }
                      
                                $conn-> close();
                              ?>
                      </select>
                      <i class="fas fa-check text-success"></i>
                      <i class="fas fa-exclamation text-danger"></i>
                      <small class="text-danger"><b>Error message</b></small> 
                    </div>
                  </div>
                  <div class="col-md-6 mb-3">
                    <div class="formControl mb-0 pb-0">
                    <label></label>
                    <button class="btn btn-primary btn-danger button-submit my-1" type="submit" value="Submit">испрати</button>
                </div>
                 </div>
                
              </form>
            </div>
        </div>     
    </div>
</div>
 <!-- CREATING FOOTER -->
 <footer class="container-fluid bg-dark text-center py-2 py-sm-3 text-white">
      <p class="mb-0">Изработено со <i class="fas fa-heart text-danger"></i> од студентите на Brainster</p>
</footer>
<!-- SCRIPT FOR VALIDATION FOR FORM INPUT AND ADDING JS CODE FOR SIDE BAR FOR SMALL AND MOBILE DEVICES -->
<script>
  const sliderBtn = document.getElementById('sliderBtn');
  const navBar = document.getElementById('navBar');

  sliderBtn.addEventListener('click', () => {
    console.log('Button clicked');
    navBar.classList.toggle('open');
  });
  </script>
  <script>
    const form = document.getElementById('form');
    const name = document.getElementById('NameLastname');
    const company = document.getElementById('NameCompany');
 
    const email = document.getElementById('email');
    const phone = document.getElementById('phone');
    const selections = document.getElementById('selection');
    
    function checkInputs() {
    const nameValue = name.value.trim();
    const companyValue = company.value.trim();
    const emailValue = email.value.trim();
    const phoneValue = phone.value.trim();
    const selectionValue = selections.value.trim();
    flag = true;
   
    if(nameValue === '' || nameValue === null) {
         setErrorFor(name, 'Не e внесено вашето име и презиме');
         flag = false;
    } else {
          setSuccessFor(name);
    }
    if(companyValue === '' || companyValue === null) {
         setErrorFor(company, 'Не e внесено името на компанијата');
         flag = false;
    } else {
          setSuccessFor(company);
    } 
    if(emailValue === '' || emailValue === null) {
         setErrorFor(email, 'Не e внесен контакт имeјлот на вашата компанија');
         flag = false;
    } else if((validateEmail(emailValue)) === false) {
      setErrorFor(email, 'Не е валидна имeјл адресата на вашата компанија');
      flag = false;
    }
    else {
          setSuccessFor(email, 'Внесовте точен имајл');
    } 
    if(phoneValue === '' || phoneValue === null) {
         setErrorFor(phone, 'Не e внесен контакт телефонот на вашата компанија');
         flag = false;
    } else if (validatePhone(phoneValue) === false) {
      setErrorFor(phone, 'Не внесовте точен број');
      flag = false;
    }
    else {
          setSuccessFor(phone);
    }
    if(selectionValue === '' || selectionValue === null) {
         setErrorFor(selections, 'Изберете тип на студент');
         flag = false;
    } else {
          setSuccessFor(selections);
    } 
    return flag
  }

  function  setErrorFor(input, message) {
    const formcontrol = input.parentElement;
    const small = formcontrol.querySelector('small')
    small.innerText = message;

    formcontrol.className = 'formControl error'
  }
  function setSuccessFor(input) {
   const formcontrol = input.parentElement;
   formcontrol.className = 'formControl success'
  }
  function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email); }
function validatePhone(phone) {
  const re = /((00)?\+?[389]{3})?[\/\-\s*\.]?(((\(0\))|0)?\s*7\d{1})[\/\-\s*\.\,]?([\d]{3})[\/\-\s*\.\,]?([\d]{3})/;
  return re.test(phone)
}
  </script> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="./js/project_1.js"></script>
       <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
       integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
      </script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
       integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
      </script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js"
       integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous">
      </script>  
</body>
</html>