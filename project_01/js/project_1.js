// JS CODE FOR FILTERING CARDS
document.querySelector("#filter-coding").addEventListener("change", filterCoding);
document.querySelector("#filter-design").addEventListener("change", filterDesign);
document.querySelector("#filter-marketing").addEventListener("change", filterMarketing);


function filterCoding() {
    hideAllCards();

    if(document.querySelector("#filter-coding").checked) {
        var codingCards = document.querySelectorAll(".coding");
        codingCards.forEach(codingCard => {
            codingCard.style.display = "inline-block";
        });

        document.querySelector("#filter-design").checked = false;
        document.querySelector("#filter-marketing").checked = false;
    } else {
        showAllCards();
    }
}

function filterDesign() {
    hideAllCards();

    if(document.querySelector("#filter-design").checked) {
        var designCards = document.querySelectorAll(".design");
        designCards.forEach(designCard => {
            designCard.style.display = "inline-block";
        });

        document.querySelector("#filter-coding").checked = false;
        document.querySelector("#filter-marketing").checked = false;
    } else {
        showAllCards();
    }
}

function filterMarketing() {
    hideAllCards();

    if(document.querySelector("#filter-marketing").checked) {
        var marketingCards = document.querySelectorAll(".marketing");
        marketingCards.forEach(marketingCard => {
            marketingCard.style.display = "inline-block";
        });

        document.querySelector("#filter-design").checked = false;
        document.querySelector("#filter-coding").checked = false;
    } else {
        showAllCards();
    }
}

// JS CODE FOR THE SIDEBAR FOR SMALL AND MOBILE DEVICES 
const sliderBtn = document.getElementById('sliderBtn');
const navBar = document.getElementById('navBar');

sliderBtn.addEventListener('click', () => {
  console.log('Button clicked');
  navBar.classList.toggle('open');
});
function hideAllCards() {
    var allCards = document.querySelectorAll(".cards");  

    allCards.forEach(cards => {
        cards.style.display = "none";
    });
}

function showAllCards() {
    var allCards = document.querySelectorAll(".cards");  

    allCards.forEach(cards => {
        cards.style.display = "inline-block";
    });
}
// JQUERY CODE FOR CHECKBOX
  $("input[type='checkbox']:checked").parent().addClass("input-projects");
  $('input[type=checkbox]').change(function() {
       $("input[type='checkbox']").parent().removeClass("input-projects");
       $("input[type='checkbox']:checked").parent().addClass("input-projects");
         });