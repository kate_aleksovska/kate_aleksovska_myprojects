<?php
require_once 'Furniture.php';
require_once 'Sofa.php';
require_once 'Bench.php';
require_once 'Chair.php';
echo "<h1>Furniture</h1>";
$f1 = new Furniture(200, 300, 100);

$f1->setIsForSeating(0);
$f1->setIsForSleaping(1);
echo "{$f1->getIsForSeating()} seating";
echo "<br>";
echo "{$f1->getIsForSleaping()} sleaping";
echo "<br>";
echo "<br> area: ";
echo "{$f1->Area()} cm2";
echo "<br>";
echo "<br> Volume: ";
echo "{$f1->Volume()} cm3";


echo "<h1>Sofa for seating</h1>";
$s1 = new Sofa (200,300,200,5,10,3);

$s1->setIsForSleaping(0);
echo "<br> area: ";
echo "{$s1->Area()} cm2";
echo "<br> Volume: ";
echo "{$s1->Volume()} cm3";
echo "<br> area_opened: ";
echo "{$s1->areaOpened()} cm2";
echo "<br>";
echo "<h1>Sofa for sleaping</h1>";
$s1->setIsForSleaping(1);
echo "<br> Area ";
echo "{$s1->Area()} cm2";
echo "<br> Volume";
echo "{$s1->Volume()} cm3";
echo "<br> area_opened: ";
echo "{$s1->areaOpened()} cm2";
echo "<br> type of furniture: ";
echo $s1->sneakpeek();
echo "<br> Print furniture: ";
echo $s1->print();
echo "<br>Print: long info: ";
echo $s1->fullinfo();

echo "<h1>Chair</h1>";
$c1 = new Chair (250, 250, 250);
$c1->setIsForSleaping(0);
echo "<br> area: ";
echo "{$c1->Area()} cm2";
echo "<br> Volume: ";
echo "{$c1->Volume()} cm3";
echo "<br> type of furniture: ";
echo $c1->sneakpeek();
echo "<br> Print furniture: ";
echo $c1->print();
echo "<br>Print: long info: ";
echo $c1->fullinfo();

echo "<h1>Bench</h1>";
$b1 = new Bench (500, 600, 250, 3, 2, 300);
$b1->setIsForSleaping(1);
echo "<br> area: ";
echo "{$b1->Area()} cm2";
echo "<br> Volume:";
echo "{$b1->Volume()} cm3";
echo "<br> type of furniture: ";
echo $b1->sneakpeek();
echo "<br> Print furniture: ";
echo $b1->print();
echo "<br>Print: long info: ";
echo $b1->fullinfo();
echo "<hr>";

echo "<h1>All furnitures</h1>";
$furnitures = [$f1, $c1, $s1, $b1];

forEach($furnitures as $furniture) {
echo "<br> area: ";
echo "{$furniture->Area()} cm2";
echo "<br> Volume: ";
echo "{$furniture->Volume()} cm3";
echo "<br> type of furniture: ";
echo $furniture->sneakpeek();
echo "<br> Print furniture: ";
echo $furniture->print();
echo "<br>Print: long info: ";
echo $furniture->fullinfo();
echo "<hr>";
}

