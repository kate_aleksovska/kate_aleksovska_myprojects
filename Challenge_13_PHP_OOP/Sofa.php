<?php
require_once 'Furniture.php';
class Sofa extends Furniture {
    private $seats;
    private $armrests;
    private $lengthOpened;

    public function __construct(int $width, int $length, int $height, int $seats, int $armrests, int $lengthOpened) 
    {
        parent::__construct($width, $length, $height);
        $this->setSeats($seats);
        $this->setArmrest($armrests);
        $this->setLengthOpen($lengthOpened);
    }

    public function setSeats($seats) {
      $this->seats = $seats;
    }
    public function setArmrest($armrests) {
        $this->armrests = $armrests;
    }
    public function setLengthOpen($lengthOpened) {
        $this->lengthOpened = $lengthOpened;
    }
    public function getSeats() {
        return $this->seats;
      }
    public function getArmrest() {
        return $this->armrests;
    }
    public function getLengthOpen() {
        return $this->lengthOpened;
    }

    public function areaOpened() {
        if($this->getIsForSleaping()) {
            return $this->getWidth()*$this->getLengthOpen();
        } else {
            return "This sofa is for sitting only, it has {$this->getArmrest()} armrests and {$this->getSeats()} seats.";
        }
    }

}