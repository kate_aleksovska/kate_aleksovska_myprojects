<?php
require_once 'Printable.php';
class Furniture implements Printable {
    private $width;
    private $length;
    private $height;
    private $isForSeating;
    private $isForSleeping;

    public function __construct( int $width, int $length, int $height) {
        $this->setWidth($width);
        $this->setLength($length);
        $this->setHeight($height);
    }

    public function setWidth($width) {
        $this->width=$width;
    }
    public function setLength($length) {
        $this->length=$length;
    }
    public function setHeight($height) {
        $this->height=$height;
    }
    public function setIsForSeating($isForSeating) {
        $this->isForSeating=$isForSeating;
    }
    public function setIsForSleaping($isForSleeping) {
        $this->isForSleeping=$isForSleeping;
    }
    public function getWidth() {
        return $this->width;
    }
    public function getLength() {
        return $this->length;
    }
    public function getHeight() {
        return $this->height;
    }
    public function getIsForSeating() {
        return $this->isForSeating;
    }
    public function getIsForSleaping() {
        return $this->isForSleeping;
    }

public function Area() {
    return $this->getWidth()*$this->getLength();
}

public function Volume() {
    return $this->Area()*$this->getHeight();
}
public function Sleapable() {
    if($this->getIsForSleaping() != 0 && $this->sneakpeek() == 'Sofa') {
        return "Sleapable";
    } else {
        return "sitting-only";
    }
}
public function sneakpeek()
{
    return get_class($this);
}
public function print() {
    return "{$this->sneakpeek()}, {$this->Sleapable()}, {$this->Area()}cm2";
}
public function fullinfo() {
    return "{$this->print()}, {$this->getWidth()}cm, {$this->getLength()}cm, {$this->getHeight()}cm";
}
}