@extends('layout.master')

@section('content')
<main class="container p-4">
    <div class="row flex-column">
        <div class="col-5 mx-auto">
            <p class="pb-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum tenetur nesciunt maxime fugit itaque, explicabo voluptatibus omnis veniam molestias qui deserunt repellendus, error, nemo quo expedita assumenda culpa architecto nostrum? Blanditiis, doloribus reprehenderit! Exercitationem tenetur aperiam minima placeat. Dolore animi voluptatem voluptates laboriosam qui modi commodi saepe suscipit velit sunt.</p>

            <p class="pb-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis atque necessitatibus dolorum consequuntur voluptate dolores commodi. Velit fugiat similique numquam vero! Quod, porro voluptatem. Labore voluptates aut quos minima fuga tempore placeat sequi. Non ducimus quia nesciunt consequatur impedit doloribus dignissimos officia iusto sint cum velit veniam perspiciatis consectetur illum, ad deserunt dicta ea laudantium!</p>

            <p class="pb-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequuntur aliquid nihil ducimus reprehenderit inventore amet nesciunt vel, autem dolor omnis modi voluptatibus distinctio quidem ea. Eveniet quos explicabo voluptate odit quisquam deserunt, labore officiis recusandae consequuntur culpa doloribus sint. Mollitia et ab, quae omnis aut quos alias quod eligendi, eos, sed dignissimos? Voluptates, non esse? Natus modi autem qui in ad, architecto, corporis error officia officiis voluptate sapiente, aperiam sit!</p>

            <p class="pb-4">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ullam doloremque itaque modi facilis ipsum veritatis tempore beatae iure? Facilis possimus, eligendi voluptate deserunt fugiat veritatis dolorum ab hic fuga accusantium, tempora perspiciatis impedit, explicabo reiciendis quibusdam sapiente similique ex placeat aliquam dignissimos ipsum? Doloremque, voluptates. Ducimus assumenda, eveniet quos voluptatem doloribus modi placeat reiciendis aspernatur voluptatibus minus voluptate labore vitae!</p>

            <p class="pb-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Iure dolor et, inventore fuga consectetur similique a nisi quo obcaecati excepturi culpa, dolorum repellendus quibusdam aut error odit dignissimos. Temporibus hic, cumque asperiores quaerat enim labore animi, obcaecati, consequuntur ea porro odio ipsam. Ullam eius facilis tempore molestias impedit! Ad quos dolores ab quas totam sit ullam, possimus architecto obcaecati quibusdam?</p>
        </div>
    </div>
    <div class="row flex-column">
        <div class="col-5 mx-auto">
            <h3 class="pt-5"><b> Final Frontier</b></h3>
            <p class="pb-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos aliquam numquam voluptatibus cupiditate tempora accusamus hic asperiores eaque, possimus assumenda aspernatur! Magni accusantium recusandae suscipit dolorum saepe quos laudantium dicta eos, odit vitae accusamus assumenda fugiat eveniet reiciendis quibusdam veritatis repellendus natus minus maiores voluptates perspiciatis! Quasi accusamus provident consectetur!</p>

            <p class="pb-4">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quidem qui enim, quibusdam numquam doloremque perferendis facilis dolore perspiciatis voluptates sapiente incidunt quisquam magnam, dolores mollitia rerum dolor corporis esse itaque eius molestias iste quam consectetur illum voluptatibus. Harum doloremque necessitatibus maxime consequuntur nesciunt alias at nobis ad dolores dicta quisquam illo fugit sapiente placeat vero, repudiandae delectus dolore, aliquam tempore.</p>

            <p class="pb-4 text-dark"><i>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Autem architecto expedita impedit neque assumenda, error fuga, aspernatur aliquid cupiditate rerum omnis explicabo quod mollitia velit dolor nobis beatae accusantium quae vero odio suscipit. Rem, nesciunt recusandae culpa placeat sit velit, id, deleniti minima illum natus esse? Reiciendis pariatur in corrupti obcaecati praesentium ex quibusdam consectetur natus, nostrum, expedita officia similique est odio eveniet, ut odit cum quaerat fuga quam sit? Provident tempora quisquam veniam, culpa vitae ipsum incidunt alias tenetur soluta! Ducimus deleniti est, quia hic tempora sunt cumque atque optio aspernatur aut vitae possimus tenetur explicabo! Enim, doloribus sunt?</i></p>

            <p class="pb-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis ullam laudantium vitae! Veniam libero ipsam asperiores quos distinctio, fugiat illo at ipsa aspernatur, alias iste quod. Labore saepe, minus eos modi, ipsa blanditiis enim nisi praesentium reprehenderit asperiores animi obcaecati!</p>

            <p class="pb-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam laudantium qui ab officia quasi, harum, obcaecati ducimus ut beatae consequatur vitae voluptas ratione esse libero quaerat rerum quidem! Rerum, rem modi magni vitae, molestiae deleniti quod ipsam a corrupti quia obcaecati. Laudantium sequi, quidem tenetur facere est nulla culpa similique minus harum deserunt corrupti quia nobis explicabo amet laborum quibusdam a molestias provident atque reprehenderit aliquid odit. Magni, exercitationem, iste minima blanditiis optio similique quibusdam nemo tempora deleniti earum at.</p>
        </div>
    </div>

    <div class="row flex-column">
        <div class="col-5 mx-auto">
            <h3 class="py-4"><b>Reaching for the stars</b></h3>
            <p class="pb-3">Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat deleniti vel aliquid, ipsum ratione commodi totam voluptate non nisi quod vero debitis est consectetur dicta quam, repellendus obcaecati repellat nihil. Ipsum voluptatibus reprehenderit quidem, delectus doloribus ullam vitae. In ex dolorum assumenda natus harum praesentium excepturi ad ut nesciunt eos, eveniet, nulla aut accusantium dicta fugiat possimus nisi, quaerat officiis veniam sit repellat atque beatae! Maiores excepturi possimus consectetur soluta placeat, obcaecati, sequi accusamus quis odit perspiciatis quod tempora, illo temporibus nesciunt quam ipsum! Autem natus quis fuga consequuntur illum expedita. Ipsam molestiae tempora atque, at similique non, sit perspiciatis rem doloremque placeat fugit minima ab dignissimos odio? Quidem, officia.</p>

            <figure>
                <img class="col-12 p-0 img-fluid" src="{{ asset('img/blog-image.jpg') }}" />
                <figcaption class="text-gray text-center"><i>Elit sint voluptate fugiat eu
                        ullamco ea ea.
                    </i></figcaption>
            </figure>

            <p class="py-3">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil eius quaerat officia minima enim porro nisi dolore, in repellat totam maiores cupiditate fuga sequi error nobis earum ratione eos voluptates aliquam reprehenderit praesentium animi sapiente! Inventore cum delectus alias harum.</p>

            <p class="py-3">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Laborum, aspernatur unde quasi dignissimos saepe nisi natus ipsam facilis consequuntur eligendi commodi excepturi harum at delectus quaerat velit iusto fuga perspiciatis dicta nam eos! Iusto laudantium cupiditate, quaerat quos sequi distinctio vero ut quidem repudiandae repellendus dolores libero consectetur quis possimus eum sit veniam non aut perspiciatis, eligendi provident tenetur, atque corporis? Dignissimos modi illo quis quasi omnis sint quas mollitia in placeat, alias tempora similique nulla! Accusantium aperiam sunt tempora molestias, magni commodi consequuntur sit inventore, ipsum dolores nobis sequi!</p>

            <p class="py-3">Dolor nulla <u>voluptate velit</u>. Voluptate <u>ipsum consequat</u> excepteur nisi aliqua.
            </p>
        </div>
    </div>
</main>
@endsection