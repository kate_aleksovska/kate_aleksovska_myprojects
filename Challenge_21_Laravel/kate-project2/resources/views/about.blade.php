@extends('layout.master')

@section('content')
<main class="container p-3">
    <div class="row d-flex flex-column">
        <div class="col-6 mx-auto">
            <p class="py-3">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Natus recusandae quisquam alias similique tempore corporis quidem sit officia pariatur tempora, commodi quam, facilis blanditiis aliquid? Itaque, perspiciatis quidem. Dolores voluptatibus, sit quo nesciunt expedita optio nemo? Aliquid, alias enim soluta illo sunt amet tempora dolor dolores, repellat delectus, consectetur quos?</p>

            <p class="py-3">Lorem ipsum dolor sit amet consectetur adipisicing elit. Corrupti est voluptatem molestias consequuntur commodi incidunt tempore fugit laboriosam, dolorem quibusdam velit laudantium ea voluptatibus enim qui rerum beatae nisi eum explicabo magni quam suscipit. Perspiciatis non voluptatum voluptas harum sapiente eos distinctio placeat vel blanditiis?

            </p>

            <p class="py-3">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ratione cupiditate quia natus eligendi nesciunt reiciendis ad tenetur minima consequatur perspiciatis, aliquam debitis rerum quod nulla. Explicabo ratione labore perspiciatis nobis cum ducimus vitae provident nam, nisi quidem modi natus laudantium fugiat soluta. Excepturi qui assumenda asperiores animi in.</p>
        </div>

    </div>
</main>
@endsection