<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title }}</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    <style>
        header {
            background:
            url({{ asset($bg_photo) }});
            background-size: cover;
            background-position: center;
        }
    </style>
</head>

<body>
    @if (Request::is('samplePost'))
        <header class="header-height">
    @else
        <header>
    @endif
        <nav class="navbar navbar-expand-lg navbar-dark bg-light">
            <div class="container">
                <a class="navbar-brand" href="{{ route('index') }}"><b>Blog</b></a>
                <ul class="navbar-nav ml-auto">
                    @if (Request::is('/'))
                        <li class="nav-item active">
                            <a class="nav-link" href="{{ route('index') }}">Home
                            </a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('index') }}">Home
                            </a>
                        </li>
                    @endif

                    @if (Request::is('about'))
                        <li class="nav-item active">
                            <a class="nav-link" href="{{ route('about') }}">About</a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('about') }}">About</a>
                        </li>
                    @endif

                    @if (Request::is('samplePost'))
                        <li class="nav-item active">
                            <a class="nav-link" href="{{ route('samplePost') }}">Sample post</a>
                        </li>
                    @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('samplePost') }}">Sample post</a>
                    </li>
                    @endif
                    
                    @if (Request::is('contact'))
                        <li class="nav-item active">
                            <a class="nav-link" href="{{ route('contact') }}">Contact</a>
                        </li>
                    @else 
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('contact') }}">Contact</a>
                        </li>
                    @endif
                </ul>
            </div>
        </nav>

        <div class="container ">
            <div class="row h-100">
                <div class="col-9 mx-auto text-center text-white d-flex align-items-center flex-column justify-content-center">
                    @if (Request::is('samplePost'))
                        <h1 class="text-left"> <b>{{ $title }}</b> </h1>
                        <h3 class="text-left"> {{ $sub_title }} 
                        </h3>
                        <h3 class="text-left w-100"> {{ $sm_letters }}</h3>
                    @else
                        <h1> {{ $title }} </h1>
                        <h3> {{ $sm_letters }} </h3>
                    @endif
                </div>
            </div>
        </div>
    </header>