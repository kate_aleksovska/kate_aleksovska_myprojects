@extends('layout.master')

@section('content')
<main class="container p-3">
    <div class="row d-flex flex-column">
        <div class="card border-card col-5 mx-auto">
            <div class="card-body">
                <h2 class="card-title"><b> Ipsum</b></h2>
                <p class="card-text">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ad, praesentium!</p>
                <p class="card-subtitle mb-2 text-gray">
                    <i>Posted by </i><b>John Doe</b>
                </p>
            </div>
        </div>
        <div class="card border-card col-5 mx-auto">
            <div class="card-body">
                <h2 class="card-title"><b>Lorem Ipsum 2</b></h2>
                <p class="card-subtitle mb-2 text-gray">
                    <i>Posted by</i><b> John Doe</b> in another
                        boring news
                    </i>
                </p>
            </div>
        </div>
        <div class="card border-card col-5 mx-auto">
            <div class="card-body">
                <h2 class="card-title"><b>Lorem Ipsum 3</b></h2>
                <p class="card-text h5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis laudantium quo cum, sapiente ex nobis fuga beatae iusto saepe quibusdam totam? Delectus, incidunt magnam itaque officia molestias, illum, veniam eos sed nesciunt quis doloribus. Aliquam et necessitatibus architecto mollitia soluta, velit fugiat dolores quis placeat?</p>
                <p class="card-subtitle text-gray my-3 ">
                    <i>Posted by <b>Jane Doe</b></i>
                </p>
            </div>
        </div>
        <div class="card border-card col-5 mx-auto">
            <div class="card-body">
                <h2 class="card-title"><b>Lorem Ipsum 4</b></h2>
                <p class="card-text">Mollit aute dolore proident consectetur exercitation ex.</p>
                <p class="card-subtitle  text-gray my-3">
                    <i>Posted by <b>Missy Doe</b></i>
                </p>
            </div>
        </div>

    </div>
    <div class="row col-5 py-3 mx-auto justify-content-end">
            <button type="button" class="btn btn-primary">Older posts</button>
    </div>
</main>
@endsection

