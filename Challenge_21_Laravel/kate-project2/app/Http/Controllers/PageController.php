<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $index = "index";
        $title = "A Clean Blog";
        $sm_letters = "A Blog Theme Start By Bootstrap";
        $bg_photo = '/img/home-bg.jpg';
        return view('index', compact('title', 'sm_letters', 'bg_photo'));
    }

    public function about()
    {
        // $index = "index";
        $title = "About Me";
        $sm_letters = "This is what I do.";
        $bg_photo = '/img/about-bg.jpg';
        return view('about', compact('title', 'sm_letters', 'bg_photo'));
    }

    public function contact()
    {
        // $index = "index";
        $title = "Contact Me";
        $sm_letters = "Have questions? I have answers!";
        $bg_photo = '/img/contact-bg.jpg';
        return view('contact', compact('title', 'sm_letters', 'bg_photo'));
    }

    public function samplePost()
    {
        // $index = "index";
        $title = "Man must explore, and this is exploration at it's greatest.";
        $sub_title = "Problems look mighty small from 150 miles up.";
        $sm_letters = "Posted by Start Bootstrap on August 24, 2018";
        $bg_photo = '/img/blog-image.jpg';
        return view('samplePost', compact('title', 'sub_title', 'sm_letters', 'bg_photo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
