<?php

use App\Http\Controllers\AplicantsController;
use App\Http\Controllers\ChangePasswordController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Route::middleware(['auth'])->group(function () {

    Route::prefix('dashboards')->group(function() {
        Route::get('',                          [DashboardController::class, 'index'])->name('dashboards.index');
        Route::post('dashboard',                [DashboardController::class, 'store'])->name('dashboards.store');
        Route::get('dashhboard/{id}',           [DashboardController::class, 'show'])->name('dashboards.show');
        Route::put('dashboard/{id}',            [DashboardController::class, 'update'])->name('dashboards.update');
    });
    Route::put('change-password', [ChangePasswordController::class, 'update'])->name('change-password.update');
    Route::prefix('users')->group(function() {
        Route::get('',             [UserController::class, 'index'])->name('users.index');
        Route::put('{user}',       [UserController::class, 'update'])->name('users.update');
        Route::get('user/{id}',    [UserController::class, 'show'])->name('users.show');
    });

    Route::prefix('aplicants')->group(function() {     
        Route::put('{aplicant}',           [AplicantsController::class, 'update'])->name('aplicants.update');
        Route::get('',                     [AplicantsController::class, 'index'])->name('aplicants.index');
        Route::delete('aplicant/{id}',     [AplicantsController::class, 'destroy'])->name('aplicants.destroy');
    });
    Route::prefix('projects')->group(function() {
        Route::get('',                     [ProjectController::class, 'index'])->name('projects.index');
        Route::get('project/create',       [ProjectController::class, 'create'])->name('projects.create');
        Route::post('project',             [ProjectController::class, 'store'])->name('projects.store');
        Route::get('project/{id}',         [ProjectController::class, 'show'])->name('projects.show');
        Route::get('project/{id}/edit',    [ProjectController::class, 'edit'])->name('projects.edit');
        Route::put('{project}',            [ProjectController::class, 'update'])->name('projects.update');
        Route::delete('project/{id}',      [ProjectController::class, 'destroy'])->name('projects.destroy');
    });
});




require __DIR__ . '/auth.php';
