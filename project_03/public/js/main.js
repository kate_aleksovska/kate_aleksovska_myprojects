// toggle for password visibility on change password and loggin and refistar
$(document).ready(function () {
    $(".toggle-password").click(function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
    // });
    $("#menu-icon").click(function () {
        $(".sub-nav").toggleClass("visible");
    });
    $(".delete").on("click", function (e) {
        e.preventDefault();
        let form = $(this).parents("form");
        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, delete it!",
        }).then((result) => {
            if (result.isConfirmed) {
                form.submit();
            }
        });
    });
    // for check and uncheck academies on user profile
    let arr = [];
    $('input[id^="academy"]').click(function () {
        var idCard = this.id;
        arr = $("[name='academy[]']");

        $.each(arr, function (i, j) {
            $(`*[id="${j.id}"]`).prop("checked", false);
        });
        $(`*[id="${idCard}"]`).prop("checked", true);
    });

    // for check and uncheck academies on dashboard profile
    let arr_project = [];
    $('input[id^="academies_project"]').click(function () {
        var idCard = this.id;
        arr_project = $("[name='academy[]']");
        if($(`*[id="${idCard}"]`).is(":checked")) {
            $(`*[id="${idCard}"]`).click(function () {
                location.reload();
            })
         }
        $.each(arr_project, function (i, j) {
            $(`*[id="${j.id}"]`).prop("checked", false);
        });
        $(`*[id="${idCard}"]`).prop("checked", true);
        if ($("#attrText").attr(`data-${idCard}`) === idCard) {
            $.each(academies, function (i, j) {
                $(`.academies_project${j.id}`).css("display", "none");
            });
            $(`.${idCard}`).css("display", "block");
        } else if (idCard === "academies_project0") {
            $.each(academies, function (i, j) {
                $(`.academies_project${j.id}`).css("display", "block");
            });
        } else {
            $.each(academies, function (i, j) {
                $(`.academies_project${j.id}`).css("display", "none");
            });
        }
    });

    //for multiple selection
    $("#academy").select2({
        placeholder: "Select",
        allowClear: true,
    });
});

function AddReadMore() {
    //This limit you can set after how much characters you want to show Read More.
    var carLmt = 500;
    // Text to show when text is collapsed
    var readMoreTxt = " ... Show More";
    // Text to show when text is expanded
    var readLessTxt = " ...Show Less";

    //Traverse all selectors with this class and manupulate HTML part to show Read More
    $(".addReadMore").each(function () {
        if ($(this).find(".firstSec").length) return;

        var allstr = $(this).text();
        if (allstr.length > carLmt) {
            var firstSet = allstr.substring(0, carLmt);
            var secdHalf = allstr.substring(carLmt, allstr.length);
            var strtoadd =
                firstSet +
                "<span class='SecSec'>" +
                secdHalf +
                "</span><p class='readMore'  title='Click to Show More'>" +
                readMoreTxt +
                "</p><p class='readLess' title='Click to Show Less'>" +
                readLessTxt +
                "</p>";
            $(this).html(strtoadd);
        }
    });
    //Read More and Read Less Click Event binding
    $(document).on("click", ".readMore,.readLess", function () {
        $(this)
            .closest(".addReadMore")
            .toggleClass("showlesscontent showmorecontent");
    });
}
$(function () {
    //Calling function after Page Load
    AddReadMore();
});
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })
  upload.onchange = evt => {
    const [file] = upload.files
    if (file) {
        imgShow.src = URL.createObjectURL(file)
    }
  }
