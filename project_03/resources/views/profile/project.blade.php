@extends('layout.master')
@section('title', 'Project')
@section('content')
          <div class="contaner-fluid px-5 bg-image-create">
            @include('welcome')
            @if (Auth::user()->profile_complete > 0)
              <div class="row">
                  <div class="col-6">
                      <h3>Have a new idea to make the world better?</h3>
                      <h1>Create new project<a href="{{route('projects.create')}}">+</a></h1>
                  </div>
              </div>
                  @include('card-project')
               @endif
            </div>
@endsection
