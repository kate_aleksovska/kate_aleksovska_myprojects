@extends('layout.master')
@section('title', 'Edit projects')
@section('content')
@include('welcome')
@if (Auth::user()->profile_complete > 0)
   <div class="container-fluid p-5">
       <div class="row">
           <div class="col">
            <h1>New Project</h1>
           </div>
       </div>
     
        <form id="edit_project" data-parsley-validate>
            @method('PUT')
            {{ csrf_field() }}
           <div class="row">
               <div class="col-sm-6">
                <div class="col-lg-12">
                    <div class="position-relative form-group">
                        <label>Enter name for the project</label>
                        <input type="text" name="new_project"
                            class="form-control" id="new_project" value="{{ old('new_project', $project->name) }}">
                    </div>
                    <div id="name_project">
                    </div>
                    <div class="position-relative form-group">
                        <label>Description of a project</label>
                        <textarea name="new_description" class="form-control text-left @error('biography') is-invalid @enderror"
                            rows="8" id="new_description">{{ old('new_description', $project->description) }}</textarea> 
                        <div id="project_description" class="mb-3">
                        </div>
                    </div>
                </div> 
               </div>
                <div class="col-sm-6">
                <h1>What I need</h1>
                <div class="form-group row justify-content-around align-items-center skills-checkboxes scroll-skills @error('academy') is-invalid @enderror form-check">
                    @foreach ($academies as $academy)
                    <label class="custom-checkbox text-center m-1 form-check-label"><input class="form-check-input" type="checkbox" name="academy" value="{{ $academy->id }}" id="academies{{$academy->id}}"  @if (in_array($academy->id, $project->academy()->pluck("academy_project.academy_id")->toArray())) checked @endif><div class="checkmark d-flex justify-content-center align-items-center font-weight-bold">{{ $academy->name }}</div></label>
                    @endforeach
                </div>
                <div id="academy_error">
                </div>
                </div>
           </div>
           <div class="row">
            <div class="col text-right">
                <button type="button" class="btn btn-lg bg-color-dark-green text-light" id="edit_new_project" data-id="{{$project->id}}">EDIT</button>
               </div>
           </div>
            </form>
       
       </div>
 @endif
@endsection