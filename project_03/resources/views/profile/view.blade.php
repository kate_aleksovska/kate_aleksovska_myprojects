@extends('layout.master')
@section('title', 'Create profile')
@section('content')
    <div class="container-fluid px-5">
        <div class="row">
            <div class="col-sm-6 offset-sm-3 col-12">
                @if (Session::has('success'))
                    <div class="alert alert-success text-center">
                        {{ Session::get('success') }}
                    </div>
                @endif
                @if (Session::has('error'))
                    <div class="alert alert-danger text-center">
                        {{ Session::get('error') }}
                    </div>
                @endif
            </div>
        </div>
        <form method="POST" action="{{ route('users.update', Auth::user()->id) }}" enctype="multipart/form-data" novalidate>
            <div class="row">
                @csrf
                @method('PUT')
                <div class="col-5">
                    <h1>My profile</h1>
                    <div class="row">
                        <div class="col-6 text-center pb-5">
                            <div class="mb-4 form-group">
                            <label for="upload">
                                    <span aria-hidden="true">
                                        <img src="{{Auth::user()->getUserAvatar()}}" class="rounded-circle border-rounded-circle" style="width:150px !important; height:150px !important" id="imgShow"/> 
                                        <p class="font-weight-bold text-muted">Click here to upload image</p> 
                                    </span>              
                                    <input type="file" id="upload" style="display:none" name="user_picture" accept="image/png, image/jpeg, image/jpg" class=" @error('surname') is-invalid @enderror" autofocus>
                                    
                              </label>
                              @error('user_picture')
                                    <small class="color-dark-yelow font-weight-bold">{{ $message }}</small>
                                @enderror
                             
                            </div>
                        </div>
                        <div class="col-6">
                                    <input id="name" type="text" name="name" value="{{ old('name', auth::user()->name) }}" autofocus
                                class="input-register form-control text-muted font-weight-bold @error('name') is-invalid @enderror" />
                            @error('name')
                                <small class="color-dark-yelow font-weight-bold">{{ $message }}</small>
                            @enderror
    
                            <input id="surname" type="text" name="surname" value="{{ old('surname', auth::user()->surname) }}"
                                class="input-register form-control text-muted font-weight-bold @error('surname') is-invalid @enderror" autofocus />
                            @error('surname')
                                <small class="color-dark-yelow font-weight-bold">{{ $message }}</small>
                            @enderror
                            <input id="email" type="email" name="email" value="{{ old('email', auth::user()->email) }}"
                                class="input-register form-control text-muted font-weight-bold @error('email') is-invalid @enderror"/>
                            @error('email')
                                <small class="color-dark-yelow font-weight-bold">{{ $message }}</small>
                            @enderror  
                        </div>
                        <div class="col-12">
                            <div class="form-group text-muted">
                                <h4>Biography</h4>
                                <textarea name="biography" class="form-control text-left @error('biography') is-invalid @enderror"
                                    rows="8">{{ Auth::user()->biography }}</textarea>
                                @error('biography')
                                    <small class="color-dark-yelow font-weight-bold">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-7">
                    <div class="row">
                        <div class="col-12">
                            <h1>Skills</h1>
                            
                            <div class="form-group row justify-content-around align-items-center skills-checkboxes scroll-skills @error('skills') is-invalid @enderror form-check" id="skills">

                                @foreach ($skills as $skill)
                                <label class="custom-checkbox text-center m-1 d-inline-block"><input type="checkbox" name="skills[]" value="{{ $skill->id }}" @if (in_array($skill->id, Auth::user()->skills()->pluck('skill_user.skill_id')->toArray())) checked @endif>
                                <div class="checkmark d-flex justify-content-center align-items-center font-weight-bold">{{ $skill->name }}</div> </label>
                                @endforeach
                            </div>
                        
                            @error('skills')
                            <div class="color-dark-yelow font-weight-bold">
                                {{ $message }}
                            </div>
                            @enderror    
                        </div>
                        <div class="col-12">
                            <h1>Academies</h1>
                            <div class="form-group row justify-content-around align-items-center skills-checkboxes scroll-skills @error('academy') is-invalid @enderror form-check">
                                @foreach ($academies as $academy)
                                <label class="custom-checkbox text-center m-1"><input type="checkbox" name="academy[]" value="{{ $academy->id }}" @if ($academy->id == Auth::user()->academy_id) checked @endif id="academy{{$academy->id}}">
                                    <div class="checkmark d-flex justify-content-center align-items-center font-weight-bold">{{ $academy->name }}</div></label>
                                @endforeach
                            </div>
                            @error('academy')
                            <div class="color-dark-yelow font-weight-bold">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
    
                </div>
                <div class="col-12 text-right">
                    <button class="btn btn-lg bg-color-dark-green text-white font-weight-bold px-5" style="width:200px" id="edit_user_profile">
                        Edit
                    </button>
                </div>
    
            </div>
        </form>
    </div>
@endsection
@section('script')
<script>
     var academies = <?php echo json_encode($academies) ?>;
</script>
@endsection
