<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js"
integrity="sha384-VHvPCCyXqtD5DqJeNxl2dtTyhF78xXNXdkwX1CZeRusQfRKp+tA7hAShOK/B/fQ2" crossorigin="anonymous"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
@yield('script')
<script src="{{ asset('js/main.js') }}"></script>
<script>
    $("#submit-reset-pass").click(function() {
        let data = {
            '_token': '{{ csrf_token() }}',
            current_password: $('#old_password').val(),
            new_password: $('#new_password').val()
        };
        $.ajax({
            type: 'PUT',
            url: '{{ route('change-password.update') }}',
            data: data,
            dataType: "json",
            enctype: "multipart/form-data",
            success: function(data) {
                $('#current_password').html('')
                $('#new_pass').html('')
                $('#old_password').val('')
                $('#new_password').val('')
                $(".close").click()
                // $('.sub-nav').toggleClass('visible');
                Swal.fire(
                    'Great!',
                    'Your password has been successfully changed!',
                    'success'
                )
            },
            error: function(error) {
                console.log(error.responseJSON.errors);
                let current_password = error.responseJSON.errors.current_password
                let new_password = error.responseJSON.errors.new_password
                $('#current_password').html('')
                $('#new_pass').html('')
                if (current_password) {
                    current_password.forEach(el => {
                        let current_msg =
                            `<span class="color-dark-yelow font-weight-bold font-weight-bold d-block">${el}</span>`
                        $('#current_password').append(current_msg)
                    });
                }
                if (new_password) {
                    new_password.forEach(elem => {
                        let new_msg =
                            `<span class="color-dark-yelow font-weight-bold font-weight-bold font-weight-bold d-block">${elem}</span>`
                        $('#new_pass').append(new_msg)
                    });
                }
            }
        });
    });
    // ajax for new project
    $("#submit_new_project").click(function() {
        var academies = [];
        $('input[name=academy]:checked').map(function() {
            academies.push($(this).val());
        });
        let data = {
            '_token': '{{ csrf_token() }}',
            new_project: $('#new_project').val(),
            new_description: $('#new_description').val(),
            academy: academies
        };
        console.log(data);
        $.ajax({
            type: 'POST',
            url: '{{ route('projects.store') }}',
            data: data,
            dataType: "json",
            enctype: "multipart/form-data",
            success: function(data) {
                console.log(data)
                $('#name_project').html('')
                $('#project_description').html('')
                $('#academy_error').html('')
                $('#new_description').val('')
                $('#new_project').val('')
                //for uncheck academies
                $.each(academies, function(i, j) {
                    let idAcademy = "academies" + j;
                    console.log(idAcademy);
                    $(`*[id="${idAcademy}"]`).prop('checked', false);
                })
                $(".close").click()
                Swal.fire(
                    'Great!',
                    'Your project has been successfully created!',
                    'success'
                ).then((result) => {
                    window.location.href = '{{ route('projects.index') }}';
                });
            },
            error: function(error) {
                console.log(error.responseJSON.errors);
                let new_description = error.responseJSON.errors.new_description
                let new_project = error.responseJSON.errors.new_project
                let academy = error.responseJSON.errors.academy
                $('#name_project').html('')
                $('#project_description').html('')
                $('#academy_error').html('')
                if (new_project) {
                    new_project.forEach(el => {
                        let name_project_msg =
                            `<span class="color-dark-yelow font-weight-bold d d-block">${el}</span>`
                        $('#name_project').append(name_project_msg)
                    });
                }
                if (new_description) {
                    new_description.forEach(elem => {
                        let new_description_msg =
                            `<span class="color-dark-yelow font-weight-bold d-block">${elem}</span>`
                        $('#project_description').append(new_description_msg)
                    });
                }
                if (academy) {
                    academy.forEach(elem => {
                        let new_academy_msg =
                            `<span class="color-dark-yelow font-weight-bold d d-block">${elem}</span>`
                        $('#academy_error').append(new_academy_msg)
                    });
                }

            }
        });
    });

    $("body").on("click", "#delete_new_project", function(e) {
        e.preventDefault();
        var id = $(this).data("id");
        // var id = $(this).attr('data-id');
        var token = $("meta[name='csrf-token']").attr("content");
        var url = e.target;
        $.ajax({
            url: url.href,
            type: 'DELETE',
            data: {
                _token: token,
                id: id,
            },
            dataType: "json",
            enctype: "multipart/form-data",
            success: function(response) {
                $(".close").click()
                let projectName = response.success.name
                Swal.fire(
                    'Great!',
                    `${projectName} has been successfully created!`,
                    'success'
                ).then((result) => {
                    location.reload();
                });
            }
        });
        return false;
    });
    //edit project
    $("#edit_new_project").click(function() {
        var id = $('#edit_new_project').data("id");
        console.log(id);
        var academies = [];
        $('input[name=academy]:checked').map(function() {
            academies.push($(this).val());
        });
        var url = "{{ route('projects.update', ':id') }}";
        url = url.replace(':id', id);
        console.log(url)
        let data = {
            '_token': '{{ csrf_token() }}',
            new_project: $('#new_project').val(),
            new_description: $('#new_description').val(),
            academy: academies,
            id: id

        };
        console.log(data);
        $.ajax({
            type: 'PUT',
            url: url,
            data: data,
            dataType: "json",
            enctype: "multipart/form-data",
            success: function(data) {
                console.log(data)
                $('#name_project').html('')
                $('#project_description').html('')
                $('#academy_error').html('')
                $('#new_description').val('')
                $('#new_project').val('')
                $(".close").click()
                Swal.fire(
                    'Great!',
                    'Your project has been successfully created!',
                    'success'
                ).then((result) => {
                    window.location.href = '{{ route('projects.index') }}';
                });
            },
            error: function(error) {
                console.log(error.responseJSON.errors);
                let new_description = error.responseJSON.errors.new_description
                let new_project = error.responseJSON.errors.new_project
                let academy = error.responseJSON.errors.academy
                $('#name_project').html('')
                $('#project_description').html('')
                $('#academy_error').html('')
                if (new_project) {
                    new_project.forEach(el => {
                        let name_project_msg =
                            `<span class="color-dark-yelow font-weight-bold  d-block">${el}</span>`
                        $('#name_project').append(name_project_msg)
                    });
                }
                if (new_description) {
                    new_description.forEach(elem => {
                        let new_description_msg =
                            `<span class="color-dark-yelow font-weight-bold  d-block">${elem}</span>`
                        $('#project_description').append(new_description_msg)
                    });
                }
                if (academy) {
                    academy.forEach(elem => {
                        let new_academy_msg =
                            `<span class="color-dark-yelow font-weight-bold d-block">${elem}</span>`
                        $('#academy_error').append(new_academy_msg)
                    });
                }

            }
        });
    });

    $('button[id^="apply_new_project"]').click(function() {
        console.log('tuka')
        var projectId = $(this).attr("data-newproject");
        let data = {
            '_token': '{{ csrf_token() }}',
            project_id: projectId,
            new_description: $(`#new_description`+projectId).val(),
        };
        console.log(data)
        $.ajax({
            type: 'POST',
            url: '{{ route('dashboards.store') }}',
            data: data,
            dataType: "json",
            enctype: "multipart/form-data",
            success: function(data) {
                console.log(data)
                $('#project_description').html('')
                $('#new_description').val('')
                //for uncheck academies
                let projectName = data.success.name
                $(".close").click()
                Swal.fire(
                    'Great!',
                    `Your successfully applied for ${projectName}!`,
                    'success'
                ).then((result) => {
                    location.reload();
                });
            },
            error: function(error) {
                console.log(error.responseJSON.errors);
                let new_description = error.responseJSON.errors.new_description
                $('#project_description').html('')
                if (new_description) {
                    new_description.forEach(elem => {
                        let new_description_msg =
                            `<span class="color-dark-yelow font-weight-bold  d-block">${elem}</span>`
                        $('#project_description'+projectId).append(new_description_msg)
                    });
                }
            }
        });
    });
    $('button[id^="add_new_applicant"]').click(function () {
        var id = $(this).data("id");
        console.log(id);
        var url = "{{ route('aplicants.update', ':id') }}";
        url = url.replace(':id', id);
        console.log(url)
        let data = {
            '_token': '{{ csrf_token() }}',
            id: id
        };
        console.log(data);
        $.ajax({
            type: 'PUT',
            url: url,
            data: data,
            dataType: "json",
            enctype: "multipart/form-data",
            success: function(data) {
                console.log(data)
                Swal.fire(
                    'Great!',
                    'This applicant has been successfully added',
                    'success'
                ).then((result) => {
                    location.reload();
                });
            },
            error: function(error) {
                console.log(error.responseJSON.errors);
            }
        });
    });

    $('button[id^="team_assambled"]').click(function () {
        var id = $(this).data("project");
        console.log(id);
        var url = "{{ route('dashboards.update', ':id') }}";
        url = url.replace(':id', id);
        console.log(url)
        let data = {
            '_token': '{{ csrf_token() }}',
            id: id
        };
        console.log(data);
        $.ajax({
            type: 'PUT',
            url: url,
            data: data,
            dataType: "json",
            enctype: "multipart/form-data",
            success: function(data) {
                let errors = data.errors;
                console.log(data.errors)
                if(data.success) {
                Swal.fire(
                    'Great!',
                    'Team has been successfully assembled for this project',
                    'success'
                ).then((result) => {
                    location.reload();
                });
            }
            if(data.errors) {
                Swal.fire({
              title: 'Team is already assembled',
              icon: 'warning',
                }).then((result) => {
                    location.reload();
                });

            }},
            error: function(error) {
                // console.log(error.responseJSON.errors);
                Swal.fire(
                    'Great!',
                    'Team is already assembled',
                    'success'
                ).then((result) => {
                    location.reload();
                });
            }
        });
    });
    $("body").on("click", "#cancel_application", function(e) {
        e.preventDefault();
        var id = $(this).data("id");
        console.log(id)
        var token = $("meta[name='csrf-token']").attr("content");
        var url = e.target;
        console.log(url.href);
        $.ajax({
            url: url.href,
            type: 'DELETE',
            data: {
                _token: token,
                id: id,
            },
            dataType: "json",
            enctype: "multipart/form-data",
            success: function(response) {
                $(".close").click()
                let projectName = response.success.name
                Swal.fire(
                    'Great!',
                    `You successfully canceled application for ${projectName}`,
                    'success'
                ).then((result) => {
                    location.reload();
                });
            }
        });
        return false;
    });
    $('#back').click(function(e) {
        e.preventDefault()
        history.go(-1);
    })
</script>
