
<div class="container-fluid navbar-bottom-collor-dark-yellow">
  <div class="row bg-light">
    <div class="col">
        <nav class="navbar navbar-expand-lg navbar-light px-5">
          <h1 class="navbar-brand font-weight-bolder"><span class="text-color-dark-green">BRAINSTER</span><span class="text-muted">PRENEURS</span></h1>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav ml-auto">
                {{-- @if (Route::currentRouteName() == 'users.index') --}}
                @if (Auth::user()->profile_complete > 0 || Route::currentRouteName() == 'users.index' || Route::currentRouteName() == 'projects.index')
                <li class="nav-item font-weight-bold">
                <a class="nav-link {{ Route::currentRouteName() == 'projects.index' || Route::currentRouteName() == 'projects.create' ? 'text-muted' : 'text-dark' }}" href="{{route('projects.index')}}">My Project</a>
                </li>
                <li class="nav-item font-weight-bold">
                    <a class="nav-link {{ Route::currentRouteName() == 'aplicants.index' ? 'text-muted' : 'text-dark' }}" href="{{route('aplicants.index')}}">My Applications</a>
                </li>
                <li class="nav-item font-weight-bold">
                    <a class="nav-link {{ Route::currentRouteName() == 'users.index' ? 'text-muted' : 'text-dark' }}" href="{{route('users.index')}}">My Profile</a>
                </li>
                @endif     
                <li class="nav-item font-weight-bold avatar-navbar">
                    <a href="{{route('users.index')}}"><img style="width:50px !important; height:50px !important" class="rounded-circle"
                      src="{{ Auth::user()->getUserAvatar() }}" alt="avatar"></a>
                    </li>
                      <li class="nav-item dropdown"> 
                      <div class="dropdown-toggle px-2" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      </div>
                      <div class="dropdown-menu dropdown-menu-right text-center" aria-labelledby="navbarDropdown">
                        <div class="dropdown-item color-dark-yelow font-weight-bold" href="#">{{Auth::user()->name. ' '.Auth::user()->surname}}</div>
                        <a href="#passwordModal" data-toggle="modal" data-target="#passwordModal" class="bg-color-dark-green btn text-light"><i class="bg-color-dark-green text-light" style="width: 200px"></i>Change Password</a>
                        @if (null !== auth::user())
                        <div class="dropdown-item" href="#">
                        <button onclick="event.preventDefault(); 
                        document.getElementById('logout-form').submit();" 
                                class="btn bg-color-dark-grey text-light" style="width: 150px">Logout</button>
                      <form id="logout-form" action="{{ route('logout') }}" method="GET" style="display: none;">
                      {{ csrf_field() }}
                                </form>
                            </div>
                        @endif
                      </div>
                    </li>
                </ul>
            </nav>
        </div>
    </div>   
</div>

<div class="modal inmodal fade modal-custom" id="passwordModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header text-center">
              <h4 class="modal-title color-dark-green"> <i class="pe-7s-key btn-icon-wrapper"></i>Change password</h4>
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
              class="sr-only">Close</span></button>
          </div>
          <div class="modal-body">
              <form id="change_password_form" data-parsley-validate>
                  @method('PUT')
                  {{ csrf_field() }}
                  <div class="col-lg-12">
                      <div class="position-relative form-group">
                          <label class="color-dark-green">Enter old password</label>
                          <input type="password" name="current_password" id="old_password"
                              class="form-control">
                          <span toggle="#old_password" id="password-toggle" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                          
                      </div>
                      <div id="current_password">
                      </div>
                      <div class="position-relative form-group">
                          <label class="color-dark-green">Enter new password</label>
                          <input type="password" name="new_password" id="new_password" class="form-control">
                          <div class="input-group-addon">
                          <span toggle="#new_password" id="password-toggle" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                          </div>
                          <div id="new_pass" class="mb-3">
                          </div>
                      </div>
                  </div>
              </form>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn bg-color-dark-yellow btn-lg" data-dismiss="modal">Close</button>
              <button type="submit" id="submit-reset-pass" class="btn bg-color-dark-green btn-lg text-light">Save
                  changes</button>
          </div>
      </div>
  </div>
</div>

