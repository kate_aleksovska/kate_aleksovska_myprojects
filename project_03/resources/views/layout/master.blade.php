<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    @include('layout.head')

    <!-- Bootstrap CSS -->
    @include('layout.style')
    <title>@yield('title')</title>
</head>

<body class="bg-grey-light">

  @auth
    @include('layout.navbar')
  @endauth

    @yield('content')

    {{-- @include('layout.footer') --}}
    @include('layout.scripts')
</body>

</html>
