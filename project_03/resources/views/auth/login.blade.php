@extends('layout.master')
@section('title', 'Login')
@section('content')
    <div class="container-fluid vh-100 bg-image-login">
        <div class="row justify-content-center align-items-center">
            <div class="col-6 offset-1 d-flex align-items-center">
                <div>
                    <h1>BRAINSTER<span class="text-muted">PRENEURS</span></h1>
                    <h3 class="mt-5">Prepel your ideas to life!</h3>
                </div>
            </div>
        <div class="col-5">
            <div class="row justify-content-center align-items-center vh-100">
                <div class="col-6">
                    <h1 class="mt-5">Login</h1>
                    <form method="POST" action="{{ route('login') }}" novalidate>
                        @csrf
                        <!-- Email Address -->
                        <div class="row">
                            <div class="col-12 mb-3">

                                <input id="email" type="email" name="email" value="{{ old('email') }}"
                                    class="input-register form-control @error('email') is-invalid @enderror"
                                    placeholder="Email" />
                                @error('email')
                                    <small class="text-danger invalid-feedback">{{ $message }}</small>
                                @enderror
                            </div>
                            <!-- Password -->
                            <div class="col-12 mb-3">
                                <input id="password" type="password" name="password" class="input-register form-control"
                                    placeholder="Password" autocomplete="new-password" />
                                <span toggle="#password" id="password-toggle"
                                    class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                @error('password')
                                    <small class="text-danger ivalid-feedback">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>


                        <!-- Remember Me -->
                        <div class="block mt-4">
                            <label for="remember_me" class="inline-flex items-center">
                                <input id="remember_me" type="checkbox"
                                    class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                                    name="remember">
                                <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                            </label>
                        </div>
                        <button class="ml-4 btn bg-color-dark-yellow text-white btn-lg px-5">
                            {{ __('Log in') }}
                        </button>
                        <div class="flex items-center justify-end mt-4">
                            @if (Route::has('password.request'))
                                <a class="underline text-sm text-gray-600 hover:text-gray-900"
                                    href="{{ route('password.request') }}">
                                    {{ __('Forgot your password?') }}
                                </a>
                            @endif
                            @if (Route::has('login'))
                                @if (Route::has('register'))
                                    <p class="text-muted"><i>Don't have an account, register<a
                                                href="{{ route('register') }}"
                                                class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">here</a></i>
                                    </p>
                                @endif
                        </div>
                        @endif

                </div>
                </form>
            </div>

        </div>
    </div>
    </div>
    </div>

@endsection
