@extends('layout.master')
@section('title', 'Register')
@section('content')
    <div class="container-fluid d-flex flex-column justify-content-center vh-100 bg-image-register">
        <div class="row mx-5">
            <div class="col-12 my-4">
                <h1>Register</h1>
            </div>
            <div class="col-5">
                <form method="POST" action="{{ route('register') }}" novalidate>
                    @csrf
                    <!-- Name -->
                    <div class="row mb-5">
                        <div class="col-6">
                            <input id="name" type="text" name="name" value="{{ old('name') }}" autofocus
                                class="input-register form-control @error('name') is-invalid @enderror" placeholder="Name" />
                            @error('name')
                                <small class="color-dark-yelow font-weight-bold">{{ $message }}</small>
                            @enderror
                        </div>
                        <!-- Surname -->
                        <div class="col-6">

                            <input id="surname" type="text" name="surname" value="{{ old('surname') }}"
                                class="input-register form-control @error('surname') is-invalid @enderror"
                                placeholder="Surname" autofocus />
                            @error('surname')
                                <small class="color-dark-yelow font-weight-bold">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    <!-- Email Address -->
                    <div class="row mb-5">
                        <div class="col-6">

                            <input id="email" type="email" name="email" value="{{ old('email') }}"
                                class="input-register form-control @error('email') is-invalid @enderror"
                                placeholder="Email" />
                            @error('email')
                                <small class="color-dark-yelow font-weight-bold">{{ $message }}</small>
                            @enderror
                        </div>
                        <!-- Password -->
                        <div class="col-6">
                            <input id="password" type="password" name="password" class="input-register form-control"
                                placeholder="Password" autocomplete="new-password" />
                            <span toggle="#password" id="password-toggle"
                                class="fa fa-fw fa-eye field-icon toggle-password"></span>
                            @error('password')
                                <small class="color-dark-yelow font-weight-bold">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>


                    <div class="row">
                        <div class="form-group text-muted col-12">
                            <label>Biography</label>
                            <textarea name="biography" 
                                class="form-control text-left @error('biography') is-invalid @enderror"
                                rows="8">{{ old('biography') }}</textarea>
                            @error('biography')
                                <small class="color-dark-yelow font-weight-bold">{{ $message }}</small>
                            @enderror
                        </div>

                    </div>
                    <a class="color-dark-yelow font-weight-bold text-sm" href="{{ route('login') }}">
                        {{ __('Already registered?') }}
                    </a>
                    <button class="ml-4 btn bg-color-dark-green text-white btn-lg px-5">
                        {{ __('Register') }}
                    </button>
            </div>
            </form>
        </div>
    </div>
    </div>
@endsection
