@extends('layout.master')
@section('title', 'Project Applicants')
@section('content')
    <div class="container-fluid p-5 bg-light height-user-show">
        <div class="row">
            <div class="col-5 px-5">
                <div class="row">
                    <div class="col-6">
                        <img src="{{$applicant->getUserAvatar()}}" class="rounded-circle border-rounded-circle" style="width:300px !important; height:300px !important" /> 
                    </div>
                    <div class="col-6 py-5">
                        <h4 class="text-muted textfont-weight-bold">Name:</h4>
                        <h2 class="textfont-weight-bold">{{$applicant->name.' '.$applicant->surname}}</h2>
                        <h4 class="text-muted textfont-weight-bold">Contact:</h4>
                        <h2 class="textfont-eight-bold">{{$applicant->email}}</h2>
                    </div>
                </div>
                
            </div>
            <div class="col-7 p-5">
              <div class="row">
                  <div class="col-12 height-50">
                      <h3 class="text-muted ">
                        Biography:
                      </h3>
                      <div class="scroll-applicant">
                           <p class="addReadMore showlesscontent ">{{$applicant->biography}}</p>
                      </div>
                      
                  </div>
                  <div class="col-12 height-50">
                    <h3 class="text-muted">
                      Skills:
                    </h3>
                    <div class="row">
                        @foreach ($applicant->skills as $skill)
                        <div class="square bg-white m-2 text-center d-flex justify-content-center align-items-center">
                            <p>{{$skill->name}}</p>
                           
                        </div>
                        @endforeach
                    </div>
                   
                </div>
              </div>
            </div>
        </div>
        <div class="row">
            <div class="col text-left">
                <button id="back" type="button" class="btn btn-lg bg-color-dark-green text-light" id="submit_new_project" style="width:200px">Back</button>
            </div>
        </div>
    </div>
@endsection
@section('script')
@endsection