@extends('layout.master')
@section('title', 'View Applicantions')
@section('content')
<div class="container-fluid px-5">
    <div class="row flex-column">
        <h1>My applications</h1>
        @if(count($applicants) == 0)
            <h3 class="px-5 py-3">You haven't applayed anywhere yet</h3>
        @endif
    </div>
    @include('card-project')
</div>
@endsection
@section('script')
@endsection