@extends('layout.master')
@section('title', 'View Applicants')
@section('content')
    <div class="container-fluid bg-image-applicants">
        @include('welcome')
        <div class="row">
            <div class="col-6 px-5">
                <h1>{{ $project->name }}-Applicants</h1>
            </div>
            @if ($project->applicants->count() > 0)
                <div class="col-6 d-flex flex-column justify-content-end text-center px-5">
                    <form data-parsley-validate class="text-center">
                        {{ csrf_field() }}
                        @method('PUT')
                        <small class="d-block">Ready to start</small>
                        <small class="d-block">Click on the button bellow</small>
                        <button type="button" class="text-white btn bg-color-dark-yellow btn-inline-block"
                            id="team_assambled" data-project="{{ $project->id }}">TEAM ASEMPLED<i
                                class="fas fa-check"></i></button>
                    </form>
                </div>
            @endif
        </div>
        <div class="row px-5">
            <h3>Choose your teammates</h3><img src="{{ URL::to('./icons/4.png') }} " alt="arrow-right"
                class="arrow">
        </div>
        <div class="row align-items-center">
            @if ($project->applicants->count() == 0)
                <h1>There are 0 applicants</h1>
            @else
                <div class="container">
                    <div class="row">
                        <div class="col scroll-x">
                            @foreach ($project->applicants as $aplicants)
                                <div class="card m-3 @if ($aplicants->status > 0) bg-color-dark-grey @endif"
                                    style="width: 18rem;">
                                    <form id="add_applicant" data-parsley-validate class="text-center">
                                        {{ csrf_field() }}
                                        @method('PUT')
                                        <a href="{{ route('users.show', $aplicants->user->id) }}"><img
                                                src="{{ $aplicants->user->getUserAvatar() }}"
                                                class="rounded-circle border-rounded-circle img-position"
                                                style="height:170px" /></a>
                                        <div class="card-body">
                                            <p class="font-weight-bold">
                                                {{ $aplicants->user->name . ' ' . $aplicants->user->surname }}</p>
                                            <p class="color-dark-yelow">{{ $aplicants->user->academy->profession }}</p>
                                            <small class="text-muted">{{ $aplicants->user->email }}</small>
                                            <p class="card-text">{{ $aplicants->message }}</p>
                                            @if ($project->is_assebled == 0)
                                                @if ($aplicants->status == 0)
                                                    <button type="button" class="btn"
                                                        id="add_new_applicant{{ $aplicants->id }}"
                                                        data-id="{{ $aplicants->id }}"><i
                                                            class="fas fa-plus"></i></button>
                                                @else
                                                    <button type="button" class="btn"
                                                        @disabled(true)><i class="fas fa-plus"></i></button>
                                                @endif
                                            @else
                                                <button type="button" class="btn" @disabled(true)><i
                                                        class="fas fa-plus"></i></button>
                                            @endif
                                        </div>
                                    </form>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
@section('script')
@endsection
