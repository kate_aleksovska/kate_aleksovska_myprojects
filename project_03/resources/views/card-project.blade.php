
  <div class="container">
      <div class="row" id="scroll-hidden">
          <div class="col scroll">
           @if (Route::currentRouteName() == 'dashboards.index' || Route::currentRouteName() == 'projects.index')

           @if (count($projects))
           @foreach ($projects as $project)
         <div class="myDIV  @if($project->is_assebled > 0 && Route::currentRouteName() == 'dashboards.index') d-none @else d-block @endif">
               <div class="row">
                   <div class="@if (Route::currentRouteName() == 'dashboards.index') col-12 @else col-10 @endif">
                       <div class="jumbotron jumbotron-fluid pb-0 pt-1 @foreach ($project->academy as $academy) academies_project{{$academy->id}} @endforeach" id="attrText" @foreach ($project->academy as $academy) data-academies_project{{$academy->id}} = "academies_project{{$academy->id}}" @endforeach>
                           <div class="container">
                               <div class="row">
                                   <div class="col-4 align-left-part-jumbotron">
                                       <div class="row mx-3 align-items-stretch flex-column justify-content-between">
                                           <div class="col-12 d-flex align-items-center flex-column img-position">
                                               <img src="{{ $project->user->getUserAvatar() }}" class="rounded-circle border-rounded-circle " style="height:170px" />
                                               <h4>{{ $project->user->name . ' ' . $project->user->surname }}</h4>
                                               <p class="color-dark-yelow">I'm {{ $project->user->academy->profession }}</p>
                                           </div>   
                                           <div class="row align-items-stretch mt-5">
                                               <p class="col-12 text-center pb-0 mb-0 mt-5 academy-font-size">I'm looking for</p>
                                               @foreach ($project->academy as $academy)
                                               <div class="half-circle col-4 text-center d-flex justify-content-center align-items-center d-inline-block">
                                                   <p class="academy-font-size text-wrap text-white p-0 m-0" id="{{ $academy->id }}">{{ $academy->name }}</p>
                                               </div>
                                               @endforeach
                                           </div>
                                       </div>
                                   </div>
                                   <div class="col-8">
                                       <div class="row justify-content-between pr-5">
                                           <span class="h1">{{ $project->name }}</span> <a class="rounded-circle bg-color-dark-green text-white text-center p-2 applicant-position" @if ($project->user_id == Auth::user()->id)
                                               href="{{ route('dashboards.show', $project->id) }}" data-toggle="tooltip" data-placement="top" title="View your applicants"
                                           @else
                                            data-toggle="tooltip" data-placement="top" title="This is not your project"
                                           @endif  style="height:70px; width:70px; cursor: pointer;">{{$project->applicants->count()}} <p class="academy-font-size">Applicants</p></a>
                                       </div>
                                      
                                       <p class="addReadMore showlesscontent">{{ $project->description }}</p>
                                       <div class="text-right {{ Route::currentRouteName() == 'dashboards.index' && $project->user->id != Auth::user()->id ? 'd-block' : 'd-none' }}">
                                       <button type="button" class="btn bg-color-dark-green text-white px-5" @if (Auth::user()->profile_complete == 0)
                                           disabled
                                       @endif @foreach ($project->applicants as $applicant)
                                           @if ($applicant->user->id == Auth::user()->id)
                                              disabled 
                                           @endif
                                       @endforeach
                                       data-toggle="modal" data-target="#exampleModalApply{{ $project->id }}">
                                           I'M IN
                                       </button>
                                      </div>
                                      
                                   </div>
                                   @if ($project->is_assebled > 0 && Route::currentRouteName() == 'projects.index')
                                   <div class="text-right badge">
                                       <img src="{{ URL::to('./icons/badge.png') }}" alt="" style="width: 40px; height:60px" class="">
                                   </div>
                                   @endif
                               </div>
                           </div>
                       </div>
                   </div>
                   
                       @if ($project->user->id == Auth::user()->id && Auth::user()->profile_complete > 0 && Route::currentRouteName() == 'projects.index' )
                       <div class="col-1 px-1 d-flex justify-content-end flex-column mx-1 py-5">
                       <a href="{{ route('projects.edit', $project->id) }}" class="btn hideButtons my-2">
                           <img src="{{ URL::to('./icons/8.png') }}" alt="" style="height:30px; width:30px">
                       </a>
                       <button type="button" class="btn hideButtons" data-toggle="modal"
                           data-target="#exampleModal{{ $project->id }}">
                           <img src="{{ URL::to('./icons/7.png') }}" alt="" style="height:30px; width:30px">
                       </button>
                    </div>
                   @endif
                   
               </div>
           </div>
               
               <!-- Modal -->
               <div class="modal fade" id="exampleModal{{ $project->id }}" tabindex="-1"
                   aria-labelledby="exampleModalLabel" aria-hidden="true">
                   <div class="modal-dialog">
                       <div class="modal-content">
                           <div class="modal-header">
                               <h5 class="modal-title" id="exampleModalLabel">Delete project
                                   {{ $project->name }}</h5>
                               <button type="button" class="close" data-dismiss="modal"
                                   aria-label="Close">
                                   <span aria-hidden="true">&times;</span>
                               </button>
                           </div>
                           <div class="modal-body">
                               Do you want to delete project with name {{ $project->name }}
                           </div>
                           <div class="modal-footer">
                               <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                               <a href="{{ route('projects.destroy', $project->id) }}" class="btn btn-warning"
                                   id="delete_new_project" data-id="{{ $project->id }}">
                                   Delete
                               </a>
                           </div>
                       </div>
                   </div>
               </div>

               <!-- Modal -->
               <div class="modal fade" id="exampleModalApply{{ $project->id }}" tabindex="-1"
                   aria-labelledby="exampleModalLabel" aria-hidden="true">
                   <div class="modal-dialog">
                       <div class="modal-content">
                           <div class="modal-header">
                               <h5 class="modal-title" id="exampleModalLabel">Apply for project project
                                   {{ $project->name }}</h5>
                               <button type="button" class="close" data-dismiss="modal"
                                   aria-label="Close">
                                   <span aria-hidden="true">&times;</span>
                               </button>
                           </div>
                           <div class="modal-body">
                               <form id="apply_for_new_project" data-parsley-validate>
                                   @method('PUT')
                                   {{ csrf_field() }}
                               <div class="position-relative form-group">
                                       <label>Why you want to join in realization of new genious idea</label>
                                       <textarea name="new_description" class="form-control text-left" rows="6"
                                           id="new_description{{ $project->id }}">{{ old('new_description') }}</textarea>
                                       <div id="project_description{{ $project->id }}" class="mb-3">
                                       </div>
                               </div>
                           </form>
                           </div>
                           <div class="modal-footer">
                               <button type="button" class="btn bg-color-dark-yellow text-white" data-dismiss="modal">Close</button>
                               <button class="btn bg-color-dark-green text-white"
                                   id="apply_new_project" data-newproject="{{ $project->id }}">
                                   Apply
                               </button>
                           </div>
                       </div>
                   </div>
               </div>
           @endforeach
       @endif
               
           @endif
            @if (Route::currentRouteName() == 'aplicants.index')
            @if (count($applicants))
            @foreach ($applicants as $applicant)
          <div class="myDIV  @if($applicant->project->is_assebled > 0 && Route::currentRouteName() == 'dashboards.index') d-none @else d-block @endif">
                <div class="row">
                    <div class="col-10">  
                        <div class="jumbotron jumbotron-fluid pb-0 pt-1 @foreach ($applicant->project->academy as $academy) academies_project{{$academy->id}} @endforeach" id="attrText" @foreach ($applicant->project->academy as $academy) data-academies_project{{$academy->id}} = "academies_project{{$academy->id}}" @endforeach>
                            <div class="container">
                                <div class="row">
                                    <div class="col-4 align-left-part-jumbotron">
                                        <div class="row mx-3 align-items-stretch flex-column justify-content-between">
                                            <div class="col-12 d-flex align-items-center flex-column img-position">
                                                <img src="{{ $applicant->project->user->getUserAvatar() }}" class="rounded-circle border-rounded-circle " style="height:170px" />
                                                <h4>{{ $applicant->project->user->name . ' ' . $applicant->project->user->surname }}</h4>
                                                <p class="color-dark-yelow">I'm {{ $applicant->project->user->academy->profession }}</p>
                                            </div>
                                           
                                            <div class="row align-items-stretch mt-5">
                                                <p class="col-12 text-center pb-0 mb-0 mt-5 academy-font-size">I'm looking for</p>
                                                @foreach ($applicant->project->academy as $academy)
                                                <div class="half-circle col-4 text-center d-flex justify-content-center align-items-center d-inline-block">
                                                    <p class="academy-font-size text-wrap text-white p-0 m-0" id="{{ $academy->id }}">{{ $academy->name }}</p>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-8">
                                        <div class="row justify-content-between pr-5">
                                            <span class="h1">{{ $applicant->project->name }}</span> <a class="rounded-circle bg-color-dark-green text-white text-center p-2 applicant-position" @if ($applicant->project->user_id == Auth::user()->id)
                                                href="{{ route('dashboards.show', $applicant->project->id) }}" data-toggle="tooltip" data-placement="top" title="View your applicants"
                                            @else
                                             data-toggle="tooltip" data-placement="top" title="This is not your project"
                                            @endif  style="height:70px; width:70px; cursor: pointer;">{{$applicant->project->applicants->count()}} <p class="academy-font-size">Applicants</p></a>
                                        </div>
                                        <p class="addReadMore showlesscontent">{{ $applicant->project->description }}</p>
                                        @if ($applicant->project->is_assebled > 0 && $applicant->status == 0)
                                               <h5 class="color-dark-red">Application Denied <img src="{{ URL::to('./icons/6.png') }}" alt="" style="height:30px; width:30px"></h5>
                                        @endif 
                                        @if ($applicant->status > 0)
                                               <h5 class="color-dark-green">Aplication accepted <img src="{{ URL::to('./icons/5.png') }}" alt="" style="height:30px; width:30px"></h5>
                                        @endif  
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-1 px-1 d-flex justify-content-center flex-column mx-1 py-5">
                        @if ($applicant->user_id == Auth::user()->id && Auth::user()->profile_complete > 0 && $applicant->project->is_assebled == 0)
                        <button type="button" class="btn btn-outline-light hideButtons text-secondary" data-toggle="modal"
                            data-target="#exampleModalCancel{{ $applicant->project->id }}">
                           <img src="{{ URL::to('./icons/2.png') }}" alt="" style="height:30px; width:30px">
                           Cancel
                        </button>
                      @endif
                    </div>
                </div>
          </div>
          <div class="modal fade" id="exampleModalCancel{{ $applicant->project->id }}" tabindex="-1"
            aria-labelledby="exampleModalLabelCancel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabelCancel">Canceling application for
                            {{ $applicant->project->name }}</h5>
                        <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Do you want to cancel the application for {{ $applicant->project->name }}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <a href="{{ route('aplicants.destroy', $applicant->id) }}" class="btn btn-warning"
                            id="cancel_application" data-id="{{ $applicant->id }}">
                            Cancel
                        </a>
                    </div>
                </div>
            </div>
        </div>
            @endforeach
            @endif
            @endif
          </div>
    </div>
</div>
