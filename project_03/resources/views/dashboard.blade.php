@extends('layout.master')
@section('title', 'Dashboard')
@section('content')
<div class="col-sm-6 offset-sm-3 col-12">
    @if (Session::has('success'))
        <div class="alert alert-success text-center">
            {{ Session::get('success') }}
        </div>
    @endif
    @if (Session::has('error'))
        <div class="alert alert-danger text-center">
            {{ Session::get('error') }}
        </div>
    @endif
</div>
    <div class="container-fluid bg-image-dashboard @if (Auth::user()->profile_complete == 0) 'height-with-welcome' @else 'heigh-withouth-welcome' @endif">
        <div class="row">
            
        @include('welcome')
        <div class="row" >
            <div class="col-4 pl-5" style="width:600px!important">
                <h1 class="pb-5">In what field you can be amazing?</h1>
                <div class="form-group row justify-content-around align-items-center">
                <label class="custom-checkbox text-center m-1 form-check-label"><input type="checkbox" name="academy[]" id="academies_project0"><div class="checkmark d-flex justify-content-center align-items-center font-weight-bold"> all</div></label>
                @foreach ($academies as $academy)
                <label class="custom-checkbox text-center m-1 form-check-label"><input type="checkbox" name="academy[]"
                            id="academies_project{{ $academy->id }}"><div class="checkmark d-flex justify-content-center align-items-center font-weight-bold">{{ $academy->name }}</div></label>
                @endforeach
                </div>
            </div>
            <div class="col-8 f-flex align-items-center text-center">
                <div class="row">
                    <div class="col-9 d-flex justify-content-end py-2">
                        <img src="../icons/3.png" alt="" srcset="" class="arrow">
                        <h1>Checkout the latiest projects</h1>
                    </div>
                </div>
                @include('card-project')
            </div>
        </div>
    @endsection
    @section('script')
        <script>
            var academies = <?php echo json_encode($academies); ?>;
        </script>
    @endsection
