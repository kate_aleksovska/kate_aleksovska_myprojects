@if (Auth::user()->profile_complete == 0)
<div class="container-fluid bg-light m-0 p-0">
    <div class="container bg-white">
    <div class="row m-0 p-0">
        <div class="col text-center m-0 p-0">
                <h1 class="m-0 p-0">Welcome</h1>
                <p class="m-0 p-0"></p>Please finish up your profile on the folowing <a href="{{route('users.index')}}">link</a>, so that you can enjoy all our features</p>
        </div>
    </div>
</div>
</div>
@endif
