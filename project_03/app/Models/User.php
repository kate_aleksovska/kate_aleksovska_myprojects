<?php

namespace App\Models;

use App\Models\Skill;
use App\Models\Academy;
use App\Models\Project;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'surname',
        'email',
        'password',
        'biography',
        'avatar',
        'profile_complete',
        'academy_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
     /**
     * Get the user's picture.
     *
     * @param  string  $value
     * @return string
     */
    public function getUserAvatar()
    {
        return !blank($this->avatar)
            ? Storage::url($this->avatar)
            : asset('/images/avatar.jpg');
    }
    public function skills()
    {
        return $this->belongsToMany(Skill::class);
    }
    public function projects()
    {
        return $this->hasMany(Project::class);
    }
    public function academy()
    {
    //    return Academy::find(Auth::user()->academy_id);
         return $this->belongsTo(Academy::class);
    }
}
