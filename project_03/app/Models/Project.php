<?php

namespace App\Models;

use App\Models\User;
use App\Models\Academy;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Project extends Model
{
    use HasApiTokens, HasFactory, Notifiable;
   /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
   protected $fillable = [
       'name',
       'description',
       'user_id',
       'is_assemled'
   ];
   /**
     * Get the user's picture.
     *
     * @param  string  $value
     * @return string
     */
     public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function academy()
    {
      return $this->belongsToMany(Academy::class);
    }
    public function applicants()
    {
      return $this->HasMany(Aplicants::class);
    }
}
