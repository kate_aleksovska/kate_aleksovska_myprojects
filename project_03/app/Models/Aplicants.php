<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
class Aplicants extends Model
{
    use HasApiTokens, HasFactory, Notifiable;
   /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
   protected $fillable = [
       'user_id',
       'project_id',
       'message',
       'status',
   ];


   public function project()
   {
    return $this->belongsTo(Project::class);
   }
   public function user()
   {
    return $this->belongsTo(User::class);
   }
   
}
