<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\ChangePasswordRequest;

class ChangePasswordController extends Controller
{
    public function update(ChangePasswordRequest $request)
    { 
        $success = auth()->user()->update(['password'=> Hash::make($request->new_password)]);
        return response()->json(['success'=>$success], $success ? 200 : 500);   
    }
}
