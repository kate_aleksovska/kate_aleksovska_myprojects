<?php

namespace App\Http\Controllers;

use App\Http\Requests\ApplyRequest;
use App\Models\Academy;
use App\Models\Aplicants;
use App\Models\User;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {    
        $aplicants = Aplicants::all();
        $projects = Project::all();
        $academies = Academy::all();
        return view('dashboard', compact('projects', 'academies', 'aplicants'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ApplyRequest $request)
    {
        $apply_project = $request->toArray();
        $apply_project['user_id'] = Auth::user()->id;
        $apply_project['project_id'] = $request->project_id;
        $apply_project['message'] = $request->new_description;
        $success = Aplicants::create($apply_project);
        
       $subject_mail = Auth::user()->name.' '.Auth::user()->surname.' has applyed on your project'.' '.$success->project->name;
       $message_mail = "Hello, ".Auth::user()->name.' '.'has applyed on your project'.' '.$success->project->name;
       $message_mail .="His/Her skills are: ";

       foreach(Auth::user()->skills as $skill) {
        $message_mail .= $skill->name.', ';
       }
       $message_mail .="\r\n"."The message of the applicant is: ".$success->message.'.'."\r\n";
       $message_mail .= "Thank you for your trust,
        
       Yours Brainster";
        $to      = $success->project->user->email;  
        $subject = $subject_mail;
        $message = $message_mail;
        $headers = 'From: brainstertest@gmail.com' . "\r\n";
        if(mail($to, $subject, $message, $headers)) {
            return response()->json(['success'=>$success->project], $success ? 200 : 500); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Project::find($id);
        return view('applicants.view-project-applicants', compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $project = Project::find($id);
       if($project->is_assebled == 0) {
        foreach($project->applicants as $applicant) {
            if($applicant->status == 1) {
                $project->is_assebled = 1;
            }
        }
        
       if($project->save()) {        
        return response()->json(['success'=> $project], $project ? 200 : 500); 
         
       }else {
        return response()->json(['errors'=> $project], $project ? 200 : 500);
       }

    } else {
        return response()->json(['errors'=> 'project is already assembled']);  
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
