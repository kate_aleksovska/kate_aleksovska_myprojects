<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Aplicants;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\AplicantRequest;

class AplicantsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $applicants = Aplicants::where('user_id', Auth::user()->id)->get();
        return view('applicants.view-applications', compact('applicants'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Aplicants  $aplicants
     * @return \Illuminate\Http\Response
     */
    public function show(Aplicants $aplicants)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Aplicants  $aplicants
     * @return \Illuminate\Http\Response
     */
    public function edit(Aplicants $aplicants)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Aplicants  $aplicants
     * @return \Illuminate\Http\Response
     */
    public function update(AplicantRequest $request)
    {
       $aplicants = Aplicants::find($request->id);
       $aplicants->status = 1;
       if($aplicants->save()) {
        return response()->json(['success'=> $aplicants], $aplicants ? 200 : 500);  
       } 
      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Aplicants  $aplicants
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $spplicants = Aplicants::find($id);
        $project = $spplicants->project;
        if($spplicants->delete()) {
            return response()->json(['success'=>$project], $project ? 200 : 500);
        }
    }
}
