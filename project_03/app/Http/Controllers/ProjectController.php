<?php

namespace App\Http\Controllers;

use App\Models\Academy;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ProjectRequest;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Auth::user()->projects;
        return view('profile.project', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $academies = Academy::all();
        return view('profile.create-project', compact('academies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectRequest $request)
    {
        $project = $request->toArray();
        $project['name'] = $request->new_project;
        $project['description'] = $request->new_description;
        $project['user_id'] = auth()->user()->id;
        $success = Project::create($project);
        // return response()->json(['success'=>$success->academies()], $success ? 200 : 500);   
        $success->academy()->sync($request->academy);
        return response()->json(['success'=>$success], $success ? 200 : 500);   
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Project::find($id);
        $academies = Academy::all();
        return view('profile.edit-project', compact('project', 'academies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectRequest $request, Project $project)
    {
        $project->name = $request->new_project;
        $project->description =  $request->new_description;
        $project->user_id = auth()->user()->id;
  
        if($project->save()) {
         $project->academy()->sync($request->academy);
        return response()->json(['success'=> $project], $project ? 200 : 500);   
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
    * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::find($id);
        $academies = $project->academy()->pluck("academy_project.academy_id")->toArray();

        if($project->delete()) {
        // $project->academies();
        $project->academy()->detach($academies);
        return response()->json(['success'=>$project], $project ? 200 : 500);
    }
       
    }
        
  
}
