<?php

namespace App\Rules;

use Illuminate\Support\Arr;
use Illuminate\Contracts\Validation\Rule;

class ProjectAcademiesRule implements Rule
{
    protected $messages = [];
        
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (count($value) < 1 || count($value) > 4 ) {
            $this->fail("Please select no more than 4 options");
        }

        if (! empty($this->messages)) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->messages;
    }

    /**
     * Adds the given failures, and return false.
     *
     * @param  array|string  $messages
     * @return bool
     */
    protected function fail($messages)
    {
        $messages = collect(Arr::wrap($messages))->map(function ($message) {
            return __($message);
        })->all();

        $this->messages = array_merge($this->messages, $messages);

        return false;
    }
}
