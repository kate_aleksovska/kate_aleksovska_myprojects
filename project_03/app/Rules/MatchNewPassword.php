<?php

namespace App\Rules;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Validation\Rule;

class MatchNewPassword implements Rule
{
    protected $messages = [];
        
        /**
         * Determine if the validation rule passes.
         *
         * @param  string  $attribute
         * @param  mixed  $value
         * @return bool
         */
        public function passes($attribute, $value)
        {
            if (strlen($value) < 6) {
                $this->fail("The :attribute must have at least 6 characters or more.");
            }
    
            if (! preg_match('/(\p{Ll}+.*\p{Lu})|(\p{Lu}+.*\p{Ll})/u', $value)) {
                $this->fail('The :attribute must contain at least one uppercase and one lowercase letter.');
            }
    
            if (! preg_match('/\pL/u', $value)) {
                $this->fail('The :attribute must contain at least one letter.');
            }
    
            if (! preg_match('/\p{Z}|\p{S}|\p{P}/u', $value)) {
                $this->fail('The :attribute must contain at least one symbol.');
            }
    
            if (! preg_match('/\pN/u', $value)) {
                $this->fail('The :attribute must contain at least one number.');
            }
            
            if (Hash::check($value, auth()->user()->password)) {
               $this->fail('You already used this password, create new one');
            }

            if (! empty($this->messages)) {
                return false;
            }
    
            return true;
        }
    
        /**
         * Get the validation error message.
         *
         * @return string
         */
        public function message()
        {
            return $this->messages;
        }
    
        /**
         * Adds the given failures, and return false.
         *
         * @param  array|string  $messages
         * @return bool
         */
        protected function fail($messages)
        {
            $messages = collect(Arr::wrap($messages))->map(function ($message) {
                return __($message);
            })->all();
    
            $this->messages = array_merge($this->messages, $messages);
    
            return false;
        }
}
