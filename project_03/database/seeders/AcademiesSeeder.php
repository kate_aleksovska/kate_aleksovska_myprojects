<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class AcademiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('academies')->insert(
            [['name'=>'Backend Dev', 'profession'=>'Backend Developer'],
            ['name'=>'Frontend Dev', 'profession'=>'Frontend Developer'],
            ['name'=>'Marketing', 'profession'=>'Marketing Manager'],
            ['name'=>'Design', 'profession'=>'designer'],
            ['name'=>'QA', 'profession'=>'QA Tester'],
            ['name'=>'UX/UI', 'profession'=>'UX/UI Designer'],
            ['name'=>'Data Science', 'profession'=>'Data Scientist'], 
        ]);
    }
}
