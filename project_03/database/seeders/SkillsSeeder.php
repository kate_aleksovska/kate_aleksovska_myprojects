<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class SkillsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('skills')->insert([
            ['name' => 'Data Analisys'],
            ['name' => 'Business Intelligence'],
            ['name' => 'Machine Learning'],
            ['name' => 'Data Visualizations'],
            ['name' => 'Deep Learning'],
            ['name' => 'Statistics'],
            ['name' => 'Python'],
            ['name' => 'Power BI'],
            ['name' => 'NumPy'],
            ['name' => 'Keras'],
            ['name' => 'TensorFlow'],
            ['name' => 'Pandas'],
            ['name' => 'OpenCV'],
            ['name' => 'Writing and Executing Test Cases and Scenarios'],
            ['name' => 'Test Design'],
            ['name' => 'Translation Manual Test Cases and Scenarios'],
            ['name' => 'Defect Reporting'],
            ['name' => 'Quality Assurance'],
            ['name' => 'Waterfall and SCRUP Methodology'],
            ['name' => 'Java'],
            ['name' => 'C#'],
            ['name' => 'Kiwi'],
            ['name' => 'Selenium Web developer'],
            ['name' => 'Illustrator'],
            ['name' => 'Photoshop'],
            ['name' => 'InDesign'],
            ['name' => 'XD'],
            ['name' => 'LightRoom'],
            ['name' => 'Typography'],
            ['name' => 'Branding'],
            ['name' => 'Poster Design'],
            ['name' => 'Logo Design'],
            ['name' => 'Package Design'],
            ['name' => 'Digital marketing Strategy'],
            ['name' => 'Social Media Marketing'],
            ['name' => 'facebook & Instagram Ads'],
            ['name' => 'Google Ads'],
            ['name' => 'Copywriting'],
            ['name' => 'Data Analisys'],
            ['name' => 'Data Processing'],
            ['name' => 'E-Mail Marketing'],
            ['name' => 'Search Engine Optimization'],
            ['name' => 'HTML'],
            ['name' => 'CSS'],
            ['name' => 'Bootstrap'],
            ['name' => 'JavaScript'],
            ['name' => 'jQuery'],
            ['name' => 'AJAX'],
            ['name' => 'PHP'],
            ['name' => 'Laravel'],
            ['name' => 'ReactJS'],
            ['name' => 'GIT'],
            ['name' => 'UX/UI'],
            ['name' => 'MySQL'],
            ['name' => 'Data Warehouse'],
            ['name' => 'AWS Management Control'],
            ['name' => 'Big Data'],
            ['name' => 'database Management'],
            ['name' => 'Postman'],
            ['name' => 'Apache'],
            ['name' => 'JMeter'],
            ['name' => 'Google Analytics']
        ]);
    }
}
